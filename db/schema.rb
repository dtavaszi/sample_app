# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180129075722) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "businesses", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "email"
    t.string   "description"
    t.string   "phonenumber"
    t.string   "address"
    t.string   "website_url"
    t.string   "fb_url"
    t.string   "instagram_url"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "slug"
    t.string   "color"
    t.string   "avatar"
    t.string   "session_type"
  end

  create_table "change_histories", force: :cascade do |t|
    t.integer  "table",                          null: false
    t.integer  "row",                            null: false
    t.integer  "business_id",                    null: false
    t.integer  "user_id",                        null: false
    t.string   "value"
    t.string   "old_value"
    t.integer  "code",                           null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.boolean  "read",           default: false, null: false
    t.boolean  "notified_email", default: false, null: false
    t.index ["business_id"], name: "index_change_histories_on_business_id", using: :btree
    t.index ["user_id"], name: "index_change_histories_on_user_id", using: :btree
  end

  create_table "constants", force: :cascade do |t|
    t.string   "table",       null: false
    t.integer  "references",  null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "value"
    t.integer  "business_id"
  end

  create_table "friends", force: :cascade do |t|
    t.integer  "user1_id",               null: false
    t.integer  "user2_id",               null: false
    t.integer  "status",     default: 0, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "google_auths", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["user_id"], name: "index_google_auths_on_user_id", using: :btree
  end

  create_table "group_exceptions", force: :cascade do |t|
    t.integer  "group_id"
    t.integer  "workout_session_id"
    t.date     "date"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["group_id", "date"], name: "index_group_exceptions_on_group_id_and_date", unique: true, using: :btree
    t.index ["group_id"], name: "index_group_exceptions_on_group_id", using: :btree
    t.index ["workout_session_id"], name: "index_group_exceptions_on_workout_session_id", using: :btree
  end

  create_table "groups", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "repeat_on"
    t.integer  "repeat_type"
    t.date     "repeat_until"
  end

  create_table "invoice_items", force: :cascade do |t|
    t.integer  "invoice_id"
    t.integer  "item_id"
    t.integer  "quantity"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "business_id"
  end

  create_table "invoice_months", force: :cascade do |t|
    t.integer  "business_id"
    t.date     "date",                        null: false
    t.boolean  "locked",      default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["business_id"], name: "index_invoice_months_on_business_id", using: :btree
  end

  create_table "invoices", force: :cascade do |t|
    t.integer  "user_id"
    t.boolean  "processed",       default: false, null: false
    t.decimal  "amount"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "title"
    t.date     "date",                            null: false
    t.integer  "membership_id"
    t.decimal  "balance"
    t.integer  "business_id"
    t.boolean  "reconciled",      default: false, null: false
    t.datetime "reconciled_date"
    t.index ["user_id"], name: "index_invoices_on_user_id", using: :btree
  end

  create_table "items", force: :cascade do |t|
    t.string   "name"
    t.string   "category"
    t.integer  "quantity"
    t.boolean  "available"
    t.decimal  "price",          default: "0.0", null: false
    t.integer  "tax_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.date     "available_from"
    t.date     "available_to"
    t.string   "description"
    t.integer  "business_id"
  end

  create_table "memberships", force: :cascade do |t|
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "name",                           null: false
    t.string   "description"
    t.boolean  "available",      default: false, null: false
    t.decimal  "cost",           default: "0.0", null: false
    t.integer  "max_workouts",   default: 20,    null: false
    t.integer  "max_transfers",  default: 3,     null: false
    t.datetime "available_from"
    t.datetime "available_to"
    t.integer  "tax_id"
    t.integer  "business_id"
  end

  create_table "payments", force: :cascade do |t|
    t.integer  "payment_type", default: 0, null: false
    t.decimal  "amount"
    t.integer  "user_id"
    t.integer  "invoice_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.date     "paydate",                  null: false
    t.string   "memo"
    t.integer  "business_id"
    t.index ["invoice_id"], name: "index_payments_on_invoice_id", using: :btree
    t.index ["user_id"], name: "index_payments_on_user_id", using: :btree
  end

  create_table "profiles", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "firstname"
    t.string   "lastname"
    t.date     "dob"
    t.string   "street"
    t.string   "city"
    t.string   "postalcode"
    t.string   "phonenumber"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["user_id"], name: "index_profiles_on_user_id", using: :btree
  end

  create_table "reminders", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "date"
    t.integer  "workout_session_id"
    t.string   "memo"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "business_id"
    t.string   "title",              default: "",    null: false
    t.boolean  "complete",           default: false, null: false
    t.index ["user_id"], name: "index_reminders_on_user_id", using: :btree
    t.index ["workout_session_id"], name: "index_reminders_on_workout_session_id", using: :btree
  end

  create_table "reschedules", force: :cascade do |t|
    t.integer  "workout_session_id",              null: false
    t.integer  "link",               default: -1, null: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "business_id"
    t.index ["link"], name: "index_reschedules_on_link", using: :btree
    t.index ["workout_session_id"], name: "index_reschedules_on_workout_session_id", using: :btree
  end

  create_table "subscriptions", force: :cascade do |t|
    t.integer  "user_id",                          null: false
    t.integer  "business_id",                      null: false
    t.integer  "status"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.boolean  "active",            default: true, null: false
    t.integer  "role",              default: 0,    null: false
    t.string   "gdrive_url"
    t.datetime "gdrive_updated_at"
    t.integer  "membership_id"
    t.index ["business_id"], name: "index_subscriptions_on_business_id", using: :btree
    t.index ["user_id", "business_id"], name: "index_subscriptions_on_user_id_and_business_id", unique: true, using: :btree
    t.index ["user_id"], name: "index_subscriptions_on_user_id", using: :btree
  end

  create_table "suggestions", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "business_id"
    t.string   "category"
    t.string   "location"
    t.string   "body"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["business_id"], name: "index_suggestions_on_business_id", using: :btree
    t.index ["user_id"], name: "index_suggestions_on_user_id", using: :btree
  end

  create_table "taxes", force: :cascade do |t|
    t.string   "name"
    t.integer  "percentage"
    t.boolean  "available"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "business_id"
  end

  create_table "user_rates", force: :cascade do |t|
    t.integer  "user_id"
    t.decimal  "rate",        default: "0.0", null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "business_id"
    t.index ["user_id"], name: "index_user_rates_on_user_id", using: :btree
  end

  create_table "user_workouts", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "workout_session_id"
    t.integer  "usertype",           default: 0
    t.boolean  "attended",           default: true
    t.string   "note"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.decimal  "cost"
    t.integer  "invoice_id"
    t.boolean  "membership",         default: false, null: false
    t.integer  "business_id"
    t.boolean  "deleted",            default: false, null: false
    t.index ["invoice_id"], name: "index_user_workouts_on_invoice_id", using: :btree
    t.index ["user_id"], name: "index_user_workouts_on_user_id", using: :btree
    t.index ["workout_session_id"], name: "index_user_workouts_on_workout_session_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                                  null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "admin",                  default: false
    t.datetime "last_seen"
    t.decimal  "credit",                 default: "0.0", null: false
    t.string   "activation_digest"
    t.boolean  "activated",              default: false
    t.datetime "activated_at"
    t.boolean  "email_session_reminder", default: true,  null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
  end

  create_table "workout_sessions", force: :cascade do |t|
    t.datetime "date"
    t.integer  "user_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "memo"
    t.integer  "status",      default: 0,     null: false
    t.datetime "duration"
    t.string   "title"
    t.integer  "eventtype",   default: 1,     null: false
    t.integer  "group_id"
    t.integer  "bfr_from",    default: 0,     null: false
    t.integer  "bfr_to",      default: 0,     null: false
    t.string   "location"
    t.string   "gid"
    t.boolean  "gsync",       default: true,  null: false
    t.integer  "tax_id"
    t.integer  "business_id"
    t.boolean  "deleted",     default: false, null: false
    t.index ["group_id"], name: "index_workout_sessions_on_group_id", using: :btree
    t.index ["user_id", "date"], name: "index_workout_sessions_on_user_id_and_date", using: :btree
    t.index ["user_id"], name: "index_workout_sessions_on_user_id", using: :btree
  end

  add_foreign_key "change_histories", "businesses"
  add_foreign_key "change_histories", "users"
  add_foreign_key "google_auths", "users"
  add_foreign_key "group_exceptions", "groups"
  add_foreign_key "group_exceptions", "workout_sessions"
  add_foreign_key "invoice_months", "businesses"
  add_foreign_key "invoices", "users"
  add_foreign_key "payments", "invoices"
  add_foreign_key "payments", "users"
  add_foreign_key "profiles", "users"
  add_foreign_key "reminders", "users"
  add_foreign_key "reminders", "workout_sessions"
  add_foreign_key "reschedules", "workout_sessions"
  add_foreign_key "subscriptions", "businesses"
  add_foreign_key "subscriptions", "users"
  add_foreign_key "suggestions", "businesses"
  add_foreign_key "suggestions", "users"
  add_foreign_key "user_rates", "users"
  add_foreign_key "user_workouts", "invoices"
  add_foreign_key "user_workouts", "users"
  add_foreign_key "user_workouts", "workout_sessions"
  add_foreign_key "workout_sessions", "groups"
  add_foreign_key "workout_sessions", "users"
end
