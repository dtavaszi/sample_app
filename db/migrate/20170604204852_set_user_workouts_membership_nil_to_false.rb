class SetUserWorkoutsMembershipNilToFalse < ActiveRecord::Migration[5.0]
  def change
	change_column :user_workouts, :membership, :boolean, null:  false, default: false
  end
end
