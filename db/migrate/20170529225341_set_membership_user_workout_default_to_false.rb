class SetMembershipUserWorkoutDefaultToFalse < ActiveRecord::Migration[5.0]
  def change
    add_column :user_workouts, :membership, :boolean, :default => false
  end
end
