class ChangeConstantsValueToNotNull < ActiveRecord::Migration[5.0]
  def change
	change_column :constants, :value, :integer, null:  false
  end
end
