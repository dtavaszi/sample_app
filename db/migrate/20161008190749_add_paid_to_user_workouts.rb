class AddPaidToUserWorkouts < ActiveRecord::Migration[5.0]
  def change
    add_column :user_workouts, :paid, :boolean, :default => false, :null => false
  end
end
