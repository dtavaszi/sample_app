class RemoveDobFromUsers < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :dob, :date
  end
end
