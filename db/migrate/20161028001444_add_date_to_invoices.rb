class AddDateToInvoices < ActiveRecord::Migration[5.0]
  def change		
    add_column :invoices, :date, :date
		
		Invoice.all.each do |i|
			i.update_attribute(:date, i.created_at)
		end
		
		change_column :invoices, :date, :date, null: false
  end
end
