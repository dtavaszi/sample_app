class CreateUserWorkouts < ActiveRecord::Migration[5.0]
  def change
    create_table :user_workouts do |t|
      t.references :user, foreign_key: true
      t.references :workout_session, foreign_key: true
      t.integer :usertype, :default => 0
      t.boolean :attended, :default => true
      t.string :note

      t.timestamps
    end
  end
end
