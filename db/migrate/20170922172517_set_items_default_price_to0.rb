class SetItemsDefaultPriceTo0 < ActiveRecord::Migration[5.0]
  def change
    change_column :items, :price, :decimal, null: false, default: 0.0
  end
end
