class AddRepeatOnAndRepeatTypeToGroup < ActiveRecord::Migration[5.0]
  def change
    add_column :groups, :repeat_on, :string
    add_column :groups, :repeat_type, :integer
  end
end
