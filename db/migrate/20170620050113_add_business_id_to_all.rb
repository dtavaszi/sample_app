class AddBusinessIdToAll < ActiveRecord::Migration[5.0]
  def up
  	add_column :constants, :business_id, :integer
  	add_column :invoices, :business_id, :integer
  	add_column :items, :business_id, :integer
  	add_column :invoice_items, :business_id, :integer
  	add_column :memberships, :business_id, :integer
  	add_column :payments, :business_id, :integer
  	add_column :reminders, :business_id, :integer
  	add_column :reschedules, :business_id, :integer
  	add_column :taxes, :business_id, :integer
  	add_column :user_rates, :business_id, :integer
  	add_column :workout_sessions, :business_id, :integer
  end

  def down
  	remove_column :constants, :business_id
  	remove_column :invoices, :business_id
  	remove_column :items, :business_id
  	remove_column :invoice_items, :business_id
  	remove_column :memberships, :business_id
  	remove_column :payments, :business_id
  	remove_column :reminders, :business_id
  	remove_column :reschedules, :business_id
  	remove_column :taxes, :business_id
  	remove_column :user_rates, :business_id
  	remove_column :workout_sessions, :business_id
  end
end
