class AddGroupToWorkoutSession < ActiveRecord::Migration[5.0]
  def change
    add_reference :workout_sessions, :group, foreign_key: true
  end
end
