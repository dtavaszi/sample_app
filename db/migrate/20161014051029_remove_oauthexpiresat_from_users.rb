class RemoveOauthexpiresatFromUsers < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :oauth_expires_at, :datetime
  end
end
