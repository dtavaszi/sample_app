class CreateUserRates < ActiveRecord::Migration[5.0]
  def change
    create_table :user_rates do |t|
      t.references :user, foreign_key: true
      t.decimal :rate, :default => 0, :null => false

      t.timestamps
    end
  end
end
