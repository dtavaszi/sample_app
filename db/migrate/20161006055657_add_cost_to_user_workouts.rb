class AddCostToUserWorkouts < ActiveRecord::Migration[5.0]
  def change
    add_column :user_workouts, :cost, :decimal
  end
end
