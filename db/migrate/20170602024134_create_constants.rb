class CreateConstants < ActiveRecord::Migration[5.0]
  def change
    create_table :constants do |t|
      t.string :table, null: false, unique: true
      t.integer :references, null: false, unique: true
      t.timestamps
    end
  end
end
