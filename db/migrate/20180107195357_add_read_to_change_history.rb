class AddReadToChangeHistory < ActiveRecord::Migration[5.0]
  def change
    add_column :change_histories, :read, :boolean, default: false, null: false
  end
end
