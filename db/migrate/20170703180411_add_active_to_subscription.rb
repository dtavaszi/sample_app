class AddActiveToSubscription < ActiveRecord::Migration[5.0]
  def change
    add_column :subscriptions, :active, :boolean, null: false, default: true

    Subscription.all.update_all(active: true)
  end
end
