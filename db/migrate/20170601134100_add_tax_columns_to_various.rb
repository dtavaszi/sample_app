class AddTaxColumnsToVarious < ActiveRecord::Migration[5.0]
  def change
    add_column :workout_sessions, :tax_id, :integer
    add_column :memberships, :tax_id, :integer
  end
end
