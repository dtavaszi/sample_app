class ChangeAmountFormatInInvoice < ActiveRecord::Migration[5.0]
  def up
    change_column :invoices, :amount, :decimal
    change_column :user_rates, :rate, :decimal
    change_column :user_workouts, :cost, :decimal
  end
  
  def down
    change_column :invoices, :amount, :integer
    change_column :user_rates, :rate, :integer
    change_column :user_workouts, :cost, :integer
  end
end
