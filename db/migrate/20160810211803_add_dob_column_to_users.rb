class AddDobColumnToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :dob, :Date
  end
end
