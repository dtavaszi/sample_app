class AddBusinessIdToUserWorkouts < ActiveRecord::Migration[5.0]
  def change
    add_column :user_workouts, :business_id, :integer, foreign_key: true
  end
end
