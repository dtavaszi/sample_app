class RenameSubscriptionTypeToStatus < ActiveRecord::Migration[5.0]
  def change
    rename_column :subscriptions, :type, :status
  end
end
