class ChangeConstantsToAllowNull < ActiveRecord::Migration[5.0]
  def change
	change_column :constants, :value, :integer, null:  true
  end
end
