class AddGidToWorkoutSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :workout_sessions, :gid, :string
  end
end
