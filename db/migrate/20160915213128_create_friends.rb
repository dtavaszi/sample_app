class CreateFriends < ActiveRecord::Migration[5.0]
  def change
    create_table :friends do |t|
      t.integer :user1_id, foreign_key: true, :null => false
      t.integer :user2_id, foreign_key: true, :null => false
      t.integer :status, :null => false, :default => 0

      t.timestamps
    end
  end
end
