class AddInvoiceToUserWorkouts < ActiveRecord::Migration[5.0]
  def change
    add_reference :user_workouts, :invoice, foreign_key: true
  end
end