class MembershipNameNotNull < ActiveRecord::Migration[5.0]
  def up
    Membership.where(:name => "").update_all(:name => "Default")
    change_column :memberships, :name, :string, null: false
    Membership.where(:cost => nil).update_all(:cost => 0)
    change_column :memberships, :cost, :decimal, null: false, default: 0
  end

  def down

  end
end
