class CreateInvoices < ActiveRecord::Migration[5.0]
  def change
    create_table :invoices do |t|
      t.references :user, foreign_key: true
      t.boolean :processed, :default => false, :null => false
      t.decimal :amount

      t.timestamps
    end
  end
end
