class CreateSuggestions < ActiveRecord::Migration[5.0]
  def change
    unless Suggestion.table_exists?
      create_table :suggestions do |t|
        t.references :user, foreign_key: true
        t.references :business, foreign_key: true
        t.string :category
        t.string :location
        t.string :body

        t.timestamps
      end
    end
  end
end
