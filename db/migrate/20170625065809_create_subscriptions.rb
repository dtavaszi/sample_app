class CreateSubscriptions < ActiveRecord::Migration[5.0]
  def change
    create_table :subscriptions do |t|
      t.references :user, foreign_key: true
      t.references :business, foreign_key: true
      t.integer :type

      t.timestamps
    end
  end
end
