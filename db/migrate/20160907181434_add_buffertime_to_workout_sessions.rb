class AddBuffertimeToWorkoutSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :workout_sessions, :bfr_from, :integer, :default => 0, :null => false
    add_column :workout_sessions, :bfr_to, :integer, :default => 0, :null => false
  end
end
