class AddValueToConstantTable < ActiveRecord::Migration[5.0]
  def change
	 add_column :constants, :value, :integer
  end
end
