class AddMembershipIdToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :membership_id, :integer
  end
end
