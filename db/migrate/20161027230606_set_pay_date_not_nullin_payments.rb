class SetPayDateNotNullinPayments < ActiveRecord::Migration[5.0]
  def change
		Payment.all.each do |p|
			p.update_attribute(:paydate, p.created_at)
		end
		
		change_column :payments, :paydate, :date, null: false
  end
end
