class AddCreditToUsers < ActiveRecord::Migration[5.0]
	def change
	add_column :users, :credit, :decimal, null: false, default: 0
	end
end
