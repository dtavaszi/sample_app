class AddIndexToRescheduleLink < ActiveRecord::Migration[5.0]
  def change
    add_index :reschedules, :link
  end
end
