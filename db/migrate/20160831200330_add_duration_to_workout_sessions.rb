class AddDurationToWorkoutSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :workout_sessions, :duration, :datetime
	add_column :workout_sessions, :title, :string
	add_column :workout_sessions, :eventtype, :int, :default => 1, :null => false
  end
end
