class RemovePhoneNumberFromUsers < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :phonenumber, :string
  end
end
