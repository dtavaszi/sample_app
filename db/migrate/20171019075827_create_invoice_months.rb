class CreateInvoiceMonths < ActiveRecord::Migration[5.0]
  def change
    create_table :invoice_months do |t|
      t.references :business, foreign_key: true
      t.date :date, null: false
      t.boolean :locked, default: false
      t.timestamps
    end
  end
end
