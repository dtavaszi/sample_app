class AddSessionTypesToBusiness < ActiveRecord::Migration[5.0]
  def change
    add_column :businesses, :session_type, :string
  end
end
