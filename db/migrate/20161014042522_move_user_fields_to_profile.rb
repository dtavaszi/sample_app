class MoveUserFieldsToProfile < ActiveRecord::Migration[5.0]
  def up
		User.all.each do |u|
			u.profile = Profile.create(:firstname => u.name, :dob => u.dob, :street => u.address, :phonenumber => u.phonenumber)
			u.save!
		end		
  end
end