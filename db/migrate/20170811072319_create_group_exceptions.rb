class CreateGroupExceptions < ActiveRecord::Migration[5.0]
  def change
    create_table :group_exceptions do |t|
      t.references :group, foreign_key: true
      t.references :workout_session, foreign_key: true
      t.date :date

      t.timestamps
    end
  end
end
