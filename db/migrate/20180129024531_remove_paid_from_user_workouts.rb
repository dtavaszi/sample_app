class RemovePaidFromUserWorkouts < ActiveRecord::Migration[5.0]
  def change
    remove_column :user_workouts, :paid
  end
end
