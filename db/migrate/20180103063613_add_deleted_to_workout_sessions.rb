class AddDeletedToWorkoutSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :workout_sessions, :deleted, :boolean, default: false, null: false
  end
end
