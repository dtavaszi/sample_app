class ChangePaymentPaymentTypeToEnum < ActiveRecord::Migration[5.0]
  def change
    change_column :payments, :payment_type, 'integer USING CAST(payment_type AS INTEGER)', default: 0, null: false
  end
end
