class AddGDriveToSubscription < ActiveRecord::Migration[5.0]
  def change
    add_column :subscriptions, :gdrive_url, :string
    add_column :subscriptions, :gdrive_updated_at, :datetime
  end
end
