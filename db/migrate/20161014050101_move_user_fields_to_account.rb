class MoveUserFieldsToAccount < ActiveRecord::Migration[5.0]
  def up
		User.all.each do |u|
			u.account = Account.create(:approved => u.approved, :user_type => u.admin ? "Trainer" : "Client")
			u.save!
		end		
  end
end
