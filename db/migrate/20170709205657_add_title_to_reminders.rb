class AddTitleToReminders < ActiveRecord::Migration[5.0]
  def change
    add_column :reminders, :title, :string, default: "Untitled reminder"

    Reminder.all.each do |reminder|
      reminder.update_attribute(:title, reminder.memo)
    end

    Reminder.where(:title => nil).update_all(:title => "Untitled reminder")

    change_column :reminders, :title, :string, null: false, default: "Untitled reminder"
  end
end
