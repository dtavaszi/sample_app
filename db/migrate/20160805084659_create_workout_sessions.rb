class CreateWorkoutSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :workout_sessions do |t|
      t.datetime :date, uniqueness: true
      t.references :user, foreign_key: true

      t.timestamps
    end
	add_index :workout_sessions, [:user_id, :date]
  end
end
