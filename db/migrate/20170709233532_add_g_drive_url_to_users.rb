class AddGDriveUrlToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :g_drive_url, :string
    add_column :users, :new_gdrive_workout, :datetime
  end
end
