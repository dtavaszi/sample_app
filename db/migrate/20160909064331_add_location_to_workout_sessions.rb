class AddLocationToWorkoutSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :workout_sessions, :location, :string
  end
end
