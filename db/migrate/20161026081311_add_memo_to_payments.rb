class AddMemoToPayments < ActiveRecord::Migration[5.0]
  def change
    add_column :payments, :memo, :string
  end
end
