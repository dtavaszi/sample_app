class ChangePriceToInteger < ActiveRecord::Migration[5.0]
  def up
    change_column :invoices, :amount, :integer
    change_column :payments, :amount, :integer
    change_column :user_rates, :rate, :integer
    change_column :user_workouts, :cost, :integer
  end
end
