class CreateChangeHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :change_histories do |t|
      t.integer :table, null: false
      t.integer :row, null: false
      t.references :business, foreign_key: true, null: false
      t.references :user, foreign_key: true, null: false
      t.string :value
      t.string :old_value
      t.integer :code, null: false

      t.timestamps
    end
  end
end
