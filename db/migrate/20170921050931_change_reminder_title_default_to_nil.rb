class ChangeReminderTitleDefaultToNil < ActiveRecord::Migration[5.0]
  def up
    change_column :reminders, :title, :string, null: false, default: ""
  end
end
