class AddRepeatUntilToGroups < ActiveRecord::Migration[5.0]
  def change
    add_column :groups, :repeat_until, :date
  end
end
