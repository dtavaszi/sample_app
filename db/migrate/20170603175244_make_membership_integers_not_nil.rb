class MakeMembershipIntegersNotNil < ActiveRecord::Migration[5.0]
  def change
    change_column :memberships, :max_workouts, :integer, null: false, default:  20
    change_column :memberships, :max_transfers, :integer, null: false, default: 3
  end
end
