class AddBusinessColorToBusiness < ActiveRecord::Migration[5.0]
  def change
    add_column :businesses, :color, :string
  end
end
