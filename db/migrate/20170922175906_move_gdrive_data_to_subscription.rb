class MoveGdriveDataToSubscription < ActiveRecord::Migration[5.0]
  def change
    User.all.each do |usr|
      usr.subscriptions.update_all(:gdrive_url => usr.g_drive_url, :gdrive_updated_at => usr.new_gdrive_workout)
    end
  end
end
