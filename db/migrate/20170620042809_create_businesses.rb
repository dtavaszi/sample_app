class CreateBusinesses < ActiveRecord::Migration[5.0]
  def change
    create_table :businesses do |t|
		t.integer :user_id
		t.string :name
		t.string :email
		t.string :description
		t.string :phonenumber
		t.string :address
		t.string :website_url
		t.string :fb_url
		t.string :instagram_url

		t.timestamps
    end
  end
end
