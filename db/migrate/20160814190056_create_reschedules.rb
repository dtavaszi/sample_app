class CreateReschedules < ActiveRecord::Migration[5.0]
  def change
    create_table :reschedules do |t|
      t.references :workout_session, foreign_key: true, :null => false, :unique => true
      t.integer :link, :null => false, :default => -1

      t.timestamps
    end
  end
end
