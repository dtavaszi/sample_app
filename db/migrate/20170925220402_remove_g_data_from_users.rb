class RemoveGDataFromUsers < ActiveRecord::Migration[5.0]
  def change
    remove_column :users, :g_drive_url
    remove_column :users, :new_gdrive_workout
  end
end
