class AddNotesToWorkoutSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :workout_sessions, :memo, :string
  end
end
