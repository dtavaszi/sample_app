class RedoAllInvoiceCalculations < ActiveRecord::Migration[5.0]
  def change
    Invoice.where(:date => Time.zone.now-2.months...Time.zone.now+2.months).each do |invoice|
      invoice.adjust_invoice
    end
  end
end
