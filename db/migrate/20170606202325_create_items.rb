class CreateItems < ActiveRecord::Migration[5.0]
  def change
    create_table :items do |t|
		t.string :name
		t.string :category
		t.integer :quantity
		t.boolean :available
		t.decimal :price
		t.integer :tax_id
		t.timestamps
		t.date :available_from
		t.date :available_to
    end
  end
end
