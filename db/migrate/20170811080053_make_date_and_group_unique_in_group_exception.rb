class MakeDateAndGroupUniqueInGroupException < ActiveRecord::Migration[5.0]
  def change
    add_index :group_exceptions, [:group_id, :date], unique: true
  end
end
