class CreateReminders < ActiveRecord::Migration[5.0]
  def change
    create_table :reminders do |t|
      t.references :user, foreign_key: true
      t.date :date
      t.references :workout_session, foreign_key: true
      t.string :memo

      t.timestamps
    end
  end
end
