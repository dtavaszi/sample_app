class AddTitleToInvoices < ActiveRecord::Migration[5.0]
  def change
    add_column :invoices, :title, :string
  end
end
