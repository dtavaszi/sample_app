class AddBalanceToInvoices < ActiveRecord::Migration[5.0]
  def change
	add_column :invoices, :balance, :decimal
  end
end
