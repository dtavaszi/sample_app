class ChangeRemindersDateToDateTime < ActiveRecord::Migration[5.0]
  def up
    change_column :reminders, :date, :datetime
    add_column :reminders, :complete, :boolean, default: false, null: false
  end
end
