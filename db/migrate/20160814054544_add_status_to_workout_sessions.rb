class AddStatusToWorkoutSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :workout_sessions, :status, :int, :default => 0, :null => false
  end
end
