class AddReconciledToInvoices < ActiveRecord::Migration[5.0]
  def change
    add_column :invoices, :reconciled, :boolean, default: false, null: false
    add_column :invoices, :reconciled_date, :datetime
  end
end
