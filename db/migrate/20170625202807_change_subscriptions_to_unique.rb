class ChangeSubscriptionsToUnique < ActiveRecord::Migration[5.0]
  def change
    change_column :subscriptions, :user_id, :integer, null: false
    change_column :subscriptions, :business_id, :integer, null: false
    add_index :subscriptions, [:user_id, :business_id], unique: true
  end
end
