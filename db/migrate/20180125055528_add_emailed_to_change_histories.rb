class AddEmailedToChangeHistories < ActiveRecord::Migration[5.0]
  def change
    add_column :change_histories, :notified_email, :boolean, default: false, null: false
  end
end
