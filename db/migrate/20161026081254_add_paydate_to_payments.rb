class AddPaydateToPayments < ActiveRecord::Migration[5.0]
  def change
    add_column :payments, :paydate, :date
  end
end
