class AddEmailSessionReminderToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :email_session_reminder, :boolean, default: true, null: false
  end
end
