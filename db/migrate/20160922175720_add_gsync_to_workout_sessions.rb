class AddGsyncToWorkoutSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :workout_sessions, :gsync, :boolean, :default => true, :null => false
  end
end
