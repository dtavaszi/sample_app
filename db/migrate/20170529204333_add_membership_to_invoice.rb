class AddMembershipToInvoice < ActiveRecord::Migration[5.0]
  def change
    add_column :invoices, :membership_id, :integer
  end
end
