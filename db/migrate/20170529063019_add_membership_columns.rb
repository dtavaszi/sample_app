class AddMembershipColumns < ActiveRecord::Migration[5.0]
  def change
    add_column :memberships, :name, :string
    add_column :memberships, :description, :string
    add_column :memberships, :available, :boolean, :default => false, :null => false
    add_column :memberships, :cost, :decimal
    add_column :memberships, :max_workouts, :integer
    add_column :memberships, :max_transfers, :integer
    add_column :memberships, :available_from, :datetime
    add_column :memberships, :available_to, :datetime
  end
end
