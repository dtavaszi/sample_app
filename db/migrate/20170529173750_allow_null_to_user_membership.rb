class AllowNullToUserMembership < ActiveRecord::Migration[5.0]
  def change
    change_column :users, :membership_id, :integer, null: true
  end
end
