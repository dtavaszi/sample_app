class MoveMembershipToSubscription < ActiveRecord::Migration[5.0]
  def change
    add_column :subscriptions, :membership_id, :integer, default: :null
    remove_column :users, :membership_id
  end
end
