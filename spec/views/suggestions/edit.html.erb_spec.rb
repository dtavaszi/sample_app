require 'rails_helper'

RSpec.describe "suggestions/edit", type: :view do
  before(:each) do
    @suggestion = assign(:suggestion, Suggestion.create!(
      :user => nil,
      :business => nil,
      :category => "MyString",
      :location => "MyString",
      :body => "MyString"
    ))
  end

  it "renders the edit suggestion form" do
    render

    assert_select "form[action=?][method=?]", suggestion_path(@suggestion), "post" do

      assert_select "input#suggestion_user_id[name=?]", "suggestion[user_id]"

      assert_select "input#suggestion_business_id[name=?]", "suggestion[business_id]"

      assert_select "input#suggestion_category[name=?]", "suggestion[category]"

      assert_select "input#suggestion_location[name=?]", "suggestion[location]"

      assert_select "input#suggestion_body[name=?]", "suggestion[body]"
    end
  end
end
