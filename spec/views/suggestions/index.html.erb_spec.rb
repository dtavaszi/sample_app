require 'rails_helper'

RSpec.describe "suggestions/index", type: :view do
  before(:each) do
    assign(:suggestions, [
      Suggestion.create!(
        :user => nil,
        :business => nil,
        :category => "Category",
        :location => "Location",
        :body => "Body"
      ),
      Suggestion.create!(
        :user => nil,
        :business => nil,
        :category => "Category",
        :location => "Location",
        :body => "Body"
      )
    ])
  end

  it "renders a list of suggestions" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Category".to_s, :count => 2
    assert_select "tr>td", :text => "Location".to_s, :count => 2
    assert_select "tr>td", :text => "Body".to_s, :count => 2
  end
end
