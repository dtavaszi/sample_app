require 'rails_helper'

RSpec.describe "suggestions/show", type: :view do
  before(:each) do
    @suggestion = assign(:suggestion, Suggestion.create!(
      :user => nil,
      :business => nil,
      :category => "Category",
      :location => "Location",
      :body => "Body"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
    expect(rendered).to match(/Category/)
    expect(rendered).to match(/Location/)
    expect(rendered).to match(/Body/)
  end
end
