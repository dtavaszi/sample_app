require 'rails_helper'

RSpec.describe "subscriptions/new", type: :view do
  before(:each) do
    assign(:subscription, Subscription.new(
      :user_id => nil,
      :business => nil,
      :type => 1
    ))
  end

  it "renders new subscription form" do
    render

    assert_select "form[action=?][method=?]", subscriptions_path, "post" do

      assert_select "input#subscription_user_id_id[name=?]", "subscription[user_id_id]"

      assert_select "input#subscription_business_id[name=?]", "subscription[business_id]"

      assert_select "input#subscription_type[name=?]", "subscription[type]"
    end
  end
end
