require 'rails_helper'

RSpec.describe "users/show", type: :view do
  before(:each) do
    @user = assign(:user, User.create!(
      :email => "Email",
      :password_digest => "Password Digest",
      :remember_digest => "Remember Digest",
      :membership_id => 2,
      :credit => "9.99"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Email/)
    expect(rendered).to match(/Password Digest/)
    expect(rendered).to match(/Remember Digest/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/9.99/)
  end
end
