require 'rails_helper'

RSpec.describe "users/index", type: :view do
  before(:each) do
    assign(:users, [
      User.create!(
        :email => "Email",
        :password_digest => "Password Digest",
        :remember_digest => "Remember Digest",
        :membership_id => 2,
        :credit => "9.99"
      ),
      User.create!(
        :email => "Email",
        :password_digest => "Password Digest",
        :remember_digest => "Remember Digest",
        :membership_id => 2,
        :credit => "9.99"
      )
    ])
  end

  it "renders a list of users" do
    render
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Password Digest".to_s, :count => 2
    assert_select "tr>td", :text => "Remember Digest".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
  end
end
