require 'rails_helper'

RSpec.describe "users/edit", type: :view do
  before(:each) do
    @user = assign(:user, User.create!(
      :email => "MyString",
      :password_digest => "MyString",
      :remember_digest => "MyString",
      :membership_id => 1,
      :credit => "9.99"
    ))
  end

  it "renders the edit user form" do
    render

    assert_select "form[action=?][method=?]", user_path(@user), "post" do

      assert_select "input#user_email[name=?]", "user[email]"

      assert_select "input#user_password_digest[name=?]", "user[password_digest]"

      assert_select "input#user_remember_digest[name=?]", "user[remember_digest]"

      assert_select "input#user_membership_id[name=?]", "user[membership_id]"

      assert_select "input#user_credit[name=?]", "user[credit]"
    end
  end
end
