require 'rails_helper'

RSpec.describe "invoice_months/new", type: :view do
  before(:each) do
    assign(:invoice_month, InvoiceMonth.new())
  end

  it "renders new invoice_month form" do
    render

    assert_select "form[action=?][method=?]", invoice_months_path, "post" do
    end
  end
end
