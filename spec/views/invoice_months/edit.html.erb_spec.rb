require 'rails_helper'

RSpec.describe "invoice_months/edit", type: :view do
  before(:each) do
    @invoice_month = assign(:invoice_month, InvoiceMonth.create!())
  end

  it "renders the edit invoice_month form" do
    render

    assert_select "form[action=?][method=?]", invoice_month_path(@invoice_month), "post" do
    end
  end
end
