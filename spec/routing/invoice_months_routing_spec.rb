require "rails_helper"

RSpec.describe InvoiceMonthsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/invoice_months").to route_to("invoice_months#index")
    end

    it "routes to #new" do
      expect(:get => "/invoice_months/new").to route_to("invoice_months#new")
    end

    it "routes to #show" do
      expect(:get => "/invoice_months/1").to route_to("invoice_months#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/invoice_months/1/edit").to route_to("invoice_months#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/invoice_months").to route_to("invoice_months#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/invoice_months/1").to route_to("invoice_months#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/invoice_months/1").to route_to("invoice_months#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/invoice_months/1").to route_to("invoice_months#destroy", :id => "1")
    end

  end
end
