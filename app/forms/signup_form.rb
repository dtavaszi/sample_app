class SignupForm
  # Rails 4: include ActiveModel::Model
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations

  def persisted?
    false
  end

  def self.model_name
    ActiveModel::Name.new(self, nil, "User")
  end

  validates_presence_of :email
  validate :verify_unique_email
	validate :verify_passwords_match
  validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/
  validates :password, length: { minimum: 6, maximum: 30, too_short: "must be at least %{count} characters", too_long: "must have no more than %{count} characters" }
  delegate :email, :password, :password_confirmation, to: :user
  delegate :firstname, :lastname, :dob, :street, :city, :postalcode, :phonenumber, to: :profile

  def user
    @user ||= User.new
  end

  def profile
    @profile ||= @user.build_profile
  end

  def submit(params)
    user.attributes = params.slice(:email, :password, :password_confirmation)
		profile.attributes = params.slice(:firstname, :lastname, :dob, :street, :city, :postalcode, :phonenumber)
    if valid?
      user.save!
			profile.save!
      true
    else
      false
    end
  end

	def verify_unique_email
		if User.exists? email: email
			errors.add :email, "Email already in use"
		end
	end

	def verify_password
		if password.empty?
			errors.add :password, "Please enter a password"
		end
	end

	def verify_passwords_match
		if password_confirmation && !password_confirmation.empty? && password != password_confirmation
			errors.add :password_confirmation, "Passwords do not match"
		end
	end

end
