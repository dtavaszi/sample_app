class ApplicationController < ActionController::Base
	protect_from_forgery with: :null_session
	before_action :verify_activated

	#protect_from_forgery with: :exception

	include Pundit
	include SessionsHelper

	rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

	def not_found
	  raise ActionController::RoutingError.new('Not Found')
	end

	private
		# Confirms a logged-in user.
		def logged_in_user
			if !logged_in?
				store_location
				redirect_to login_url
			elsif false && !approved
				redirect_to current_user
				flash[:info] =  "Your account has not yet been approved. Please wait or contact your trainer."
			end
		end

		def logged_in_user_embedded
			if !logged_in?
				store_location
				if @workoutsession
					redirect_to business_embedded_signup_url(@workoutsession.business.slug, format: 'js')
				else
					redirect_to embedded_signup_url, format: 'js'
				end
			end
		end

		def approved
			if (current_user.account && current_user.account.approved) || current_user.admin
				return true
			else
				return false
			end
		end

		def user_params
				params.require(:user).permit(:name, :email, :address, :phonenumber, :occupation, :dob, :password,
																		 :password_confirmation, )
		end

		# Confirms an admin user.
		def admin_user
			redirect_to( @business ? @business : root_url) unless current_user.try(:admin)
		end

		def user_not_authorized
			flash[:danger] = "Unauthorized"
			redirect_to(request.referrer || root_path)
		end

		def verify_activated
			if current_user && !current_user.activated? && (!current_user.activated_at || (current_user.activated_at + 12.hours) < Time.zone.now)
				flash[:warning] = "Please verify your email: <a href='#{ account_activations_send_activation_url }'> Send activation link </a>"
			end
		end

		def convert_to_datetime date
			Time.zone.strptime(date, "%m/%d/%Y %I:%M %p") if date.present?
		end

		def convert_to_date date
			Date.strptime(date, "%m/%d/%Y") if date.present?
		end

		def convert_to_date_ymd date
			Date.strptime(date, "%Y-%m-%d") if date.present?
		end
end
