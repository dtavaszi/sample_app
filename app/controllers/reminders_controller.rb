class RemindersController < ApplicationController
  after_action :verify_authorized
  before_action :set_reminder, only: [:show, :edit, :update, :destroy]
  #before_action :set_business, only: [:index, :create] ### Reminders irrelevant of business? ###

  #before_action :admin_user
  # GET /reminders
  # GET /reminders.json

  def index
    authorize current_user.reminders.new
    @reminders = Reminder.all
  end

  # GET /reminders/1
  # GET /reminders/1.json
  def show
    authorize @reminder
  end

	def view
		@reminder = Reminder.find(params[:id])
    authorize @reminder

		respond_to do |format|
			format.js
		end
	end

	def add
		@reminder = current_user.reminders.build
    authorize @reminder

		@ws = WorkoutSession.find(params[:id])

		respond_to do |format|
			format.js
		end
	end

  # GET /reminders/new
  def new
    @reminder = Reminder.new

    authorize @reminder
  end

  # GET /reminders/1/edit
  def edit
    authorize @reminder
  end

  # POST /reminders
  # POST /reminders.json
  def create
    @reminder = current_user.reminders.new(reminder_params)
    authorize @reminder

		@reminder.date = convert_to_date reminder_params[:date] if reminder_params[:date]

		if @reminder.save
			flash[:info] = "Reminder created. We'll make sure you don't forget!"
			redirect_to :back
		else
			error_message = ""
			br = ""
			@reminder.errors[:base].each do |e|
				error_message += br
				error_message += e

				br = "</br>"
			end
			flash[:warning] = error_message unless error_message.blank?

			redirect_to :back
		end
  end

  # PATCH/PUT /reminders/1
  # PATCH/PUT /reminders/1.json
  def update
    authorize @reminder

    respond_to do |format|
      if @reminder.update(reminder_params)
        format.html { redirect_to :back, notice: 'Reminder was successfully updated. Lest we forget.' }
        format.json { render :show, status: :ok, location: @reminder }
      else
        format.html { render :edit }
        format.json { render json: @reminder.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reminders/1
  # DELETE /reminders/1.json
  def destroy
    authorize @reminder
    @reminder.destroy

		flash[:info] = "Reminder removed. It won't be missed!"
		redirect_to :back

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reminder
      @reminder = Reminder.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reminder_params
      params.require(:reminder).permit(:user_id, :business_id, :date, :workout_session_id, :title, :memo)
    end

		def convert_to_date date
			Time.zone.strptime("15:00" + date, "%H:%M %m/%d/%Y").to_date
		end
end
