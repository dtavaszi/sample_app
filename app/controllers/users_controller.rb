class UsersController < ApplicationController
	after_action :verify_authorized, except: [:create]
	protect_from_forgery with: :exception, except: [:new_embedded, :create]
	before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
	before_action :set_business, only: [:index, :new_gdrive_workout, :edit_modal, :new_modal, :admin_create, :new_embedded, :create, :settings, :update]

	def new_modal
		@user = @business.users.new
		@user.target_business = @business
		authorize @user

		respond_to do |format|
			format.js
		end
	end

	def edit_modal
		@user = User.find(params[:id])
		@user.target_business = @business
		authorize @user

		@subscription = @user.get_subscription(@business.id)

		respond_to do |format|
			format.js
		end
	end

	def new_gdrive_workout
		@user = User.find(params[:id])

		@user.target_business = @business
		authorize @user

		@user.tick_gdrive_for(@business.id)

		redirect_to :back
	end

	def admin_index
		authorize

		@users = User.all.includes(:profile, :user_workouts, :user_rates).reorder("profiles.firstname")
		@uws = UserWorkout.all
		@user = User.new
	end

	def index
		usr = @business.users.new
		usr.target_business = @business
		authorize usr

		@users = @business.users.includes(:profile, :user_workouts, :user_rates).reorder("profiles.firstname")
		@uws = UserWorkout.where(:business_id => @business.id)
		@user = @business.users.new
	end

	def show
		@user = User.find(params[:id])

		@user.target_business = @business
		authorize @user

		@workout_sessions = @user.workout_sessions.paginate(page: params[:page])
		@sesh = WorkoutSession.joins("JOIN user_workouts ON workout_sessions.id = user_workouts.workout_session_id AND user_workouts.user_id =" + @user.id.to_s).where(:status => 0, :eventtype => 1)
	end

	def new
		authorize User.new
		if current_user
			redirect_to root_path
		else
			@signup_form ||= SignupForm.new

		end
	end

	def new_embedded
		@signup_form ||= SignupForm.new
		authorize User.new

		respond_to do |format|
			format.js
		end
	end

	def edit
		@user = User.find(params[:id])
		authorize @user

		respond_to do |format|
			format.js
		end
	end

	def settings
		id = params[:user]

		if id && @business && @business.view_as_employee?(current_user)
			@user = User.find(id)
		else
			@user = current_user
		end

		@user.target_business = @business
		authorize @user

		@tab = params[:tab]
		@edit = params[:edit]

		if @tab == "billing"
			@rate = @user.user_rates.build
		end
	end

	def update
		@user = User.find(params[:id])

		@user.target_business = @business
		authorize @user

		if @user.update(user_params)
			if current_user.admin?
				redirect_to :back, notice: "Profile updated. Amazing!"
			else
				redirect_to :back
			end
		else
			redirect_to :back
		end
	end

	def admin_create
		@user = User.new(user_params)
		@user.target_business = @business
		authorize @user

		if @user.save
			@business.users << @user
			redirect_to :back, notice: 'User was successfully created. Great job!'
		else
			redirect_to :back, notice: "Hmm, user could not be created"
		end
	end

	def create
		respond_to do |format|
			if (user = User.find_by(email: params[:user][:email].downcase))
				if user && user.authenticate(params[:user][:password])
					log_in user
					remember(user)

					format.html { redirect_back_or root_url, notice: 'Welcome to Fitrackular!' }
					format.js { render 'businesses/view_available_groupsessions_embedded.js.erb' }
				else
					@signup_form = SignupForm.new
					@signup_form.errors.add :email, "already in use, silly!"
					@signup_form.errors.add :password, "is not quite right..."
					format.html { render 'new' }
					format.js { render 'update_form.js.erb'}
				end
			else
				@signup_form = SignupForm.new

				if @signup_form.submit(user_params)
					if current_user && current_user.admin
						set_business
						@business.users << @signup_form.user

						format.html { redirect_back_or root_url, notice: 'User was successfully created.' }
						format.js { redirect_back_or root_url, notice: 'User was successfully created.' }
					else
						log_in @signup_form.user
						format.html {  redirect_back_or root_url, notice: 'Welcome to Fitrackular!' }
						format.js { render 'businesses/view_available_groupsessions_embedded.js.erb' }
					end

					ApplicationMailer.welcome_email(@signup_form.user).deliver_later

				else
					format.html { render 'new' }
					format.js { render 'update_form.js.erb'}
				end
			end
		end
	end

	def destroy
		authorize @user

		User.find(params[:id]).destroy
		UserWorkout.where(:user_id => params[:id]).destroy_all
		Friend.where("user1_id = ? OR user2_id = ?", params[:id], params[:id]).destroy_all
		redirect_to users_url
	end

	def approve
		user = User.find(params[:id])

		@user.target_business = @business
		authorize @user

		if @business
			if (subscription = user.subscriptions.where(:business_id => @business.id))
				subscription.first.update_attribute(:approved, true)
			else
				user.subscriptions << Subscription.create(:user_id => user.id, :business_id => @business.id, :approved => true)
			end
		end

		redirect_to :back
	end

	def autocomplete_user_name
		@autouser = User.select("id, name").where("name LIKE ?", "#{params[:name]}%").order(:name).limit(10)

		respond_to do |format|
			format.json { render json: @user , :only => [:id, :name] }
		end
	end

	def resync_google
		authorize current_user
		google_authenticate(true)
	end

	private

		def set_business
      if Business.friendly.exists? (params[:business_id])
        @business = Business.friendly.find(params[:business_id])
      end
    end

		def user_params
			params.require(:user).permit(
				:email, :g_drive_url, :password, :password_confirmation, :email_session_reminder, :membership_id, :firstname, :lastname, :phonenumber, :dob, :street, :city, :postalcode,
				profile_attributes: [:id, :user_id, :firstname, :lastname, :phonenumber, :dob, :street, :city, :postalcode],
				subscriptions_attributes: [:id, :user_id, :business_id, :status, :active, :role, :gdrive_url, :gdrive_updated_at]
			)
		end

		def correct_user
			@user = User.find(params[:id])
			redirect_to(root_url) unless logged_in? && (current_user?(@user) || current_user.admin?)
		end

		def google_authenticate(overwrite = false)
			if !current_user.google_auth
				current_user.google_auth = GoogleAuth.create
			end

			if !current_user.google_auth.auth_valid? || overwrite
					redirect_to "/auth/google_oauth2", id: "sign_in"
			end
		end
end
