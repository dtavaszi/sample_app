class BusinessesController < ApplicationController
  protect_from_forgery with: :exception, except: [:view_available_groupsessions_embedded]

  after_action :verify_authorized, except: [:welcome]

  before_action :logged_in_user, except: [:welcome, :home, :view_available_groupsessions_embedded]
  before_action :set_business, only: [:show, :calendar, :edit, :update, :destroy, :home, :dashboard, :view_available_groupsessions_embedded, :get_month]
  before_action :google_authenticate, :last_seen, only: [:show, :calendar]
  before_action :subscribed_client, only: [:show]

  before_action :verify_client_rates

  START_DAY = :sunday
  def sharks #temporary
  end

  def view_available_groupsessions_embedded
  end

  def employee_workouts date
    WorkoutSession.where(business: @business, eventtype: WorkoutSession.instance_eventtypes_array, date: date.beginning_of_month..date.end_of_month).includes( :group, user_workouts: [ user: [:profile]])
  end

  def client_workouts_ids date, eventtype
    UserWorkout.includes(:workout_session, user: [:profile]).where(user: current_user).where("workout_sessions.business_id" => @business.id, "workout_sessions.date" => date.beginning_of_month.beginning_of_week.beginning_of_day..date.end_of_month.end_of_week.end_of_day, "workout_sessions.eventtype" => eventtype).pluck(:workout_session_id)
  end

  def client_workouts_partakes date, ws_ids
    WorkoutSession.where(id: ws_ids).includes(user_workouts: [ user: [:profile]])
  end

  def client_workouts_unavailable date, ws_ids, eventtype
    WorkoutSession.where(business: @business, date: date.beginning_of_month.beginning_of_week.beginning_of_day..date.end_of_month.end_of_week.end_of_day, eventtype: eventtype).where('date > ?', Time.zone.now).where.not(:id => ws_ids).includes(user_workouts: [ user: [:profile]])
  end

  def repeating_events date
    WorkoutSession.where(:business_id => @business, :eventtype => WorkoutSession.recurring_eventtypes_array).where("workout_sessions.date < ?", date.end_of_month.end_of_week.end_of_day).includes( group: [:group_exceptions], user_workouts: [ user: [:profile]])
  end

  def get_month
    authorize @business

    date = parse_week_date
    re = repeating_events(date)

    if @business.view_as_employee?(current_user)
      ws = employee_workouts(date)
    else
      ws_private_ids = client_workouts_ids(date, WorkoutSession.private_eventtypes_array)
      ws = client_workouts_partakes(date, ws_private_ids)
      xws = client_workouts_unavailable(date, ws_private_ids, WorkoutSession.private_eventtypes_array)

      ws_group_ids = client_workouts_ids(date, WorkoutSession.groupworkout)
      ws_group = client_workouts_partakes(date, ws_group_ids)
      xws_group = client_workouts_unavailable(date, ws_group_ids, WorkoutSession.groupworkout)
    end

    if params[:token]
      updated_since = Time.zone.strptime(params[:token], "%s")

      ws = ws.where("updated_at > ?", updated_since) if ws
      ws_group = ws_group.where("updated_at > ?", updated_since) if ws_group
      xws = xws.where("updated_at > ?", updated_since) if xws
      xws_group = xws.where("updated_at > ?", updated_since) if xws_group
      re = re.where("updated_at > ?", updated_since) if re
    end

    only = [ :id, :date, :duration, :status, :bfr_from, :bfr_to, :title, :eventtype]
    xonly = [ :id, :date, :duration, :bfr_from, :bfr_to, :eventtype]
    wsg_only = [ :id, :date, :duration, :status, :bfr_from, :bfr_to, :title, :eventtype]

    uw_only = []
    xuw_only = []
    group_uw_only = [:user_id]

    u_only = [:email]
    xu_only = []

    p_only = [:firstname, :lastname]
    xp_only = []

    g_only = [:id, :name, :repeat_on, :repeat_type, :repeat_until]
    xg_only = []

    ge_only = [:date]
    xge_only = []

    if ws.any?
      token = ws.order(:updated_at).first.updated_at.strftime("%s")
    end

    if ws_group && ws_group.any?
      xtoken = ws_group.order(:updated_at).first.updated_at.strftime("%s")
      token = xtoken if (token && (xtoken > token)) || !token
    end

    if xws && xws.any?
      xtoken = xws.order(:updated_at).first.updated_at.strftime("%s")
      token = xtoken if (token && (xtoken > token)) || !token
    end

    if xws_group && xws_group.any?
      xtoken = xws_group.order(:updated_at).first.updated_at.strftime("%s")
      token = xtoken if (token && (xtoken > token)) || !token
    end

    if re && re.any?
      xtoken = re.order(:updated_at).first.updated_at.strftime("%s")
      token = xtoken if (token && (xtoken > token)) || !token
    end

    response = []
    if token
      if ws && ws.any?
        response = ws.as_json(include: { group: { only: g_only }, user_workouts: { include: { user: { include: { profile: { only: p_only } }, only: u_only} }, only: uw_only}}, only: only)
      end

      if ws_group && ws_group.any?
        response = response + ws_group.as_json(include: { group: { only: g_only }, user_workouts: { only: group_uw_only}}, only: only)
      end

      if xws && xws.any?
        response = response + xws.as_json(only: xonly)
      end

      if xws_group && xws_group.any?
        response = response + xws_group.as_json(include: { group: { only: g_only }, user_workouts: { only: group_uw_only}}, only: only)
      end

      if re && re.any?
        response = response + re.as_json(include: { user_workouts: { only: [:id] }, group: { include: { group_exceptions: { only: ge_only }}, only: g_only }})
      end

      if response
        token_hash = { "token" => token }
        response = response << token_hash.as_json
      end
    end

    respond_to do |format|
      format.json { render json: response, status: :ok }
    end
  end

  def home
  end

  def welcome
  end

  # GET /businesses
  # GET /businesses.json
  def index
    authorize Business.new

    @businesses = Business.all
  end

  def dashboard
    authorize @business

    @date_range = Time.zone.now.beginning_of_month - 2.months..Time.zone.now.end_of_month

    @workout_sessions = WorkoutSession.where(:business_id => @business, :date => @date_range).reorder(:date).includes(:reminders, user_workouts: [ user: [:profile]] )
    @reminders = current_user.reminders
    @reschedules = Reschedule.where(:workout_session_id => @workout_sessions.pluck(:id)).or(Reschedule.where(:link => @workout_sessions.pluck(:id)))

    @current_month = nil
    @monthly_sessions = 0
    @monthly_cost = 0
    @monthly_paid = 0
    @monthly_difference = 0

    if params[:user]
      @user = User.find(params[:user])
      @invoices = Invoice.where(:user_id => @user.id, :business_id => @business.id).includes(user: [:profile]).order(date: :desc)
      @user_workouts = UserWorkout.where(:user_id => @user.id)
    else
      @invoices = Invoice.where(:business_id => @business.id).includes(user: [:profile]).order(date: :desc)
      @user_workouts = UserWorkout.all
    end
  end # employee

  # GET /businesses/1
  # GET /businesses/1.json


  def show
    authorize @business

    if false
      @date = parse_month_date
      @start_date = @date.beginning_of_month.beginning_of_week(START_DAY).beginning_of_day
      @end_date = (@start_date + 42.days).end_of_day

      @for_user = current_user
      @users = @business.users.includes(:profile)

      @workout_session = @for_user.workout_sessions.build if logged_in?

      @reminder = @for_user.reminders.build if @business.view_as_employee?(@for_user)

      @workout_sessions = WorkoutSession.where(:business_id => @business, :date => @start_date..@end_date, :eventtype => WorkoutSession.instance_eventtypes_array).includes(:reminders, user_workouts: [ :invoice, user: [:profile] ])
      @repeating_events = WorkoutSession.where(:business_id => @business, :eventtype => WorkoutSession.recurring_eventtypes_array).where("workout_sessions.date < ?", @date.end_of_month.end_of_week.end_of_day).includes( :reminders, user_workouts: [ :invoice, user: [:profile]])

      @reminders = @for_user.reminders.where(:workout_session_id => nil, :date => @start_date..@end_date)

      @reschedules = Reschedule.where(:workout_session_id => @workout_sessions.pluck(:id)).or(Reschedule.where(:link => @workout_sessions.pluck(:id)))

  		get_client_sessions
    else
      redirect_to businesses_calendar_path(@business.slug)
    end
  end # subscribed user

  def get_client_sessions
		unless @business.view_as_employee?(@for_user)
      @user_partakes_in = {}

			user_session_id = UserWorkout.joins(:workout_session).where(:user_id => @for_user.id, "workout_sessions.date" => @start_date..@end_date).pluck(:workout_session_id)
			user_session_id.each do |id|
				@user_partakes_in[id] = ""
			end
		end
  end

  def calendar
    authorize @business

    @workout_session = current_user.workout_sessions.build
    @reminder = current_user.reminders.build
    @reminders = current_user.reminders

    @mode = 1
    @mode_url = "m"
    if params[:mode] && params[:mode] == "w"
      @mode = 2
      @mode_url = "w"
    end

    @date = parse_week_date.beginning_of_week(:sunday)
  end

  def parse_month_date
    if params[:date]
      begin
        Time.strptime(params[:date], "%Y-%m")
      rescue ArgumentError
        Time.zone.now
      end
    else
      Time.zone.now
    end
  end

  def parse_week_date
    if params[:date]
      begin
        Time.strptime(params[:date], "%Y-%m-%d")
      rescue ArgumentError
        Time.zone.now
      end
    else
      Time.zone.now
    end
  end

  def parse_time
    if params[:date]
      begin
        Time.parse(params[:date])
      rescue ArgumentError
        Time.zone.now
      end
    else
      Time.zone.now
    end
  end

  # GET /businesses/new
  def new
    @business_new = Business.new

    authorize @business_new
  end

  # GET /businesses/1/edit
  def edit
    authorize @business

    @tab = params[:tab]
  end

  # POST /businesses
  # POST /businesses.json
  def create
    @business = Business.new(business_params)
    authorize @business

    respond_to do |format|
      if @business.save
        format.html { redirect_to @business, notice: 'Business was successfully created. Fantastic!' }
        format.json { render :show, status: :created, location: @business }
      else
        format.html { render :new }
        format.json { render json: @business.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /businesses/1
  # PATCH/PUT /businesses/1.json
  def update
    authorize @business

    respond_to do |format|
      if @business.update(business_params)
        format.html { redirect_to :back, notice: 'Business was successfully updated. Nice one!' }
        format.json { render :show, status: :ok, location: @business }
      else
        format.html { render :back }
        format.json { render json: @business.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /businesses/1
  # DELETE /businesses/1.json
  def destroy
    authorize @business

    @business.destroy
    respond_to do |format|
      format.html { redirect_to businesses_url, notice: 'Business was successfully destroyed. Goodbye!' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_business
      if Business.friendly.exists? (params[:id])
        @business = Business.friendly.find(params[:id])
      else
        not_found
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def business_params
      params.require(:business).permit(:user_id, :name, :slug, :email, :session_type, :description, :phonenumber, :address, :website_url, :fb_url, :instagram_url, :color, :avatar)
    end

    def last_seen
      current_user.update_attribute(:last_seen, Time.zone.now)
    end

    def subscribed_client
      unless @business.subscribed_user?(current_user.id) || @business.view_as_employee?(current_user)
        redirect_to root_url(@business), notice: "Sorry! You need to be an approved subscriber to view."
      end
    end

    def google_authenticate(overwrite = false)
  		if @business.view_as_employee?(current_user)
  			if !current_user.google_auth
  				current_user.google_auth = GoogleAuth.create
  			end

  			if !current_user.google_auth.auth_valid? || overwrite
  					redirect_to "/auth/google_oauth2", id: "sign_in"
  			end
  		end
  	end

    def verify_client_rates
      if current_user && @business && current_user == @business.user
        uruid = UserRate.where(:business => Business.first.id).pluck(:user_id).uniq
        uids = Business.first.active_users.where.not(:id => uruid).where.not(:id => current_user.id)

        if uids.any?

          out = ""
          uids.each do |u|
            out += "<a href='#{ business_settings_path(@business.slug) + "/?tab=billing&user=#{ u.id }" }'> #{ u.shortname } </a>"
            out += ", " if u != uids.last
          end

          flash[:danger] = "The following clients need their session rates set: #{out}"
        end
      end
    end
end
