class SubscriptionsController < ApplicationController
  before_action :set_subscription, only: [:show, :edit, :update, :destroy, :unsubscribe, :approve, :deactivate]
  before_action :set_business, only: [:subscribe]
  before_action :set_user, only: [:subscribe]
  # GET /subscriptions
  # GET /subscriptions.json
  def index
    @subscriptions = Subscription.all
  end

  # GET /subscriptions/1
  # GET /subscriptions/1.json
  def show
  end

  # GET /subscriptions/new
  def new
    @subscription = Subscription.new
  end

  # GET /subscriptions/1/edit
  def edit
  end

  def subscribe
    if Subscription.subscription_exists?(@user.id, @business.id)
      if Subscription.resubscribe(@user.id, @business.id)
        respond_to do |format|
          format.html { redirect_to :back, notice: 'Subscription was successfully created.' }
          format.json { render :show, status: :created, location: @subscription }
        end
      else
        format.html { redirect_to :back }
        format.json { render json: @subscription.errors, status: :unprocessable_entity }
      end
    else
      @subscription = Subscription.new(:user_id => @user.id, :business_id => @business.id, :status => Subscription.status_request)

      respond_to do |format|
        if @subscription.save
          format.html { redirect_to :back, notice: 'Subscription was successfully created.' }
        else
          format.html { redirect_to :back }
        end
      end
    end
  end

  def approve
    @subscription.approve
    redirect_to :back
  end

  def deactivate
    @subscription.deactivate
    redirect_to :back
  end

  def unsubscribe
    @subscription.unsubscribe
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Unsubscribed successfully.' }
      format.json { head :no_content }
    end
  end

  # POST /subscriptions
  # POST /subscriptions.json
  def create
    @subscription = Subscription.new(subscription_params)

    respond_to do |format|
      if @subscription.save
        format.html { redirect_to @subscription, notice: 'Subscription was successfully created.' }
        format.json { render :show, status: :created, location: @subscription }
      else
        format.html { render :new }
        format.json { render json: @subscription.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /subscriptions/1
  # PATCH/PUT /subscriptions/1.json
  def update
    respond_to do |format|
      if @subscription.update(subscription_params)
        format.html { redirect_to :back, notice: 'Subscription was successfully updated.' }
        format.json { render :show, status: :ok, location: @subscription }
      else
        format.html { redirect_to :back }
        format.json { render json: @subscription.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /subscriptions/1
  # DELETE /subscriptions/1.json
  def destroy
    @subscription.destroy
    respond_to do |format|
      format.html { redirect_to root_url, notice: 'Subscription was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_business
      @business = Business.friendly.find(params[:business_id])
    end

    def set_user
      @user = User.find(params[:user_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_subscription
      @subscription = Subscription.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def subscription_params
      params.require(:subscription).permit(:user_id, :business_id, :type, :membership_id)
    end
end
