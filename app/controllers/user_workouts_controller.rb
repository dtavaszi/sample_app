class UserWorkoutsController < ApplicationController
	#before_action :admin_user

	def update
		@uw = UserWorkout.find(params[:id])
		authorize @uw

		@cost = params[:user_workout][:rate_dollars].to_f + (params[:user_workout][:rate_cents].to_f)

		@uw.update_attribute(:cost, @cost)
		redirect_to :back
	end

	def edit
    @uw = UserWorkout.find(params[:id])
		authorize @uw

		respond_to do |format|
			format.js
		end
  end

	def setoptions
		business = Business.find(params[:business_id])
		authorize business.user_workouts.new

		if params[:commit] == "Remove from Invoice"
			UserWorkout.where(:id => params[:user_workout_ids]).update_all(:invoice_id => params[:invoice_id])
		elsif params[:commit] == "Add to Invoice"
			uw = UserWorkout.where(:id => params[:user_workout_ids])

			if uw.first
				user = UserWorkout.where(:id => params[:user_workout_ids]).first.user
				uw.update_all(:invoice_id => params[:invoice_id])
			end
		elsif params[:commit] == "Apply Custom Rate"
			uw = UserWorkout.where(:id => params[:user_workout_ids])

			if uw.first
				user = UserWorkout.where(:id => params[:user_workout_ids]).first.user
				rate = custom_rate
				uw.update_all(:cost => rate)
			end
		elsif params[:commit] == "Apply Default Rate"
			uw = UserWorkout.where(:id => params[:user_workout_ids])

			if uw.first
				user = UserWorkout.where(:id => params[:user_workout_ids]).first.user
				rate = user.rate
				uw.update_all(:cost => rate)
			end
		elsif params[:commit] == "Link Membership"
			UserWorkout.where(:id => params[:user_workout_ids]).update_all(:membership => true)
		elsif params[:commit] == "Unlink Membership"
			UserWorkout.where(:id => params[:user_workout_ids]).update_all(:membership => false)
		elsif params[:commit] == "Apply Default Tax"
			UserWorkout.where(:id => params[:user_workout_ids]).each do |uw|
				if (tax_constant = Constant.where(:table => "workout_sessions", :references => "taxes")).exists?
					uw.workout_session.update_attribute(:tax_id, tax_constant.first.value)
				end
			end
		elsif params[:commit] == "Apply Custom Tax"
			UserWorkout.where(:id => params[:user_workout_ids]).each do |uw|
				uw.workout_session.update_attribute(:tax_id, custom_tax)
			end
		end

		redirect_to :back
	end

	private

		def custom_rate
			params[:invoice_cost][:custom_rate] ? (params[:invoice_cost][:custom_rate].to_f) : 0
		end

		def custom_tax
			params[:invoice_cost][:tax_id] ? (params[:invoice_cost][:tax_id].to_i) : nil
		end

end
