class UserRatesController < ApplicationController
  before_action :set_business
  after_action :verify_authorized

  def index
    authorize @business.rates.new
    @rates = Rate.where(:business_id => @business.id)
  end

	def create
		@user = User.find(params[:user_rate][:uid])
		rate = params[:user_rate][:rate]

    authorize @business.user_rates.new

		@user_rate = UserRate.new(:rate => rate)
		@user_rate.user = @user
    @user_rate.business = @business

		@user_rate.save

    flash[:success] = "User rate has been updated. KA-CHING!"
		redirect_to :back
	end

  private
    def set_business
      @business = Business.friendly.find(params[:business_id])
    end

    def user_rate_params
      params.require(:user_rate).permit(:rate, :uid, :business_id)
    end

end
