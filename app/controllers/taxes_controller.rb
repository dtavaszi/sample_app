class TaxesController < ApplicationController
  after_action :verify_authorized

  #before_action :admin_user

  before_action :set_tax, only: [:show, :edit, :update, :destroy]
  before_action :set_business, only: [:new, :index, :create, :edit]

  # GET /taxes
  # GET /taxes.json
  def index
    @taxes = Tax.all
    authorize @business.taxes.new

  	@workouts_tax = Constant.default_workout_sessions_tax @business
  	@memberships_tax = Constant.default_membership_tax @business
  end

  # GET /taxes/1
  # GET /taxes/1.json
  def show
    authorize @tax
  end

  # GET /taxes/new
  def new
    @tax = @business.taxes.new
    authorize @tax
  end

  # GET /taxes/1/edit
  def edit
    authorize @tax
  end

  # POST /taxes
  # POST /taxes.json
  def create
    @tax = @business.taxes.new(tax_params)
    authorize @tax

    respond_to do |format|
      if @tax.save
        format.html { redirect_to business_taxes_path(@business.slug), notice: 'Tax was successfully created.' }
        format.json { render :taxes, status: :created }
      else
        format.html { render :new }
        format.json { render json: @tax.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /taxes/1
  # PATCH/PUT /taxes/1.json
  def update
    authorize @tax

    respond_to do |format|
      if @tax.update(tax_params)
        format.html { redirect_to business_taxes_path(@business.slug), notice: 'Tax was successfully updated.' }
        format.json { render :taxes, status: :ok }
      else
        format.html { render :edit }
        format.json { render json: @tax.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /taxes/1
  # DELETE /taxes/1.json
  def destroy
    authorize @tax

    @tax.destroy
    respond_to do |format|
      format.html { redirect_to business_taxes_path(@business.slug), notice: 'Tax was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_business
      @business = Business.friendly.find(params[:business_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_tax
      @tax = Tax.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tax_params
      params.require(:tax).permit(:business_id, :name, :percentage, :available)
    end
end
