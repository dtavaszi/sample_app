class SessionsController < ApplicationController
	protect_from_forgery with: :exception, except: [:new_embedded, :create]
	before_action :redirect_to_home?, only: [:create, :new]
	before_action :set_business, only: [:new, :new_embedded, :create]

	def new_embedded
		respond_to do |format|
			format.js {}
		end
	end

  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)

		respond_to do |format|
	    if user && user.authenticate(params[:session][:password])
	      log_in user
	      remember(user)

				format.html {
					if @business
			      redirect_back_or @business
					else
						redirect_back_or root_url
					end
				}

				format.js { render 'businesses/view_available_groupsessions_embedded.js.erb' }
	    else
				format.html {
		      flash.now[:danger] = 'Invalid email/password combination'
		      render 'new'
				}
	    end
		end
  end

	def google_authorize
		auth = env["omniauth.auth"]
		ga = current_user.google_auth
		ga.provider = auth.provider
		ga.uid = auth.uid
		ga.oauth_token = auth.credentials.token
		ga.oauth_expires_at = Time.at(auth.credentials.expires_at)
		ga.save!

		if logged_in? && current_user.business
			redirect_to current_user.business
		else
			redirect_to businesses_path
		end
	end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end

	def redirect_to_home?
		if logged_in?
			redirect_to root_url
		end
	end

	private
		def set_business
			if Business.friendly.exists? (params[:business_id])
				@business = Business.friendly.find(params[:business_id])
			end
		end
end
