class ItemsController < ApplicationController
	after_action :verify_authorized

	#before_action :admin_user
	before_action :set_item, only: [:show, :edit, :update, :destroy]
	before_action :set_business, only: [:new, :index, :create, :edit, :destroy]

	# GET /items
	# GET /items.json
	def index
		@items = @business.items
		authorize @business.items.new
	end

	# GET /items/1
	# GET /items/1.json
	def show
		authorize @item
	end

	# GET /items/new
	def new
		@item = @business.items.new
		authorize @item
	end

	# GET /items/1/edit
	def edit
		authorize @item
	end

	# POST /items
	# POST /items.json
	def create
		@item = @business.items.new(item_params)

		authorize @item

		respond_to do |format|
			if @item.save
				format.html { redirect_to :back, notice: 'Item was successfully created.' }
				format.json { render :items, status: :created, location: @item }
			else
				format.html { render :back }
				format.json { render json: @item.errors, status: :unprocessable_entity }
			end
		end
	end

	# PATCH/PUT /items/1
	# PATCH/PUT /items/1.json
	def update
		authorize @item

		respond_to do |format|
			if @item.update(item_params)
				format.html { redirect_to business_items_path(@business.slug), notice: 'Item was successfully updated.' }
				format.json { render :items, status: :ok, location: @item }
			else
				format.html { render :edit }
				format.json { render json: @item.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /items/1
	# DELETE /items/1.json
	def destroy
		authorize @item

		@item.destroy
		respond_to do |format|
			format.html { redirect_to business_items_path(@business.slug), notice: 'Item was successfully destroyed.' }
			format.json { head :no_content }
		end
	end

	private
		def set_business
			@business = Business.friendly.find(params[:business_id])
		end

		# Use callbacks to share common setup or constraints between actions.
		def set_item
			@item = Item.find(params[:id])
		end

		# Never trust parameters from the scary internet, only allow the white list through.
		def item_params
			params.require(:item).permit(:name, :business_id, :description, :category, :quantity, :available, :price, :tax_id)
		end
end
