class PaymentsController < ApplicationController
  after_action :verify_authorized

  before_action :set_payment, only: [:show, :edit, :update, :destroy]
  before_action :set_business, only: [:new, :index, :create, :edit]

  #before_action :admin_user

  # GET /payments
  # GET /payments.json
  def index
    @payments = @business.payments.where.not(:id => nil)
    authorize @business.payments.new
  end

  # GET /payments/1
  # GET /payments/1.json
  def show
    authorize @payment
  end

  # GET /payments/new
  def new
    authorize @payment

		@invoice = Invoice.find(params[:invoice])
    @payment = @business.payments.new
  end

  # GET /payments/1/edit
  def edit
    authorize @payment
  end

  # POST /payments
  # POST /payments.json
  def create
    @payment = @business.payments.new(payment_params)
		@payment.paydate = convert_to_date(params[:payment][:paydate]) if params[:payment][:paydate]

    authorize @payment

    respond_to do |format|
      if @payment.save
        format.html { redirect_to :back }
        format.json { redirect_to business_payment_path(@business.slug, @payment), notice: "Payment created. Yay money!" }
      else
        format.html { render :back }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /payments/1
  # PATCH/PUT /payments/1.json
  def update
    authorize @payment

		@invoice = @payment.invoice
		@payment.paydate = convert_to_date(params[:payment][:paydate]) if params[:payment][:paydate]
    respond_to do |format|
      if @payment.save && @payment.update(payment_params)
        format.html { redirect_to business_invoice_path(@business.slug, @invoice), notice: "Payment updated. Well done!" }
        format.json { render :show, status: :ok, location: @invoice }
      else
        format.html { render :edit }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payments/1
  # DELETE /payments/1.json
  def destroy
    authorize @payment

    @business = @payment.business

		@invoice = @payment.invoice
    @payment.update_attribute(:invoice_id, nil)

    respond_to do |format|
      format.html { redirect_to business_invoices_path(@business.slug), notice: "Payment destroyed. A record will be kept in the payments section." }
      format.js
    end
  end

  private
    def set_business
      @business = Business.friendly.find(params[:business_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_payment
      @payment = Payment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payment_params
      params.require(:payment).permit(:payment_type, :memo, :amount, :user_id, :invoice_id)
    end

		def convert_to_date date
			Date.strptime(date, "%m/%d/%Y")
		end
end
