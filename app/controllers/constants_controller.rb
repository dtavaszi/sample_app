class ConstantsController < ApplicationController
  after_action :verify_authorized

  before_action :set_constant, only: [:show, :edit, :update, :destroy]
  before_action :set_business, only: [:new, :index, :create, :edit, :update]
  #before_action :admin_user

  # GET /constants
  # GET /constants.json
  def index
    authorize @business.constants.new
    @constants = Constant.where(:business_id => @business_id)
  end

  # GET /constants/1
  # GET /constants/1.json
  def show
    authorize @constant
  end

  # GET /constants/new
  def new
    @constant = @business.constants.new
    authorize @constant
  end

  # GET /constants/1/edit
  def edit
    authorize @constant
  end

  # POST /constants
  # POST /constants.json
  def create
    @constant = @business.constants.new(constant_params)
    authorize @constant

    respond_to do |format|
      if @constant.save
        format.html { redirect_to business_taxes_path(@business.slug), notice: 'Constant was successfully created. Welcome to this world, constant.' }
        format.json { render :taxes, status: :created, location: @constant }
      else
        format.html { render :new }
        format.json { render json: @constant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /constants/1
  # PATCH/PUT /constants/1.json
  def update
    authorize @constant

    respond_to do |format|
      if @constant.update(constant_params)
        format.html { redirect_to business_taxes_path(@business.slug), notice: 'Constant was successfully updated. About time!' }
        format.json { render :taxes, status: :ok, location: @constant }
      else
        format.html { render :edit }
        format.json { render json: @constant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /constants/1
  # DELETE /constants/1.json
  def destroy
    authorize @constant

    @business = @constant.business
    @constant.destroy
    respond_to do |format|
      format.html { redirect_to business_taxes_path(@business.slug), notice: 'Constant was successfully destroyed. Take that, constant!' }
      format.json { head :no_content }
    end
  end

  private
    def set_business
      @business = Business.friendly.find(params[:business_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_constant
      @constant = Constant.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def constant_params
      params.require(:constant).permit(:value, :table, :business_id)
    end
end
