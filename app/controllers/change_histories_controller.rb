class ChangeHistoriesController < ApplicationController
  before_action :set_business, only: [:mark_all_read]

  def mark_all_read
    @business.change_histories.update_all(read: true)

    respond_to do |format|
      format.html { redirect_to :back }
      format.js { }
    end
  end

  def show
    respond_to do |format|
      format.html { redirect_to :back, notice: "Not yet implemented." }
      format.js { }
    end
  end

  private
    def change_history_params
      params.require(:change_history).permit(:business_id, :id)
    end

    def set_business
      @business = Business.friendly.find(params[:business_id])
    end
end
