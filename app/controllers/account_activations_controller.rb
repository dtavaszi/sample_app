class AccountActivationsController < ApplicationController

  def send_activation
    current_user.create_activation_digest
    current_user.save
    ApplicationMailer.activation_email(current_user).deliver_later

    flash[:info] = "Activation email sent to #{ current_user.email }."
    redirect_to root_url
  end

  def activate
    user = User.find_by(email: params[:email])
    if user && user.activated?
      flash[:info] = "This account has already been activated"
    elsif user && !user.activated? && user.authenticated?(:activation, params[:id])
      user.update_attributes(:activated => true, :activated_at => Time.zone.now)
      user.save
      log_in user
      flash[:success] = "Email verified. Well done!"
    else
      flash[:danger] = "Invalid activation link"
    end

    redirect_to root_url
  end

  def blacklist
  end

end
