class MembershipsController < ApplicationController
  after_action :verify_authorized

  before_action :set_membership, only: [:show, :edit, :update, :destroy]
  before_action :set_business, only: [:new, :index, :create, :edit]
  #before_action :admin_user

  # GET /memberships
  # GET /memberships.json
  def index
    authorize @business.memberships.new
    @memberships = Membership.where(:business_id => @business)
  end

  # GET /memberships/1
  # GET /memberships/1.json
  def show
    authorize @membership
  end

  # GET /memberships/new
  def new
    @membership = @business.memberships.new
    authorize @membership
  end

  # GET /memberships/1/edit
  def edit
    authorize @membership
  end

  # POST /memberships
  # POST /memberships.json
  def create
    @membership = @business.memberships.new(membership_params)
    authorize @membership

    respond_to do |format|
      if @membership.save
        format.html { redirect_to :back, notice: 'Membership was successfully created. ' }
        format.json { render :memberships, status: :created }
      else
        format.html { render :new }
        format.json { render json: @membership.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /memberships/1
  # PATCH/PUT /memberships/1.json
  def update
    authorize @membership

    respond_to do |format|
      if @membership.update(membership_params)
        format.html { redirect_to business_memberships_path(@membership.business.slug), notice: 'Membership was successfully updated.' }
        format.json { render :memberships, status: :ok }
      else
        format.html { render :back }
        format.json { render json: @membership.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /memberships/1
  # DELETE /memberships/1.json
  def destroy
    authorize @membership

    business = @membership.business
    @membership.destroy
    respond_to do |format|
      format.html { redirect_to business_memberships_path(business.slug), notice: 'Membership was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_business
      @business = Business.friendly.find(params[:business_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_membership
      @membership = Membership.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def membership_params
      params.require(:membership).permit(:name, :description, :available, :cost, :tax_id, :max_workouts, :max_transfers, :available_from, :available_to)
    end
end
