class InvoicesController < ApplicationController
	after_action :verify_authorized

	before_action :set_invoice, only: [:show, :edit, :update, :destroy, :set_processed, :set_unprocessed, :settle_month, :reconcile, :pay_with_credit]
	#before_action :admin_user, except: :show
	before_action :correct_user, only: [:show]
	before_action :set_business, only: [:new, :index, :create, :show, :edit, :destroy]

	# GET /invoices
	# GET /invoices.json
	def index
		authorize @business.invoices.new

		@date = parse_date
		start_date = @date.beginning_of_month.beginning_of_day
    end_date = start_date.end_of_month.end_of_day
		@date_range = start_date..end_date

		@current_month = nil
		@monthly_sessions = 0
		@monthly_cost = 0
		@monthly_paid = 0
		@monthly_difference = 0

		@invoices = Invoice.where(:business_id => @business.id).includes(user: [:profile]).order(date: :desc)
		@invoices_this_month = Invoice.where(:business_id => @business.id, :date => @date_range).includes(user: [:profile]).order(date: :desc)

		@user_workouts = UserWorkout.all
	end

	# GET /invoices/1
	# GET /invoices/1.json
	def show
		authorize @invoice

		@reschedules = Reschedule.all
		@workout_sessions = WorkoutSession.all
		@items = Item.where(:available => true).where("quantity > ?", 0).or(Item.where(:available => true).where(:quantity => nil))
		@invoice_items = @invoice.invoice_items.includes(:item)

		@finalized_workouts = UserWorkout.includes(:workout_session).where(:invoice_id => @invoice.id).where.not("workout_sessions.status = ?", WorkoutSession.rescheduled).order("workout_sessions.date")
		@selected_workouts = UserWorkout.includes(:workout_session).where(:invoice_id => @invoice.id).where.not("workout_sessions.status = ?", WorkoutSession.rescheduled).order("workout_sessions.date")
		@available_workouts = UserWorkout.includes(:workout_session).where(:user => @invoice.user, :invoice_id => nil).where.not("workout_sessions.status = ?", WorkoutSession.rescheduled).order("workout_sessions.date")

		@balance = @invoice.balance
		@subtotal = @invoice.total_cost(false)
		@total_tax = @invoice.total_tax
		@total = @invoice.total_cost

		#InvoiceMonth for show_controls
		date = @invoice.date.beginning_of_month
		@im = InvoiceMonth.find_by(:date => date, :business => @business) || InvoiceMonth.create(:date => date, :business => @business)
	end

	# GET /invoices/new
	def new
		authorize @business.invoices.new
		@invoice = Invoice.new
		@selected = params[:user]
	end

	# GET /invoices/1/edit
	def edit
		authorize @invoice
	end

	# POST /invoices
	# POST /invoices.json
	def create
		@invoice = Invoice.new(invoice_params)
		@invoice.business = @business
		@invoice.date = convert_to_date(params[:invoice][:date])

		authorize @invoice

		respond_to do |format|
			if @invoice.save
				format.html { redirect_to business_invoice_path(@business.slug, @invoice), notice: "Invoice created" }
				format.json { render :show, status: :created, location: @invoice }
			else
				format.html { render :new }
				format.json { render json: @invoice.errors, status: :unprocessable_entity }
			end
		end
	end

	# PATCH/PUT /invoices/1
	# PATCH/PUT /invoices/1.json
	def update
		authorize @invoice
		@invoice.date = convert_to_date(params[:invoice][:date]) if params[:invoice][:date]

		respond_to do |format|
			if @invoice.save && @invoice.update(invoice_params)
				@invoice.adjust_invoice if invoice_params[:membership_id]

				format.html { redirect_to :back }
				format.json { render :show, status: :ok, location: @invoice }
			else
				format.html { render :back }
				format.json { render json: @invoice.errors, status: :unprocessable_entity }
			end
		end
	end

  # DELETE /invoices/1
  # DELETE /invoices/1.json
	def destroy
		authorize @invoice

		@invoice.user_workouts.update_all(:invoice_id => nil)
		@invoice.destroy
		respond_to do |format|
			format.html { redirect_to business_invoices_url(@business.slug) }
			format.json { head :no_content }
		end
	end

	def reconcile
		authorize @invoice
		if @invoice.reconcile!
			if @invoice.reconciled
				flash[:success] = "Invoice has been reconciled"
			else
				flash[:notice] = "Invoice reconciliation undone"
			end
		else
			flash[:info] = "Cannot reconcile before settling invoice"
		end

		redirect_to :back
	end

	def set_processed
		authorize @invoice
		if @invoice.settle
			flash[:success] = "Invoice settled"
		else
			flash[:danger] = "Balance must be positive to settle invoice"
		end
		redirect_to :back
	end

	def set_unprocessed
		authorize @invoice
		@invoice.reopen
		redirect_to :back
	end

	def settle_month
		authorize @invoice

		Invoice.where(:date => @invoice.date.beginning_of_month..@invoice.date.end_of_month).each do |i|
			 unless i.settle
				flash[:warning] = "Some invoices have a negative balance and could not be settled"
			end
		end
		redirect_to :back
	end

	def pay_with_credit
		authorize @invoice

		@invoice.pay_with_credit
		redirect_to :back
	end
  private
		def set_business
			@business = Business.friendly.find(params[:business_id])
		end

    # Use callbacks to share common setup or constraints between actions.
    def set_invoice
      @invoice = Invoice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def invoice_params
      params.require(:invoice).permit(:business_id, :id, :membership_id, :user_id, :processed, :amount, :title)
    end

		def correct_user
			user = @invoice.user
			redirect_to(root_url) unless logged_in? && (current_user?(user) || @invoice.business.view_as_employee?(current_user))
		end

		def parse_date
	    if params[:date]
	      begin
	        Time.strptime(params[:date], "%Y-%m").to_date.beginning_of_month
	      rescue ArgumentError
	        Time.zone.now.to_date.beginning_of_month
	      end
	    else
	      Time.zone.now.to_date.beginning_of_month
	    end
	  end
end
