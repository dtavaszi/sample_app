require 'google/apis/calendar_v3'

class WorkoutSessionsController < ApplicationController
  protect_from_forgery with: :exception, except: [:book_groupsession, :cancel_groupsession]

  before_action :logged_in_user_embedded, only: [:book_groupsession, :cancel_groupsession]
  before_action :logged_in_user, except: [:book_groupsession, :cancel_groupsession]
  before_action :set_business, only: [:index, :create, :show, :destroy]
  before_action :set_business_and_workout_session, only: [:update, :book_groupsession, :cancel_groupsession, :sync_google_event, :noshow, :cancel, :reschedule, :success, :approveallthismonth, :approve, :approveallfollowing, :remove, :removefollowing, :removeall, :unlink, :approveupto]

  DAYS_PATTERN = "0" * 7

  def index
    @workout_sessions = WorkoutSession.where(:business_id => @business.id).reorder(created_at: :desc)
    authorize @business.workout_sessions.new
  end

  def show
		@workout_session = WorkoutSession.find(params[:id])
    authorize @workout_session

		@ws = @workout_session.user.workout_sessions.build if logged_in?
		@reminders = @workout_session.reminders.where(:user => @current_user)
  end

	#eventtype = 0 - event; 1 - workout_session
  def create
    auth_ws = @business.workout_sessions.new
    auth_ws.date = convert_to_datetime(workout_session_params[:date])
    authorize auth_ws

    if params[:old_id].nil?
      create_sessions(workout_session_params)
    else
      @workout_session = find_old_event
      @workout_session.admin = @business.view_as_employee?(@current_user)

      if @workout_session.is_recurring_eventtype
        puts "recurring_eventtype"
        new_instance_or_update_for_repeating_event
      else
        if move_old_event?
          if ["Save", "This event only", "Request change"].include?(params[:commit])
            update_inner(@workout_session)
          else
            wss = WorkoutSession.where(:group_id => @workout_session.group_id)
            wss.update_all(location: workout_session_params[:location], memo: workout_session_params[:memo], title: workout_session_params[:title], bfr_from: workout_session_params[:bfr_from] || 0, bfr_to: workout_session_params[:bfr_to] || 0)
          end
        else
          new_rescheduled_event
        end
      end
    end

		redirect_to :back || root_url
  end

  def update
    authorize @workout_session

    respond_to do |format|
  		format.html do
        result = update_inner(@workout_session)
        if result
          flash[:success] = "Event updated"
        else
          flash[:danger] = "Sorry, looks like that time is busy"
        end

        redirect_to :back || root_url
      end
      format.js do
        status = @workout_session.update(workout_session_params)
        add_google_event @workout_session
        format.js { render js: status } # Send back status - whether successful or not
      end
    end
  end

  def destroy
		@workout_session = WorkoutSession.find(params[:id])
		@workout_session.admin = @business.view_as_employee?(@current_user)
    authorize @workout_session

		remove_google_event @workout_session
		@workout_session.delete

		flash[:info] = "Session deleted"

		redirect_to :back || root_url
  end

	def view
		@ws = WorkoutSession.find(params[:id])
    authorize @ws

    @date = params[:date]
    @business = @ws.business

    if @ws.user
      @ws_new = @ws.user.workout_sessions.build

      if @date && WorkoutSession.recurring_eventtypes_array.include?(@ws.eventtype)
        @ws_new.date = @ws.date
        @ws_new.set_start_date(@date)
      end
    end

		respond_to do |format|
      format.html {
        show
        render :show
      }
			format.js
		end
	end

	def add
		@workout_session = @current_user.workout_sessions.build if logged_in?
    authorize @workout_session

		respond_to do |format|
			format.js
		end
	end

  def book_groupsession
    authorize @workout_session
    @business = @workout_session.business

    if @workout_session.eventtype == WorkoutSession.recurring_groupworkout
      date = WorkoutSession.convert_to_date(params[:date])
      delta_date = @workout_session.duration - @workout_session.date
      start_date = @workout_session.date.change(year: date.year,
                                               month: date.month,
                                               day: date.day)

      ws_new = @business.workout_sessions.create(title: @workout_session.title,
                                                  business: @business,
                                                  memo: @workout_session.memo,
                                                  eventtype: -@workout_session.eventtype,
                                                  location: @workout_session.location,
                                                  date: start_date,
                                                  duration: start_date + delta_date,
                                                  tax_id: @workout_session.tax_id,
                                                  group_id: @workout_session.group_id)
      @workout_session = ws_new
      @workout_session.save
      @workout_session.add_group_exception date
    end

    unless @workout_session.sign_up(@current_user)
      flash[:info] = "You are already signed up for this class"
    end

    respond_to do |format|
      flash[:info] = "Class booked"
      format.html { redirect_to @workout_session.business }
      format.js { render 'businesses/view_available_groupsessions_embedded.js.erb' }
    end
  end

  def cancel_groupsession
    authorize @workout_session

    @business = @workout_session.business
    unless @workout_session.cancel_booking(@current_user)
      flash[:info] = "Class has already been cancelled"
    end

    respond_to do |format|
      flash[:info] = "Class cancelled"
      format.html { redirect_to @workout_session.business }
      format.js { render 'businesses/view_available_groupsessions_embedded.js.erb' }
    end
  end

### BEGIN ###
  def new_instance_or_update_for_repeating_event
    if params[:commit] == "This event only"
      new_instance_for_repeating_event
    elsif params[:commit] == "All future events"
      # Change the first section [----x
      date = WorkoutSession.convert_to_datetime(workout_session_params[:date])
      @workout_session.group.update_attribute(:repeat_until, date - 1.day)

      # Update the second section x----]
      recurring_events = WorkoutSession.where(:group => @workout_session.group_id, :eventtype => WorkoutSession.recurring_eventtypes_array)
      new_group = Group.create(:repeat_on => @workout_session.group.repeat_on, :repeat_type => @workout_session.group.repeat_type)
      puts "New group id: "  + new_group.id.to_s

      d1 = @workout_session.date
      d2 = date
      delta_date = (d2 - d1).to_i

      norm1 = d1

      recurring_events.each do |re|
        norm2 = re.date
        normalize_date = (norm2 - norm1).to_i
        new_event = @workout_session.dup
        new_event.update_details(workout_session_params)

        new_event.date = new_event.date + delta_date + normalize_date
        new_event.date = new_event.date + 7.days if re.date < @workout_session.date # if you have M W F, and you change W, then make M start the next week

        new_event.admin = @business.view_as_employee?(@current_user)
        new_event.group_id = new_group.id
        new_event.save
      end

      # Put all workout sessions past the x date in the new group
      wss = WorkoutSession.where(:group_id => @workout_session.group_id, :eventtype => WorkoutSession.instance_eventtypes_array).where("date >= ?", @workout_session.date)
      wss.update_all(group_id: new_group.id, location: workout_session_params[:location], memo: workout_session_params[:memo], title: workout_session_params[:title], bfr_from: workout_session_params[:bfr_from], bfr_to: workout_session_params[:bfr_to])
    else
      wss = WorkoutSession.where(:group_id => @workout_session.group_id)
      wss.update_all(location: workout_session_params[:location], memo: workout_session_params[:memo], title: workout_session_params[:title], bfr_from: workout_session_params[:bfr_from], bfr_to: workout_session_params[:bfr_to])
    end
  end

  def new_instance_for_repeating_event
    create_sessions(workout_session_params, @workout_session)
    @workout_session.add_group_exception WorkoutSession.convert_to_date(params[:old_date])
  end

  def new_rescheduled_event
    events_list = create_sessions(workout_session_params, @workout_session)
    @workout_session.update_attribute(:gid, nil)

    events_list.each do |event|
      copy_user_workouts(params[:old_id], event.id)
      reschedule(@workout_session, event.id)
    end
  end

  def find_old_event
    WorkoutSession.find(params[:old_id].to_i)
  end

  def new_event?
    return params[:old_id].nil?
  end

  def move_old_event?
     (trainer_and_update_event || client_and_session_pending_or_dates_unchanged)
  end

  def trainer_and_update_event
    @business.view_as_employee?(@current_user) && (params[:commit] != "Reschedule")
  end

  def client_and_session_pending_or_dates_unchanged
    @workout_session.draft? || event_dates_unchanged?
  end

  def event_dates_unchanged?
    @workout_session.event_dates_unchanged?(workout_session_params[:date], workout_session_params[:duration])
  end

## END ###

	def copy_user_workouts(id_old, id_new)
		uws = UserWorkout.where(:workout_session_id => id_old)

		uws.each do |uw|
			uw_new = UserWorkout.new(:workout_session_id => id_new, :user_id => uw.user_id, :cost => uw.cost, :invoice_id => uw.invoice_id)
			uw_new.save
		end
	end

  def initialize_session(wsp, old_ws = nil)
    ws = @current_user.workout_sessions.build(wsp)
    ws.business = @business
		ws.date = convert_to_datetime wsp[:date]
		ws.bfr_from = 0 if wsp[:bfr_from].nil?
		ws.bfr_to = 0 if wsp[:bfr_to].nil?
		ws.admin = @business.view_as_employee?(@current_user)
    ws.tax_id = default_workout_sessions_tax @business

		if wsp[:duration].nil? || !@business.view_as_employee?(@current_user)
			ws.duration = ws.date + 1.hour
		else
			ws.duration = convert_to_datetime wsp[:duration]
		end

		ws.location = wsp[:location] unless wsp[:location].blank?
    ws.title = wsp[:title] unless wsp[:title].blank?
		ws.status = WorkoutSession::STATUS["draft"] unless @business.view_as_employee?(@current_user)

    if ws.old_id
  		ws.old_id = old_ws.id
      ws.group_id = old_ws.group_id
    end

    return ws
  end

### MOVE ###

  def create_user_workouts_for_session(workout_session, workout_session_params)
    if workout_session_params[:clients]
      workout_session_params[:clients].each do |usr_id|
        workout_session.user_workouts.create(user_id: usr_id, cost: get_session_cost(workout_session.old_id, usr_id))
      end
    end

    unless @business.view_as_employee?(@current_user) || workout_session.old_id
      workout_session.user_workouts.create(user_id: @current_user.id, cost: get_session_cost(workout_session.old_id, @current_user.id))
    end
  end

### END ###

  def create_sessions(wsp, old_ws = nil)
		ws = initialize_session(wsp, old_ws)

    if create_repeating_sessions?(ws)
      events_list = create_repeating_sessions wsp, ws
    else
      events_list = create_single_session(wsp, ws)
    end

    save_events(events_list)
    return events_list
  end

  def save_events events_list
    events_list.each do |event|
      save_event event
    end
  end

  def update_event event
    event.approve_session if @business.view_as_employee?(@current_user)
    add_google_event event
  end

  def save_event event
    if event.save
      create_user_workouts_for_session event, workout_session_params
      event.approve_session if @business.view_as_employee?(@current_user)
      add_google_event event
    end
  end

  def create_single_session(wsp, ws)
    events_list = []
    events_list << ws if workout_in_future(ws)
  end

  def create_repeating_sessions?(ws)
    ws.repeat_on
  end

  def repeat_type_on(wsp, ws, index, group, events_list)
    start_date = ws.date + (index - ws.date.wday ).days
    duration_date = ws.duration + (index - ws.duration.wday).days
    start_date += 7.days if (index < ws.date.wday)
    duration_date += 7.days if (index < ws.date.wday)
    end_date = convert_to_date ws.until
    if dates_within_reason start_date, end_date
      while(start_date <= (end_date+1.day))
        events_list << new_workout(start_date, duration_date, ws, group)

        start_date += 7.days
        duration_date += 7.days
      end
    end

    group.repeat_until = end_date

    return events_list
  end

  def repeat_type_never(wsp, ws, index, group, events_list)
    start_date = ws.date + (index - ws.date.wday).days
    duration_date = ws.duration + (index - ws.duration.wday).days
    start_date += 7.days if (index < ws.date.wday)
    duration_date += 7.days if (index < ws.date.wday)

    w = new_workout(start_date, duration_date, ws, group)
    w.eventtype = -w.eventtype

    return events_list << w
  end

  def create_repeating_sessions(wsp, ws)
    events_list = []
		group = Group.create(:repeat_on => convertRepeatPattern(ws.repeat_on));
    @group_id = group.id

    if ws.repeattype == "on"
    	ws.repeat_on.each do |day|
        index = WorkoutSession::DAY_TO_INTEGER[day]
        repeat_type_on(wsp, ws, index, group, events_list)
      end
    elsif ws.repeattype == "never" && group.save
      ws.eventtype = -ws.eventtype
      ws.group = group
      events_list << ws
    end

    return events_list
  end

  def convertRepeatPattern(pattern)
    output = DAYS_PATTERN.dup
    pattern.each do |p|
      index = WorkoutSession::DAY_TO_INTEGER[p]
      output[index] = "1"
    end
    output
  end

  def new_workout(start_date, duration_date, ws, group)
    w = @current_user.workout_sessions.new(:date => start_date, :duration => duration_date)
    w.admin = ws.admin
    w.business_id = ws.business_id
    w.eventtype = ws.eventtype
    w.group_id = group.id
    w.title = ws.title
    w.location = ws.location
    w.memo = ws.memo
    w.status = WorkoutSession.draft unless @business.view_as_employee?(@current_user)
    w.bfr_from = ws.bfr_from
    w.bfr_to = ws.bfr_to
    w.gsync = ws.gsync
    return w
  end

	def update_inner workout_session
    workout_session.admin = @business.view_as_employee?(@current_user)

    if @business.view_as_employee?(@current_user)
      workout_session.update_all(workout_session_params)
			workout_session.update_user_workouts workout_session_params
      workout_session.update_invoices
    else
      workout_session.update_details(workout_session_params)
    end

		if workout_session.save
      add_google_event workout_session
		else
			if (workout_session.errors.any?)
					workout_session.errors.full_messages.each do |e|
						flash[:danger] = e
					end
			else
				return false
			end
		end
	end

  def remove
    authorize @workout_session
    group_exception = GroupException.new(:group_id => @workout_session.group_id, :date => WorkoutSession.convert_to_date(params[:date]), :workout_session => nil)
    group_exception.save
    redirect_to :back || root_url
  end

  def set_business_and_workout_session
    @workout_session = WorkoutSession.find(params[:id])
    @business = @workout_session.business
    @workout_session.admin = @business.view_as_employee?(@current_user)
  end

  def removefollowing
    authorize @workout_session
    if params[:date]
      @workout_session.group.update_attribute(:repeat_until, WorkoutSession.convert_to_date(params[:date]) - 1.day)
    else
  		if !@workout_session.group.nil?
  			wss = WorkoutSession.where("date >= ? AND group_id = ?", @workout_session.date, @workout_session.group.id)
  			wss.each do |w|
  				remove_google_event w
  				w.delete
  			end
  			flash[:info] = "Sessions deleted"
  		else
  			remove_google_event @workout_session
  			@workout_session.delete
  			flash[:info] = "Session deleted"
  		end
    end
		redirect_to :back || root_url
  end

  def removeall
    authorize @workout_session

		if !@workout_session.group.nil?
			wss = WorkoutSession.where(:group_id => @workout_session.group.id)

			wss.each do |w|
				remove_google_event w
				w.delete
			end

			flash[:info] = "Sessions deleted"
		else
			remove_google_event @workout_session
			@workout_session.delete
			flash[:info] = "Session deleted"
		end

		redirect_to :back || root_url
  end

	def unlink
    authorize @workout_session

		Reschedule.where(:workout_session_id => @workout_session.id).destroy_all
		Reschedule.where(:link => @workout_session.id).destroy_all
		flash[:info] = 'Session unlinked'

		redirect_to :back
	end

	def approveupto
    authorize @workout_session

		if !((ws = @workout_session.user_workouts).nil?)
			uid = ws.first.user_id
			if (!uid.nil?)
				wss = (workouts_for uid).where("date <= ? AND status = ?", @workout_session.date, WorkoutSession::STATUS["draft"])

				wss.each do |w|
					w.approve_session
					add_google_event w
				end

				flash[:info] = "Sessions approved"
			end
		else
			@workout_session.update_attribute(:status, WorkoutSession::STATUS["ok"])
			flash[:info] = "Session approved"
		end

		redirect_to :back || root_url
	end

	def approveallfollowing
    authorize @workout_session

		if !((ws = @workout_session.user_workouts).nil?)
			uid = ws.first.user_id
			if (!uid.nil?)
				wss = (workouts_for uid).where("date >= ? AND status = ?", @workout_session.date, WorkoutSession::STATUS["draft"])

				wss.each do |w|
					w.approve_session
					add_google_event w
				end

				flash[:info] = "Sessions approved"
			end
		else
			@workout_session.update_attribute(:status, WorkoutSession::STATUS["ok"])
			flash[:info] = "Session approved"
		end

		redirect_to :back || root_url
	end

	def approve
    authorize @workout_session

		@workout_session.approve_session
		flash[:info] = "Session approved"

		redirect_to :back || root_url
	end

	def approveallthismonth
    authorize @workout_session

    ws = @workout_session.user_workouts
		if ws.any?
      uid = ws.first.user_id
			if uid
				wss = (workouts_for uid).where(:date => @workout_session.date.beginning_of_month..@workout_session.date.end_of_month, :status => WorkoutSession::STATUS["draft"])
				wss.each do |w|
					w.approve_session
					add_google_event w
				end
				flash[:info] = "Sessions approved"
			end
		else
			@workout_session.update_attribute(:status, WorkoutSession::STATUS["ok"])
			flash[:info] = "Session approved"
		end

		redirect_to :back || root_url
	end

  #Status: OK = 0, Rescheduled_from = 1, Cancelled = 2, No Show = 3, Rescheduled_to = 4

  def success
    authorize @workout_session

		@workout_session.approve_session
		add_google_event @workout_session

		redirect_to :back || root_url
  end

  #Status: OK = 0, Rescheduled_from = 1, Cancelled = 2, No Show = 3, Rescheduled_to = 4
  def reschedule (workout_session, new_id)
    authorize @workout_session

		if @workout_session.update_attribute(:status, WorkoutSession::STATUS["reschedule"])
			@reschedule = Reschedule.create(:workout_session_id => @workout_session.id, :link => new_id)
			@workout_session.reschedule = @reschedule
			if @reschedule.save

			else
				flash[:danger] = "An error has occured while rescheduling."
			end
		end

  end

  def cancel
    authorize @workout_session

		remove_google_event @workout_session

    if @business.view_as_employee?(@current_user)
      @workout_session.update_attribute(:status, WorkoutSession::STATUS["cancel"])
      @workout_session.redo_invoices
		elsif @workout_session.created_at < Time.zone.now - 1.days
			if (@workout_session.date - 24.hours >= Time.zone.now || !@workout_session.billable_status?)
				@workout_session.update_attribute(:status, WorkoutSession::STATUS["cancel"])
			  @workout_session.redo_invoices
			else
				flash[:danger] = "Please consult your trainer to cancel within 24 hours."
			end
		else
			@workout_session.delete
		end

		redirect_to :back || root_url
  end

  def noshow
    @workout_session.admin = @business.view_as_employee?(@current_user)
    authorize @workout_session

		unless @workout_session.update_attribute(:status, WorkoutSession::STATUS["noshow"])
			flash[:danger] = "An error has occured while no showing."
		end
		redirect_to :back || root_url
  end

	def remove_google_event workout
		if workout.gsync && @business.view_as_employee?(@current_user)
			if @current_user.google_authenticated?
				workout.remove_google_event @current_user.oauth_token
			else
				flash[:info] = "Must authenticate before syncing Google Calendar."
			end
		end
	end

	def sync_google_event
		add_google_event @workout_session
	end

  private
    def add_google_event workout
      if @business.view_as_employee?(@current_user)
        if @current_user.google_authenticated?
             workout.setup_google_event @current_user.oauth_token
        else
          flash[:info] = "Must authenticate before syncing Google Calendar."
        end
      end
    end

    def dates_within_reason(dstart, dend)
      if (dend - dstart.to_date).to_i < 160
        return true
      else
        flash[:warning] = "Cannot schedule more than 5 months in advance"
      end
    end

    def set_business
      @business = Business.friendly.find(params[:business_id])
    end

		def workouts_for uid
			WorkoutSession.joins("JOIN user_workouts ON workout_sessions.id = user_workouts.workout_session_id AND user_workouts.user_id = " + uid.to_s)
		end

		def workout_session_params
			params.require(:workout_session).permit(:business_id, :date, :duration, :memo, :eventtype, :title, :location, :tax_id, :bfr_from, :bfr_to, :gsync, :until, :repeattype, :group_id, :repeat_on => [], :clients => [])
		end

		def workout_in_future(ws)
			if(ws.date < Date.today && !@business.view_as_employee?(@current_user))
				flash[:danger] = "Date must be in the future!"
				false
			end
			true
		end

		def get_session_cost(ws_id=nil, usr_id)
      usr = User.find(usr_id)

			if ws_id
				ws = WorkoutSession.find(ws_id)
				uw = ws.user_workouts.where(:user_id => usr_id)
				uw.empty? ? usr.rate : uw.first.cost
			else
				usr.rate
			end
		end

    def default_workout_sessions_tax business
  		@default_workout_sessions_tax ||= Constant.default_workout_sessions_tax(business).value
  	end
end
