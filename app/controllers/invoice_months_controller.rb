class InvoiceMonthsController < ApplicationController
  after_action :verify_authorized

  before_action :set_business, only: [:flip, :recalculate ]
  before_action :set_invoice_month_by_date, only: [:flip, :recalculate]

  # PATCH/PUT /invoice_months/1
  # PATCH/PUT /invoice_months/1.json
  def flip
    authorize @invoice_month

    msg = @invoice_month.flip
    redirect_to :back, notice: msg
  end

  def recalculate
    authorize @invoice_month

    unless @invoice_month.locked?
      Invoice.where(:business => @invoice_month.business, :date => @invoice_month.date.beginning_of_month..@invoice_month.date.end_of_month).each do |invoice|
        invoice.adjust_invoice
      end
    end

    redirect_to :back, notice: "Invoices readjusted"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invoice_month_by_date
      @invoice_month = InvoiceMonth.find_by(business_id: @business.id, date: convert_to_date_ymd(params[:date]))
      @invoice_month ||= InvoiceMonth.create(business_id: @business.id, date: convert_to_date_ymd(params[:date]))
    end

    def set_business
      @business = Business.friendly.find(params[:business_id])
    end
    # Never trust parameters from the scary internet, only allow the white list through.
end
