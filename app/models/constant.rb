class Constant < ApplicationRecord
	belongs_to :business

	def self.create_default_membership_tax business
		business.constants.create(:table => "memberships", :references => "taxes", :value => nil)
	end

	def self.default_membership_tax business
		default_tax = business.constants.where(:table => "memberships", :references => "taxes")

		if default_tax.any?
			return default_tax.first
		else
			return Constant.create_default_membership_tax business
		end
	end

	def self.create_default_workout_sessions_tax business
		business.constants.create(:table => "workout_sessions", :references => "taxes", :value => nil)
	end

	def self.default_workout_sessions_tax business
		default_tax = business.constants.where(:table => "workout_sessions", :references => "taxes")
		if default_tax.any?
			return default_tax.first
		else
			return Constant.create_default_workout_sessions_tax business
		end
	end
end
