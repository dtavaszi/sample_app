class Friend < ApplicationRecord
	validates_uniqueness_of :user1_id, :scope => :user2_id
	attr_accessor :new_friend_id
end
