class WorkoutSession < ApplicationRecord
	include ActiveModel::Dirty # Used to track changes

	default_scope { where(:deleted => false) }
	default_scope -> { order(created_at: :desc) }

	enum attrs_enum: column_names

	CLIENT_SIGNIFICANT_COLUMNS = ["date", "location", "status", "deleted", "memo"]
	OWNER_SIGNIFICANT_COLUMNS = ["deleted"]

	belongs_to :business, optional: true
	belongs_to :user
	belongs_to :group, optional: true
	belongs_to :tax, optional: true

	has_many :user_workouts, dependent: :destroy
	has_many :invoices, :through => :user_workouts
	has_many :users, :through => :user_workouts
	has_many :profiles, :through => :users
	has_many :reminders

	has_one :reschedule, dependent: :destroy
	validate :no_overlap
	validate :twenty_four_hours_notice
	validate :valid_duration, on: :update

	after_save :update_history

	before_destroy :remove_rescheduled_pointers
	before_destroy :remove_change_history

	attr_accessor :until, :clients, :days, :old_id, :old_date, :admin, :repeattype, :repeat_on, :slot

	STATUS = {'ok' => 0, 'reschedule' => 1, 'cancel' => 2, 'noshow' => 3, 'draft' => 4, 0 => "Attended", 1 => "Rescheduled", 2 => "Cancelled", 3 => "No Show", 4 => "Pending"}
	DAY_TO_INTEGER = { 'Sunday' => 0, 'Monday' => 1, 'Tuesday' => 2, 'Wednesday' => 3, 'Thursday' => 4, 'Friday' => 5, 'Saturday' => 6, 'Sun' => 0, 'Mon' => 1, 'Tue' => 2, 'Wed' => 3, 'Thu' => 4, 'Fri' => 5, 'Sat' => 6 }

	def add_group_exception date
		if group_id
			group.add_group_exception date
		end
	end

	def valid_duration
		unless admin
			unless ((duration_was - date_was) == (duration - date))
				errors[:base] << "Cannot change event duration."
			end
			puts "Valid duration"
		end
	end

	def update_user_workouts workout_session_params
    clients_list = workout_session_params[:clients]

    if clients_list.nil?
      user_workouts.destroy_all
    else
			uids = user_workouts.pluck(:user_id)

      uids.each do |uid|
        unless clients_list.include?(uid)
          user_workouts.where(:user_id => uid).destroy_all
          uids.delete(uid)
        end
      end

      clients_list.each do |usr_id|
        unless uids.include?(id)
          user_workouts.create(user_id: usr_id, cost: User.find(usr_id).rate)
        end
      end

    end
  end

	def update_details workout_session_params
		self[:memo] = workout_session_params[:memo]
		self[:title] = workout_session_params[:title]
		self[:location] = workout_session_params[:location]
		self[:bfr_from] = workout_session_params[:bfr_from] if admin
		self[:bfr_to] = workout_session_params[:bfr_to] if admin
	end

	def isSameDuration duration
		return length == duration
	end

	def update_main workout_session_params
		self[:date] = WorkoutSession.convert_to_datetime workout_session_params[:date]

		if workout_session_params[:duration].nil? || !admin
			self[:duration] = date + 1.hour
		else
			self[:duration] = WorkoutSession.convert_to_datetime workout_session_params[:duration]
		end
	end

	def update_all workout_session_params
		update_details workout_session_params
		update_main workout_session_params
	end

	def is_public?
		[WorkoutSession.groupworkout, WorkoutSession.recurring_groupworkout].include?(eventtype)
	end

	def is_private?
		![WorkoutSession.groupworkout, WorkoutSession.recurring_groupworkout].include?(eventtype)
	end

	def partakes_in?(usr_id)
		user_workouts.where(:user_id => usr_id).any?
	end

	def approve_session
		update_attribute(:status, WorkoutSession::STATUS["ok"])
		update_invoices
	end

	def update_invoices
		Invoice.create_invoice_for(self) if billable_status?
	end

	def self.most_active_versus_now_ratio()
		max = most_active_period
		now = sessions_in_month.count

		(now.to_f / max * 100).to_i
	end

	def length
		duration - date
	end

	def self.most_active_period()
		d1 = reorder(:date).limit(1).first.date
		d2 = Time.zone.now.end_of_month.end_of_day

		max = 0
		while d2 > d1
			date_range = d1.beginning_of_month.beginning_of_day..d1.end_of_month.end_of_day
			current = sessions_in_month(date_range).count
			max = current if current > max
			d1 += 1.month
		end

		max
	end

	def self.sessions_count_groupby_month()
		d1 = reorder(:date).limit(1).first.date
		d2 = Time.zone.now.end_of_month.end_of_day

		out = []
		while d2 > d1
			date_range = d1.beginning_of_month.beginning_of_day..d1.end_of_month.end_of_day
			out << sessions_in_month(date_range).count
			d1 += 1.month
		end

		out
	end

	def self.sessions_in_month(date_range = nil)
		date_range ||= Time.zone.now.beginning_of_month.beginning_of_day..Time.zone.now.end_of_month.end_of_day
		unscoped.where(:date => date_range, :eventtype => workout, :status => billable_status)
	end

	def sign_up (usr)
		if eventtype == WorkoutSession.groupworkout
			if !(uw = user_workouts.unscoped.find_by(user: usr))
				uw = user_workouts.create(user: usr)
				uw.update_history
				Invoice.create_invoice_for(self)
			else
				uw.deleted = false
				uw.save
				# uw.update_history
				Invoice.create_invoice_for(self)
			end
		end
	end

	def cancel_booking (usr)
		if eventtype == WorkoutSession.groupworkout
			if (uws = user_workouts.where(user: usr)).empty?
				return false
			else
				uws.each do |uw|
					uw.delete
				end
			end
		end
	end

	def weekdays_array
		weekdays = [sun, mon, tue, wed, thu, fri, sat]
		weekdays.map { |day|  day == "1" ? true : false}
	end

	def tax_rate
		tax ? tax.rate : 1
	end

	def redo_invoices
		invoices.each do |invoice|
			invoice.adjust_invoice
		end
	end

	def apply_default_tax
		update_attribute(:tax_id, business.default_workout_sessions_tax)
	end

	def event_dates_unchanged?(start_date_param, end_date_param)
		start_date_equals_param?(start_date_param) && end_date_equals_param?(end_date_param)
	end

	def start_date_equals_param? (start_date_param)
		date == WorkoutSession.convert_to_datetime(start_date_param)
	end

	def end_date_equals_param? (end_date_param)
		duration == WorkoutSession.convert_to_datetime(end_date_param)
	end

	def self.instance_eventtypes_array
		return [WorkoutSession.event, WorkoutSession.workout, WorkoutSession.groupworkout]
	end

	def self.private_eventtypes_array
		return [WorkoutSession.event, WorkoutSession.workout]
	end

	def self.public_eventtypes_array
		return [WorkoutSession.groupworkout, WorkoutSession.recurring_groupworkout]
	end

	def is_public_eventtype
		return WorkoutSession.public_eventtypes_array.include?(eventtype)
	end

	def is_instance_eventtype
		WorkoutSession.instance_eventtypes_array.include?(eventtype)
	end

	def self.recurring_eventtypes_array
		WorkoutSession.instance_eventtypes_array.map { |type| -type}
	end

	def is_recurring_eventtype
		WorkoutSession.recurring_eventtypes_array.include?(eventtype)
	end

	def self.event
		return 1
	end

	def self.workout
		return 2
	end

	def self.groupworkout
		return 3
	end

	def self.recurring_groupworkout
		-WorkoutSession.groupworkout
	end

	def set_start_date(date_)
		new_date = WorkoutSession.convert_to_date(date_)
		self[:date] = date.change({ day: new_date.day, month: new_date.month, year: new_date.year })
	end

	def self.eventtype_string(event_type)
		case event_type
		when 1
			return "Event"
		when 2
			return "Session"
		when 3
			return "Group"
		when -3
			return "Group"
		end
	end

	def self.billable_status
		return [WorkoutSession.successful, WorkoutSession.noshow]
	end

	def billable_status?
		WorkoutSession.billable_status.include?(status)
	end

	def billable_eventtype?
		WorkoutSession.billable_eventtype.include?(eventtype)
	end

	def self.billable_eventtype
		return [WorkoutSession.workout, WorkoutSession.groupworkout]
	end

	def set_upcoming
		status = WorkoutSession.upcoming
	end

	def upcoming?
		return status == WorkoutSession.upcoming
	end

	def set_successful
		status = WorkoutSession.successful
	end

	def successful?
		return status == WorkoutSession.successful
	end

	def set_rescheduled
		status = WorkoutSession.rescheduled
	end

	def rescheduled?
		return status == WorkoutSession.rescheduled
	end

	def set_noshow
		status = WorkoutSession.noshow
	end

	def noshow?
		return status == WorkoutSession.noshow
	end

	def set_draft
		status = WorkoutSession.rescheduled
	end

	def draft?
		return status == WorkoutSession.rescheduled
	end

	def self.convert_to_datetime date
		Time.zone.strptime(date, "%m/%d/%Y %I:%M %p") if date.present?
	end

	def self.convert_to_date date
		Date.strptime(date, "%m/%d/%Y") if date.present?
	end

	def init_google_oauth oauth_token
		service = Google::Apis::CalendarV3::CalendarService.new
		service.client_options.application_name = "F!Trackular"
		service.authorization = oauth_token

		return service
	end

	def remove_google_event oauth_token
		if gid
			service = init_google_oauth oauth_token

			begin
				service.delete_event('primary', gid)
			rescue Google::Apis::ClientError => e
				errors[:base] << "Client failure. Could not sync with Google."
				return false
			rescue Google::Apis::AuthorizationError
				errors[:base] << "Authentication failed. Please resync and try again."
				return false
			end

			update_attribute(:gid, nil)
		end
	end

	def paid?
		if user_workouts.any? && billable_status?
			user_workouts.each do |uw|
				return false unless uw.invoice && uw.invoice.processed
			end
		end
		true
	end

	def setup_google_event oauth_token
		service = init_google_oauth oauth_token

		if status == WorkoutSession.upcoming
			gid ? update_google_event(service) : create_google_event(service)
			puts "setup_google_event"
		end
	end

	def create_google_event service
		puts "create_google_event"
		@summary = title.blank? ? "#{ business.name } Appt" : title
		@description = memo
		@location = location

		clients = []
		user_workouts.each do |uw|
			clients << {email: uw.user.email}
		end

		@event = Google::Apis::CalendarV3::Event.new({
			summary: @summary,
			description: @description,
			location: @location,
			start: { date_time: (date.to_datetime) },
			end: { date_time: (duration.to_datetime) },
			attendees: clients})

		calendar_id = 'primary'

		begin
			result = service.insert_event(calendar_id, @event)
			update_attribute(:gid, result.id)
			rescue Google::Apis::AuthorizationError
				return false
		end

		return result.id
	end

	def update_google_event service
		begin
			event = service.get_event('primary', gid)
			event.summary = title.blank? ? "#{ business.name } Appt" : title
			event.start = { date_time: (date.to_datetime) }
			event.end = { date_time: (duration.to_datetime) }
			event.description = memo
			event.location = location

			clients = []
			user_workouts.each do |uw|
				clients << {email: uw.user.email}
			end

			event.attendees = clients
			result = service.update_event('primary', event.id, event)
		rescue Google::Apis::ClientError
			create_google_event service
		end
	end

	def delete
		if (created_at < ChangeHistory::TIME_UNTIL_RECORDED) || ChangeHistory.sent_notification?(:workout_sessions, business, id)
			self[:deleted] = true
			save

			user_workouts.each do |uw|
				uw.delete
			end
		else
			destroy
		end
	end

	private
		def significant_columns
			puts "Admin? : " + admin.to_s
			if admin
				WorkoutSession::OWNER_SIGNIFICANT_COLUMNS
			else
				WorkoutSession::CLIENT_SIGNIFICANT_COLUMNS
			end
		end

		def update_history
			if ChangeHistory.entry_exists?(:workout_sessions, business_id, id)
				(self.changed & significant_columns).each do |changed_attr|
					puts "Add to entry #{ id }"
					puts WorkoutSession.attrs_enums[changed_attr].to_s
					ChangeHistory.create_entry(:workout_sessions, business_id, user_id, id, self[changed_attr], self.changes[changed_attr][0], WorkoutSession.attrs_enums[changed_attr])
				end
			elsif eventtype == WorkoutSession.workout
				puts "Create workout session entry #{ id }"
				ChangeHistory.create_entry(:workout_sessions, business_id, user_id, id, nil, nil, WorkoutSession.attrs_enums[:id])
			end
		end

		def remove_rescheduled_pointers
			reschedules = Reschedule.where("id = ? OR link = ?", id, id)
			reschedules.delete_all
		end

		def remove_change_history
			puts "Destroy workout session #{ id } change history"
			ChangeHistory.destroy_entry(:workout_sessions, business_id, id)
		end

		def twenty_four_hours_notice
			if ((date - 24.hours) <= Time.zone.now ) && !admin
				errors[:base] << "Uh oh... please contact your trainer to schedule less than 24 hours in advance."
			end
		end

		def rescheduled_event
			old_id
		end

		def normalize_recurring_date(w)
			x = (w.duration - w.date)
			w.date = w.date.change(year: date.year, month: date.month, day: date.day)
			w.duration = w.date + x
		end

		def no_overlap
			unless admin || is_public?
				no_overlap_with_instance_events
				no_overlap_with_recurring_events
			end
		end

		def no_overlap_with_recurring_events
			ws_overlap = WorkoutSession.where(:business_id => business_id, :eventtype => WorkoutSession.recurring_eventtypes_array).where("workout_sessions.date < ?", date.end_of_day).where("workout_sessions.group_id <> #{ group_id || -1 }")
			ws_overlap = ws_overlap.joins("LEFT JOIN groups ON workout_sessions.group_id = groups.id").joins("LEFT JOIN group_exceptions ON groups.id = group_exceptions.group_id").where("workout_sessions.group_id IS NULL OR groups.repeat_until > '#{ date }' OR groups.repeat_until IS NULL", date).where("group_exceptions.date <> '#{ date }' OR group_exceptions.date IS NULL").sort_by { |event| event.date.strftime("%H:%M")}

			ws_overlap.each do |w|
				puts "Recurring ID:" + w.id.to_s + " - Repeat on #{ w.date.wday }? =" + w.group.repeat_on_dow?(date.wday).to_s
				normalize_recurring_date w
				if w.group.repeat_on_dow?(date.wday) && w.date.between?( ( (date - bfr_from.minutes) - (w.duration - w.date) + 1.minute), (duration + bfr_to.minutes)-1.minute)
					errors[:base] << date.strftime("%l:%M%p on %B %-d, %Y") + " is not available!"
					puts "overlap recurring"
					return
				end
			end
		end

		def no_overlap_with_instance_events
			if old_id.nil?
				ws_overlap = WorkoutSession.where(:business_id => business_id, :date => (date - 24.hours)..(date + 24.hours), :status => [STATUS["ok"], STATUS["draft"], nil], :eventtype => WorkoutSession.instance_eventtypes_array).where.not(:id => id)
			else
				ws_overlap = WorkoutSession.where(:business_id => business_id, :date => (date - 24.hours)..(date + 24.hours), :status => [STATUS["ok"], STATUS["draft"]], :eventtype => WorkoutSession.instance_eventtypes_array).where.not(:id => [old_id, id])
			end

			if !admin
				ws_overlap.each do |w|
					bfr_before = (w.bfr_to > bfr_from) ? w.bfr_to : bfr_from
					bfr_after = (w.bfr_from > bfr_to) ? w.bfr_from : bfr_to

					if w.date.between?( ( (date - bfr_before.minutes) - (w.duration - w.date) + 1.minute), (duration + bfr_after.minutes)-1.minute)
						errors[:base] << date.strftime("Oops! %l:%M%p on %B %-d, %Y") + " is not available :("
						puts "overlap instance 1"
						return
					end
				end
			else
				ws_overlap.each do |w|
					if w.date.between?( date - (w.duration - w.date) + 1.minute, duration-1.minute)
						errors[:base] << date.strftime("Oops %l:%M%p on %B %-d, %Y") + " is not available :("
						puts "overlap instance 2"
						return
					end
				end
			end
		end

		def self.upcoming
			return 0
		end

		def self.successful
			return 0
		end

		def self.rescheduled
			return 1
		end

		def self.cancel
			return 2
		end

		def self.noshow
			return 3
		end
		def self.draft
			return 4
		end
end
