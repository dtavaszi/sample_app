class Group < ApplicationRecord
	belongs_to :business
	has_many :workout_sessions, dependent: :destroy
  has_many :group_exceptions, dependent: :destroy

	attr_accessor :saved

	def save_once_for_id
		unless saved
			saved = true if save()
		end

		return saved
	end

	def add_group_exception date
		GroupException.create(group_id: id, date: date)
	end

	def repeat_on_dow?(dow)
		repeat_on[dow] == "1"
	end

end
