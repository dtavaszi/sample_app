class InvoiceMonth < ApplicationRecord
  belongs_to :business

  def flip
    if (!locked && date < Time.zone.now.beginning_of_month) || locked
      self.locked = !locked
      save

      return "Month locked" if locked
      return "Month unlocked"
    end

    "Cannot lock future months, sorry!"
  end

  def self.is_locked?(business, date_)
    @im = InvoiceMonth.find_by(business_id: business.id, date: date_.beginning_of_month)
    unless @im
      @im = InvoiceMonth.create(business_id: business.id, date: date_.beginning_of_month)
    end
    return @im.locked?
  end

  def self.im(business, date_)
    @im = InvoiceMonth.find_by(business_id: business.id, date: date_.beginning_of_month)
    unless @im
      @im = InvoiceMonth.create(business_id: business.id, date: date_.beginning_of_month)
    end

    return @im
  end
end
