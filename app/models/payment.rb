class Payment < ApplicationRecord
	belongs_to :business
	belongs_to :user
	belongs_to :invoice, optional: true

	enum payment_type: ["Cash", "Visa", "Mastercard", "Interac", "Money Order", "Personal Cheque", "Company Cheque", "Adjustment", "e-Transfer"]
end
