class Item < ApplicationRecord
	belongs_to :business
	belongs_to :tax, optional: true	
end
