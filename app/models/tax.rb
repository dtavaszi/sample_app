class Tax < ApplicationRecord
	belongs_to :business
	has_many :memberships
	has_many :workout_sessions

	validates :name, :percentage, presence: true
	validates :percentage, numericality: { less_than_or_equal_to: 100, greater_than_or_equal_to: 0, only_integer: true }
	
	def rate
		if percentage
			1.0 + percentage.to_f / 100
		else
			1.0
		end		
	end
end
