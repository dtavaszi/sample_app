class GroupException < ApplicationRecord
  belongs_to :group
  belongs_to :workout_session, optional: true
end
