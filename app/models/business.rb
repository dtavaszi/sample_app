class Business < ApplicationRecord
	extend FriendlyId
	friendly_id :name, use: :slugged

	after_validation :move_friendly_id_error_to_name

	belongs_to :user, optional: true

	has_many :constants #
	has_many :groups #
	has_many :invoices #
	has_many :business_users
	has_many :items #
	has_many :invoice_items #
	has_many :invoice_months
	has_many :memberships #
	has_many :payments #
	has_many :reminders #
	has_many :reschedules #
	has_many :taxes #
	has_many :user_rates #
	has_many :workout_sessions #
	has_many :user_workouts
	has_many :change_histories

	has_many :subscriptions
	has_many :users, :through => :subscriptions

	mount_uploader :avatar, AvatarUploader

	update_interval = Time.zone.now - 15.minutes

	BOOK_TYPES = ['Private Session', 'Group Session']
	EVENING_NOTIFICATIONS_TIME = Time.zone.now.end_of_day..(Time.zone.now.end_of_day + 12.hours + 30.minutes)
	MORNING_NOTIFICATIONS_TIME = (Time.zone.now.beginning_of_day + 12.days + 30.minutes)..Time.zone.now.end_of_day

	def send_booking_notifications
		changes = change_histories.where(notified_email: false).where.not(user: get_employees)
		changes.update_all(notified_email: true)
		if changes.any?
			ApplicationMailer.booking_notification_email(self, changes).deliver
		else
			puts "No changes to mail"
		end
	end

	def send_evening_notifications
		send_session_notification(Business::EVENING_NOTIFICATIONS_TIME)
	end

	def send_morning_notifications
		send_session_notification(Business::MORNING_NOTIFICATIONS_TIME)
	end

	def send_session_notification(date_range)
		notification_workouts = grouped_by_client_workouts_for_date_range(date_range)

		puts "Number of notifications #{ notification_workouts.count }"
		notification_workouts.each do |client, client_user_workouts|
			ApplicationMailer.send_workouts_reminder(client, client_user_workouts).deliver if client.email_session_reminder && client.activated
		end
	end

	def allow_group_session?
		return true if session_type.blank?
		if session_type[1] == "1"
			return true
		end

		false
	end

	def allow_private_session?
		return true if session_type.blank?
		if session_type[0] == "1"
			return true
		end

		false
	end

	def grouped_by_client_workouts_for_date_range(date_range)
		UserWorkout.joins(:workout_session)
		.where(workout_sessions: {date: date_range, eventtype: WorkoutSession.billable_eventtype, status: WorkoutSession.upcoming, business_id: id})
		.group_by(&:user)
	end

	def pending_users
		User.joins("INNER JOIN subscriptions ON (#{id} = subscriptions.business_id AND users.id = subscriptions.user_id)")
		.where(subscriptions: {:active => true}).where(subscriptions: {:status => Subscription.status_pending})
		.joins("LEFT JOIN profiles ON users.id = profiles.user_id").reorder("profiles.firstname")
	end

	def active_users_all
		User.joins("INNER JOIN subscriptions ON (#{id} = subscriptions.business_id AND users.id = subscriptions.user_id)")
		.where(subscriptions: {:status => Subscription.status_subscribed}).joins("LEFT JOIN profiles ON users.id = profiles.user_id")
		.reorder("profiles.firstname")
	end

	def subscribed_users
		User.joins("INNER JOIN subscriptions ON (#{id} = subscriptions.business_id AND users.id = subscriptions.user_id)")
		.where(subscriptions: {:status => Subscription.status_subscribed})
	end

	def active_users
		User.get_active_clients(nil, self).joins("INNER JOIN subscriptions ON (#{id} = subscriptions.business_id AND users.id = subscriptions.user_id)")
		.where(subscriptions: {:status => Subscription.status_subscribed}).joins("LEFT JOIN profiles ON users.id = profiles.user_id")
		.reorder("profiles.firstname")
	end

	def inactive_users
		User.get_inactive_clients(nil, self).joins("INNER JOIN subscriptions ON (#{id} = subscriptions.business_id AND users.id = subscriptions.user_id)")
		.where(subscriptions: {:status => Subscription.status_subscribed}).joins("LEFT JOIN profiles ON users.id = profiles.user_id")
		.reorder("profiles.firstname")
	end

	def deactivated_users
		User.joins("INNER JOIN subscriptions ON (#{id} = subscriptions.business_id AND users.id = subscriptions.user_id)")
		.where(subscriptions: {:status => [Subscription.status_unsubscribed, Subscription.status_deactivated]})
		.joins("LEFT JOIN profiles ON users.id = profiles.user_id").reorder("profiles.firstname")
	end

	def get_subscription (user_id)
		subscriptions.where(:user_id => user_id, :business_id => id).first
	end

	def subscribed_user?(user_id)
		Subscription.active_subscription?(user_id, id)
	end

	def view_as_employee?(usr)
		is_owner?(usr.id) || is_employee?(usr.id) || usr.admin?
	end

	def owner
		user
	end

	def get_employees
 		owner
	end

	def get_session_type
		if session_type.blank?
			return "0" * BOOK_TYPES.length
		end
		session_type
	end

	def is_employee?(user_id)
		subscriptions.where(role: :employee, active: true, user_id: user_id).any?
	end

	def is_owner?(usr_id)
		user_id == usr_id
	end

	def set_owner(usr: )
		user = usr
	end

	def move_friendly_id_error_to_name
		errors.add :name, *errors.delete(:friendly_id) if errors[:friendly_id].present?
	end

	def default_workout_sessions_tax
		@default_workout_sessions_tax ||= Constant.default_workout_sessions_tax self
	end

	def default_memberships_tax
		@default_memberships_tax ||= Constant.default_membership_tax self
	end
end
