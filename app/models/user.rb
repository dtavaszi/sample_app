class User < ApplicationRecord
	has_one :google_auth, dependent: :destroy
	has_one :account, dependent: :destroy
	has_one :business

	has_one :profile, dependent: :destroy
	accepts_nested_attributes_for :profile, update_only: true

	has_many :subscriptions, inverse_of: :user
	accepts_nested_attributes_for :subscriptions, update_only: true

	has_many :businesses, :through => :subscriptions
	has_many :workout_sessions, dependent: :destroy
	has_many :reminders, dependent: :destroy
	has_many :user_workouts, :through => :workout_sessions
	has_many :user_rates, dependent: :destroy
	has_many :invoices
	has_many :suggestions

	has_many :memberships, :through => :subscriptions

	before_create :create_activation_digest
	before_save :downcase_email

	attr_accessor :remember_token, :activation_token, :target_business
	before_save { self.email = email.downcase }
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true, length: { maximum: 255 },
										format: { with: VALID_EMAIL_REGEX },
										uniqueness: { case_sensitive: false }

	has_secure_password

	def next_session business, date = Time.zone.now
		business.workout_sessions.joins("JOIN user_workouts ON workout_sessions.id = user_workouts.workout_session_id AND user_workouts.user_id =" + id.to_s).where("date > ?", Time.zone.now).reorder(date: :asc).first
	end

	def upcoming_reminders_by_date
		reminders.where('date >= ?', Time.zone.now.beginning_of_day).reorder(date: :asc).group_by(&:date)
	end

	def upcoming_reminders_by_date_limited
		reminders.where('date >= ?', Time.zone.now.beginning_of_day).limit(10).reorder(date: :asc).group_by(&:date)
	end

	def self.most_active_versus_now_ratio()
		max = most_active_period
		now = get_active_clients.count

		return (now.to_f / max * 100).to_i if max > 0
		0
	end

	def self.most_active_period()
		d1 = WorkoutSession.reorder(:date).limit(1).first.date + 6.weeks
		d2 = Time.zone.now - 6.weeks

		max = 0
		while d2 > d1
			date_range = d1 - 6.weeks..d1
			current = get_active_clients(date_range).count
			max = current if current > max
			d1 += 1.month
		end

		max
	end

	def self.active_users_count_groupby_month()
		d1 = WorkoutSession.reorder(:date).limit(1).first.date + 6.weeks
		d2 = Time.zone.now.end_of_month.end_of_day

		out = []
		while d2 > d1
			date_range = d1 - 6.weeks..d1
			out << get_active_clients(date_range).count
			d1 += 1.month
		end

		out
	end

	def self.get_active_clients(date_range = nil, business=nil)
		ids = get_active_ids(date_range, business)
		unscoped.where(:id => ids)
	end

	def self.get_active_ids(date_range = nil, business=nil)
		date_range ||= Time.zone.now - 6.weeks..Time.zone.now

		if business
			users = WorkoutSession.unscoped.where(:date => date_range, :business_id => business.id).joins(:user_workouts).select('user_workouts.user_id').uniq
		else
			users = WorkoutSession.unscoped.where(:date => date_range).joins(:user_workouts).select('user_workouts.user_id').uniq
		end

		return users
	end

	def self.get_inactive_clients(date_range = nil, business=nil)
		ids = get_active_ids(date_range, business)
		unscoped.where.not(:id => ids)
	end

	# Returns the hash digest of the given string.
	def User.digest(string)
		cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
																									BCrypt::Engine.cost
		BCrypt::Password.create(string, cost: cost)
	end

	# Returns a random token.
	def User.new_token
		SecureRandom.urlsafe_base64
	end

	def tick_gdrive_for(business_id)
		get_subscription(business_id).update_attribute(:gdrive_updated_at, Time.zone.now)
	end

	def partakes_in?(event)
		event.user_workouts.where(:user_id => id).exists?
	end

	def subscribed_to?(business)
		subscription.where(:business_id => business.id).exists?
	end

	def get_subscription(business_id)
		subscriptions.find_by(business_id: business_id)
	end

	def get_membership(business_id)
		if subs = subscriptions.find_by(business_id: business_id)
			subs.membership
		end
	end

	# Remembers a user in the database for use in persistent sessions.
	def remember
		self.remember_token = User.new_token
		update_attribute(:remember_digest, User.digest(remember_token))
	end

	# Returns true if the given token matches the digest.
	def authenticated?(attribute, token)
		digest = send("#{attribute}_digest")
		return false if digest.nil?
		BCrypt::Password.new(digest).is_password?(token)
	end

	# Forgets a user.
	def forget
		update_attribute(:remember_digest, nil)
	end

	def emailbox
		email[0..email.index('@')-1]
	end

	def shortname
		x = email.split("@")[0].humanize
		if profile
			if profile.firstname.present?
				if profile.lastname.present?
					x = profile.firstname.humanize + " " + profile.lastname[0].humanize
				else
					x = profile.firstname.humanize
				end
			elsif profile.lastname.present?
				x = profile.lastname
			end
		end

		humanize_name(x)
	end

	def name(upcase: false)
		if profile
			f = profile.firstname || ""
			l = profile.lastname || ""


			n = f + " " + l
			n = n.gsub(/\s+/, ' ').titleize
			n.blank? ? email : ( upcase ? n.upcase : humanize_name(n) )
		else
			emailbox
		end
	end

	def humanize_name(x)
		x.split.map(&:capitalize).join(' ')
	end

	def address
		address = ""
		if profile
			(address += profile.street) unless profile.street.blank?
			(address += ", " + profile.city) unless profile.city.blank?
			(address += ", " + profile.postalcode) unless profile.postalcode.blank?
			address
		end
	end

	def phonenumber
		profile.phonenumber if profile && profile.phonenumber
	end

	def age
		"(no age)"
		if profile && profile.dob
			now = Time.zone.now
			x = now.year - profile.dob.year

			unless (now.month >= profile.dob.month) || (now.month >= profile.dob.month && now.day >= profile.dob.day)
				x -= 1
			end
			x
		end
	end

	def dob
		if profile && profile.dob
			profile.dob.strftime("%-d %B, %Y")
		end
	end

	def rate
		if (upay = user_rates.order("created_at").last)
			return upay.rate
		end
	end

	def membership_cost(business_id)
		if get_membership(business_id)
			get_membership(business_id).cost
		else
			0
		end
	end

	def google_authenticated?
		google_auth && google_auth.auth_valid?
	end

	def oauth_token
		google_auth && google_auth.oauth_token
	end

	def trainer?(business = nil)
		admin
	end

	def client?(business = nil)
		not(admin)
	end

	def create_activation_digest
		self.activation_token = User.new_token
		self.activation_digest = User.digest(activation_token)
		self.activated_at = Time.zone.now
	end

	private

		def downcase_email
			self.email = email.downcase
		end

end
