class Subscription < ApplicationRecord
  belongs_to :user, inverse_of: :subscriptions
  belongs_to :business
  belongs_to :membership

  validates :user_id, presence: true
  validates :business_id, presence: true

  enum role: {
    client: 0,
    employee: 1
  } #and owner

  def self.status_deactivated
    -2
  end

  def self.status_unsubscribed
    -1
  end

  def self.status_subscribed
    0
  end

  def self.status_pending
    1
  end

  def approve
    update_attribute(:status, Subscription.status_subscribed)
  end

  def deactivate
    update_attribute(:status, Subscription.status_deactivated)
  end

  def deactivated?
    status == Subscription.status_deactivated
  end

  def unsubscribed?
    status == Subscription.status_unsubscribed
  end

  def subscribed?
    status == Subscription.status_subscribed
  end

  def pending?
    status == Subscription.status_pending
  end

  def self.status_request
    Subscription.status_pending
  end

  def self.resubscribe(user_id, business_id)
    where(:user_id => user_id, :business_id => business_id).update_all(status: Subscription.status_request)
  end

  def self.unsubscribe(user_id, business_id)
    where(:user_id => user_id, :business_id => business_id).update_all(status: Subscription.status_unsubscribe)
  end

  def self.active_subscription?(user_id, business_id)
    where(:user_id => user_id, :business_id => business_id, :status => Subscription.status_subscribed).exists?
  end

  def self.subscription_exists?(user_id, business_id)
    where(:user_id => user_id, :business_id => business_id).exists?
  end

  def unsubscribe
    update_attribute(:status, Subscription.status_unsubscribed)
  end

  def resubscribe
    update_attribute(:status, Subscription.status_request)
  end
end
