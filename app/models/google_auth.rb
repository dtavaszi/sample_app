class GoogleAuth < ApplicationRecord
  belongs_to :user
	
	def auth_valid?
		oauth_expires_at && oauth_expires_at > Time.zone.now
	end
end
