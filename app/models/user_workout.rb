class UserWorkout < ApplicationRecord
	include ActiveModel::Dirty # Used to track changes

	default_scope { where(:deleted => false) }

	belongs_to :user
	belongs_to :workout_session
	belongs_to :business
	belongs_to :invoice, optional: true

	after_save :update_history
	before_destroy :remove_change_history

	enum attrs_enum: column_names

	validates_uniqueness_of :user_id, :scope => :workout_session_id

	SIGNIFICANT_COLUMNS = ["deleted"] # Only care if it's been deleted

	def delete
		if (created_at < ChangeHistory::TIME_UNTIL_RECORDED) || ChangeHistory.sent_notification?(:user_workouts, business_id, id)
			self[:deleted] = true
			save
		else
			destroy
		end
	end

	def update_history
		if workout_session.is_public_eventtype
			if ChangeHistory.entry_exists?(:user_workouts, workout_session.business_id, id)
				(self.changed & SIGNIFICANT_COLUMNS).each do |changed_attr|
					puts "Add to entry #{ id }"
					puts WorkoutSession.attrs_enums[changed_attr].to_s
					ChangeHistory.create_entry(:user_workouts, workout_session.business_id, user_id, id, self[changed_attr], self.changes[changed_attr][0], WorkoutSession.attrs_enums[changed_attr])
				end
			else
				puts "Create user workout session entry #{ id }"
				ChangeHistory.create_entry(:user_workouts, workout_session.business_id, user_id, id, nil, nil, WorkoutSession.attrs_enums[:id])
			end
		end
	end

	def cost
		if workout_session && workout_session.billable_eventtype? && workout_session.billable_status?
			return (self[:cost] ? self[:cost].to_f : 0)
		else
			return 0
		end
	end

	def actual_cost(include_tax = true)
		if self[:cost] && workout_session.tax
			self[:cost] * workout_session.tax.rate
		elsif self[:cost]
			return self[:cost]
		else
			return 0
		end
	end

	def cost_with_tax
		if cost && workout_session.tax
			cost * workout_session.tax.rate
		else
			return 0
		end
	end

	def tax_amount
		if cost && workout_session.tax
			cost * (workout_session.tax.rate - 1)
		else
			return 0
		end
	end

	def remove_change_history
		puts "Destroy user workouts #{ id } change history"
		ChangeHistory.destroy_entry(:user_workouts, business_id, id)
	end
end
