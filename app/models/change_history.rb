class ChangeHistory < ApplicationRecord
  enum table: [:workout_sessions, :businesses, :user_workouts]
  RELEVANT_TABLES = [WorkoutSession, UserWorkout]

  belongs_to :business
  belongs_to :user

  after_save :clear_old_entries
  MAX_ENTRIES = 1000
  TIME_UNTIL_RECORDED = Time.zone.now - 50.minutes # After this time, records will be ChangeHistory

  # Events

  def self.sent_notification?(_table, _business_id, _id)
    ChangeHistory.find_by(table: _table, business_id: _business_id, row: _id, notified_email: true)
  end

  def self.entry_exists?(_table, _business_id, _id)
    ChangeHistory.find_by(table: _table, business_id: _business_id, row: _id)
  end

  def self.create_entry(_table, _business_id, _user_id, _id, _value, _old_value, _code)
    ChangeHistory.create(table: _table, user_id: _user_id, business_id: _business_id, row: _id, value: _value, old_value: _old_value, code: _code)
  end

  def self.destroy_entry(_table, _business_id, _id)
    ChangeHistory.where(table: _table, business_id: _business_id, row: _id).destroy_all
  end

  def clear_old_entries
    if ChangeHistory.all.count > ChangeHistory::MAX_ENTRIES
      ids = ChangeHistory.all.reorder(created_at: :desc).drop(ChangeHistory::MAX_ENTRIES).pluck(:id)
      ChangeHistory.where(id: ids).destroy_all
    end
  end

  def get_attribute_name
    if workout_sessions?
      WorkoutSession.column_names[code].to_s
    elsif businesses?
      Business.column_names[code].to_s
    end
  end

  def get_username
    user.shortname
  end

  def get_old_value
    return value_definition(old_value)
  end

  def get_new_value
    return value_definition(value)
  end

  def value_definition(val)
    if (workout_sessions?) && (code == WorkoutSession.attrs_enums[:status])
      return WorkoutSession::STATUS[val.to_i].downcase
    end
  end

  def get_table_record
    if workout_sessions?
      WorkoutSession.unscoped.where(:business => business)
    elsif user_workouts?
      UserWorkout.unscoped.where(:business => business)
    elsif businesses?
      Business.unscoped.where(:business => business)
    end
  end

  def get_row_record
      get_table_record.unscoped.find(row)
  end

  def get_workout_from_row_record
    if workout_sessions?
      get_table_record.unscoped.find(row)
    elsif user_workouts?
      workout_id = get_table_record.unscoped.find(row).workout_session_id
      WorkoutSession.find(workout_id)
    end
  end

  def get_notifications(business, user)
    # To be implemented from _notifications/html.erb
  end
end
