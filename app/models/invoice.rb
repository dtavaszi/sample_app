class Invoice < ApplicationRecord
	belongs_to :business
	belongs_to :user
	belongs_to :membership, optional: true
	has_many :user_workouts
	has_many :payments
	has_many :invoice_items

	validates :id, uniqueness: { message: "Already in use" }

	def self.create_invoice_for(ws)
		if ws.billable_eventtype?
			ws.user_workouts.each do |uw|
				invoices_this_month = uw.user.invoices.where(:date => ws.date.beginning_of_month.to_date..ws.date.end_of_month.to_date, :business_id => ws.business.id)
				invoices_with_membership = invoices_this_month.where(:membership => true).order(date: :desc)

				assigned_invoice = false

				invoices_with_membership.each do |invoice|
					# if this invoice has not reached its limit, add this session to it
					if (invoice.user_workouts.includes(:workout_session).where(:membership => true).where("workout_sessions.status in (?)", [0,3]).order("workout_sessions.date").count < invoice.membership.max_workouts)
						uw.update_attributes(:invoice_id => invoice.id, :membership => true)
						assigned_invoice = true
						break
					end
				end

				# None of the invoices that are closed have membership or remaining slots
				unless assigned_invoice
					invoices_still_open = invoices_this_month.where(:processed => false).order(date: :desc)

					if invoices_still_open.empty?
						invoice = uw.user.invoices.create(:date => ws.date, :title => "#{(invoices_this_month.count + 1).to_s}", :membership => Invoice.apply_membership_or_nil(ws.business, uw, invoices_with_membership), :business_id => ws.business.id)
						uw.update_attributes(:invoice_id => invoice.id, :membership => invoice.membership_or_false)
					else
						invoice = invoices_still_open.first
						uw.update_attributes(:invoice_id => invoice.id, :membership => false)
					end
				end
			end
		end
	end

	def adjust_invoice
		user_workouts.update_all(:membership => false)

		if membership
			max_sessions = membership.max_workouts
			uws_billable = user_workouts.joins(:workout_session).where(workout_sessions: { status: WorkoutSession.billable_status })
			uws_billable.limit(max_sessions).update_all(:membership => true)
		end

		update(:amount => total_cost)
	end

	def self.revenue_groupby_month
		d1 = WorkoutSession.all.reorder(:date).limit(1).first.date
		d2 = Time.zone.now.end_of_month.end_of_day

		out = []
		while d2 > d1
			date_range = d1.beginning_of_month.beginning_of_day..d1.end_of_month.end_of_day
			out << invoices_in_month(date_range).sum(:amount)
			d1 += 1.month
		end

		out
	end

	def self.invoices_in_month(date_range = nil)
		date_range ||= Time.zone.now.beginning_of_month.beginning_of_day..Time.zone.now.end_of_month.end_of_day
		where(:date => date_range, :processed => true)
	end

	def amount
		self[:amount] ? self[:amount] / 100 : 0
	end

	def amount=(val)
		self[:amount] = val * 100 if val
	end

	def balance
		 payments.sum(:amount) - total_cost
	end

	def total_tax
		total_tax = 0
		if membership && membership.tax
			total_tax += membership.tax_amount
		end

		user_workouts.where(:membership => false).each do |uw|
			if uw.workout_session.tax
				total_tax += uw.cost * uw.workout_session.tax.rate if uw.cost
			end
		end

		invoice_items.each do |ii|
			total_tax += ii.total_tax
		end

		total_tax
	end

	def sessions_cost(include_tax = true)
		sessions_cost = 0
		user_workouts.includes(:workout_session).where(:membership => false).where("workout_sessions.status in (?)", [0,3]).order("workout_sessions.date").each do |uw|
			if uw.workout_session.tax && include_tax
				sessions_cost += uw.cost * uw.workout_session.tax.rate if uw.cost
			else
				sessions_cost += uw.cost
			end
		end

		sessions_cost
	end

	def membership_or_false
		if membership
			return membership
		else
			return false
		end
	end

	def total_cost(include_tax = true)
		total_cost = 0
		if membership
			if membership.tax && include_tax
				total_cost += membership.cost_with_tax
			else
				total_cost += membership.cost
			end

			user_workouts.includes(:workout_session).where(:membership => false).where("workout_sessions.status in (?)", [0,3]).order("workout_sessions.date").each do |uw|
				if uw.workout_session.tax && include_tax
					total_cost += uw.cost * uw.workout_session.tax.rate if uw.cost
				else
					total_cost += uw.cost
				end
			end
		else
			user_workouts.includes(:workout_session).where("workout_sessions.status in (?)", [0,3]).order("workout_sessions.date").each do |uw|
				if uw.workout_session.tax && include_tax
					total_cost += uw.cost * uw.workout_session.tax.rate if uw.cost
				else
					total_cost += uw.cost
				end
			end
		end

		invoice_items.each do |ii|
			total_cost += ii.total_cost(include_tax) if ii.item
		end

		round_to_nearest_5 total_cost
	end

	def reopen
		update_attributes(:processed => false, :reconciled => false)

		if save
				user.update_attribute(:credit, user.credit - balance)
		end
	end

	def settle
		bal = balance
		if  bal >= 0
			update_attributes(:processed => true, :balance => bal, :amount => total_cost)
			if save
				user.update_attribute(:credit, user.credit + bal)

				return true
			end
		else
			return false
		end
	end

	def reconcile!
		reconcile ? save : false
	end

	def reconcile
		if processed || reconciled
			self.reconciled = !reconciled
			self.reconciled_date = Time.zone.now
		else
			false
		end
	end

	def pay_with_credit
		if user.credit > 0 && balance < 0
			if user.credit > balance.abs
				payments.create(:payment_type => "User Credit", :amount => balance, :paydate => Time.zone.now.to_date)
				user.update(:credit => user.credit - balance.abs)
			else
				payments.create(:payment_type => "User Credit", :amount => user.credit, :paydate => Time.zone.now.to_date)
				user.update(:credit => 0)
			end
		end
	end

	def round_to_nearest_5(amount)
		5 * (amount/5).round(2)
	end


	def self.apply_membership_or_nil (business, uw, invoices_with_membership)
		# If there's a membership at all, don't charge for a second one in case it changes mid month and user books sessions. (i.e. :membership => uw.user.memberhsip)
		if invoices_with_membership.exists?
			nil
		else
			uw.user.get_membership(business.id)
		end
	end

	def default_workout_sessions_tax
		@default_workout_sessions_tax ||= Constant.default_workout_sessions_tax(business).value
	end

	def default_memberships_tax
		@default_memberships_tax ||= Constant.default_workout_sessions_tax(business).value
	end
end
