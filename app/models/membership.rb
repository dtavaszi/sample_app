class Membership < ApplicationRecord
	belongs_to :business
	belongs_to :tax, optional: true

	has_many :subscriptions
	has_many :users, through: :subscriptions
	has_many :invoices

	def cost_with_tax
		if cost && tax
			cost * tax.rate
		elsif cost
			cost
		else
			0
		end
	end

	def tax_amount
		if cost && tax
			cost * (tax.rate - 1)
		else
			0
		end
	end
end
