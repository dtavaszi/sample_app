class Profile < ApplicationRecord
  belongs_to :user, optional: true
	
	def address
		val = ""
		val += street if street
		val += ", " + city if city
		val += ", " + postalcode if postalcode
		val
	end
end
