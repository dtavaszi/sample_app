class UserRate < ApplicationRecord
	belongs_to :business
	belongs_to :user
	attr_accessor :rate_dollars, :rate_cents, :uid

	validate :is_new
	
	def is_new
		unless (ur = UserRate.where(:user => user).last).nil?
			if ur.rate == self[:rate]
				errors[:base] << "Rate must be new to update."
			end
		end
	end
end
