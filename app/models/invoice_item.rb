class InvoiceItem < ApplicationRecord
	belongs_to :business
	belongs_to :invoice
	belongs_to :item
	
	def total_cost(include_tax = true)
		if item.tax && include_tax
			item.price * quantity * (item.tax.rate)
		else
			item.price * quantity
		end
	end

	def total_tax
		if item.tax
			item.price * quantity * (item.tax.rate - 1)
		else
			0
		end
	end
end
