class Reminder < ApplicationRecord
	belongs_to :business, optional: true
	belongs_to :user
	belongs_to :workout_session, optional: true

	validate :has_date_or_workout_session
	validates :title, presence: true, allow_blank: false

	def has_date_or_workout_session
		if date.nil? && workout_session.nil?
			errors[:base] << "Date or event must be present"
		end
	end
end
