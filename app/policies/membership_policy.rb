class MembershipPolicy < ApplicationPolicy
  attr_reader :user, :membership

  def initialize(user, membership)
    @user = user
    @membership = membership
    @business = membership.business
  end

  # employee

  def index?
    @business.view_as_employee?(@user)
  end

  def show?
    @business.view_as_employee?(@user)
  end

  def new?
    @business.view_as_employee?(@user)
  end

  def edit?
    @business.view_as_employee?(@user)
  end

  def create?
    @business.view_as_employee?(@user)
  end

  def update?
    @business.view_as_employee?(@user)
  end

  def destroy?
    @business.view_as_employee?(@user)
  end

end
