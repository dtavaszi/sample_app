class SuggestionPolicy < Struct.new(:user, :suggestion)
  attr_reader :user, :suggestion

  def initialize(user, suggestion)
    @user = user
    @suggestion = suggestion
    @business = @suggestion.business
  end

  def index?
    @user.admin
  end

  def show?
    @user.admin
  end

  def new?
    logged_in?
  end

  def edit?
    @user.admin
  end

  def create?
    @business.subscribed_user?(@user.id) || @business.view_as_employee?(@user)
  end

  def update?
    @user.admin
  end

  def destroy?
    @user.admin
  end

end
