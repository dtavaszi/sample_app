class InvoiceMonthPolicy < ApplicationPolicy
  attr_reader :user, :invoice_month

  def initialize(user, invoice_month)
    @user = user
    @invoice_month = invoice_month
    @business = invoice_month.business
  end

  def flip?
    @business.view_as_employee?(@user)
  end

  def recalculate?
    @business.view_as_employee?(@user)
  end
end
