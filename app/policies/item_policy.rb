class ItemPolicy < ApplicationPolicy
  attr_reader :user, :item

  def initialize(user, item)
    @user = user
    @item = item
    @business = item.business
  end

  def index?
    @business.view_as_employee?(@user)
  end

  def show?
    @business.view_as_employee?(@user)
  end

  def new?
    @business.view_as_employee?(@user)
  end

  def edit?
    @business.view_as_employee?(@user)
  end

  def create?
    @business.view_as_employee?(@user)
  end

  def update?
    @business.view_as_employee?(@user)
  end

  def destroy?
    @business.view_as_employee?(@user)
  end

end
