class UserRatePolicy < ApplicationPolicy
  attr_reader :user, :rate

  def initialize(user, rate)
    @user = user
    @rate = rate
    @business = rate.business if @rate
  end

  def create?
    @business.view_as_employee?(@user)
  end

  def index?
    @business.view_as_employee?(@user)
  end
end
