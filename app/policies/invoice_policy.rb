class InvoicePolicy < ApplicationPolicy
  attr_reader :user, :invoice

  def initialize(user, invoice)
    @user = user
    @invoice = invoice
    @business = invoice.business
  end

  # subscriber
  def new?
    @business.view_as_employee?(@user) || @business.subscribed_user?(@user.id) && !InvoiceMonth.is_locked?(@business, @invoice.date)
  end

  def create?
    (@business.view_as_employee?(@user) || @business.subscribed_user?(@user.id)) && !InvoiceMonth.is_locked?(@business, @invoice.date)
  end

  def show?
    @business.view_as_employee?(@user) || (@invoice.user_id == @user.id)
  end

  # employee


  def index?
    @business.view_as_employee?(@user)
  end

  def edit?
    @business.view_as_employee?(@user) && !InvoiceMonth.is_locked?(@business, @invoice.date)
  end

  def update?
    @business.view_as_employee?(@user) && !InvoiceMonth.is_locked?(@business, @invoice.date)
  end

  def destroy?
    @business.view_as_employee?(@user)
  end

  def reconcile?
    @business.view_as_employee?(@user) && !InvoiceMonth.is_locked?(@business, @invoice.date)
  end

  def set_processed?
    @business.view_as_employee?(@user) && !InvoiceMonth.is_locked?(@business, @invoice.date)
  end

  def set_unprocessed?
    @business.view_as_employee?(@user) && !InvoiceMonth.is_locked?(@business, @invoice.date)
  end

  def settle_month?
    @business.view_as_employee?(@user)
  end

  def pay_with_credit?
    @business.view_as_employee?(@user) && !InvoiceMonth.is_locked?(@business, @invoice.date)
  end

  # admin



end
