class WorkoutSessionPolicy < ApplicationPolicy
  attr_reader :user, :workout_session

  def initialize(user, workout_session)
    @user = user
    @workout_session = workout_session
    @business = workout_session.business
  end

  def index?
    @business.view_as_employee?(@user)
  end

  def show?
    @business.view_as_employee?(@user) || @workout_session.partakes_in?(@user)
  end

  def view?
    if @workout_session.is_private?
      @business.view_as_employee?(@user) || @workout_session.partakes_in?(@user.id)
    else
      @business.view_as_employee?(@user) || @business.subscribed_user?(@user.id)
    end
  end

  def add?
    if @workout_session.is_private?
      @business.view_as_employee?(@user) || @workout_session.partakes_in?(@user.id)
    else
      @business.subscribed_user?(@user.id)
    end
  end

  def destroy? #need to double check
    @business.view_as_employee?(@user)
  end

  def book_groupsession?
    @business.view_as_employee?(@user) || @business.subscribed_user?(@user.id)
  end

  def cancel_groupsession?
    @business.view_as_employee?(@user) || @workout_session.partakes_in?(@user.id)
  end

  def create?
    (@business.view_as_employee?(@user) || @business.subscribed_user?(@user.id)) && !InvoiceMonth.is_locked?(@business, @workout_session.date)
  end

  def update?
    @business.view_as_employee?(@user) || @workout_session.partakes_in?(@user.id) && !InvoiceMonth.is_locked?(@business, @workout_session.date)
  end

  def remove?
    @business.view_as_employee?(@user)
  end

  def removefollowing?
    @business.view_as_employee?(@user)
  end

  def removeall?
    @business.view_as_employee?(@user)
  end

  def removefromsession?
    @business.view_as_employee?(@user)
  end

  def unlink?
    @business.view_as_employee?(@user)
  end

  def approveupto?
    @business.view_as_employee?(@user)
  end

  def approveallfollowing?
    @business.view_as_employee?(@user)
  end

  def approve?
    @business.view_as_employee?(@user)
  end

  def approveallthismonth?
    @business.view_as_employee?(@user)
  end

  def success?
    @business.view_as_employee?(@user)
  end

  def reschedule?
    @business.view_as_employee?(@user) || @workout_session.partakes_in?(@user.id)
  end

  def cancel?
    @business.view_as_employee?(@user) || @workout_session.partakes_in?(@user.id)
  end

  def noshow?
    @business.view_as_employee?(@user)
  end

  def sync_google_event?
    @business.view_as_employee?(@user)
  end
end
