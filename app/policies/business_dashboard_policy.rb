class BusinessDashboardPolicy < Struct.new(:user, :business)
  attr_reader :user, :business

  def initialize(user, business)
    @user = user
    @business = business
  end

  def show?
    @business.view_as_employee?(@user)
  end
end
