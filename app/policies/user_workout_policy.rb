class UserWorkoutPolicy < ApplicationPolicy
  attr_reader :user, :user_workout

  def initialize(user, user_workout)
    @user = user
    @user_workout =  user_workout
    @business = user_workout.business
  end

  def update?
    @business.view_as_employee?(@user)
  end

  def edit?
    @business.view_as_employee?(@user)
  end

  def setoptions?
    @business.view_as_employee?(@user)
  end

end
