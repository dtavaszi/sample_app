class ConstantPolicy < ApplicationPolicy
  attr_reader :user, :constant

  def initialize(user, constant)
    @user = user
    @constant = constant
    @business = constant.business
  end

  # employee

  def index?
    @business.view_as_employee?(@user)
  end

  def show?
    @business.view_as_employee?(@user)
  end

  def new?
    @business.view_as_employee?(@user)
  end

  def edit?
    @business.view_as_employee?(@user)
  end

  def create?
    @business.view_as_employee?(@user)
  end

  def update?
    @business.view_as_employee?(@user)
  end

  def destroy?
    @business.view_as_employee?(@user)
  end

end
