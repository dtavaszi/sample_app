class PaymentPolicy < ApplicationPolicy
  attr_reader :user, :payment

  def initialize(user, payment)
    @user = user
    @payment = payment
    @business = payment.business
  end

  def index?
    @business.view_as_employee?(@user)
  end

  def show?
    @business.view_as_employee?(@user)
  end

  def new?
    @business.view_as_employee?(@user)
  end

  def edit?
    @business.view_as_employee?(@user)
  end

  def create?
    @business.view_as_employee?(@user)
  end

  def update?
    @business.view_as_employee?(@user)
  end

  def destroy?
    @business.view_as_employee?(@user)
  end

end
