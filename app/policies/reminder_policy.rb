class ReminderPolicy < ApplicationPolicy
  attr_reader :user, :reminder

  def initialize(user, reminder)
    @user = user
    @reminder = reminder
    @business = reminder.business
  end

  # employee

  def index?
    @user.admin?
  end

  def show?
    @user == reminder.user
  end

  def new?
    true
  end

  def edit?
    @user == reminder.user
  end

  def create?
    @user == reminder.user
  end

  def update?
    @user == reminder.user
  end

  def destroy?
    @user == reminder.user
  end

  def view?
    @user == reminder.user
  end

end
