class UserPolicy < ApplicationPolicy
  attr_reader :user, :target_user

  def initialize(user, target_user)
    @user = user
    @target_user = target_user
    @business = target_user.target_business if @target_user
  end

  # any

  def new?
    true
  end

  def new_embedded?
    true
  end

  def create?
    true
  end

  # user only

  def destroy?
    @user == target_user || @user.admin?
  end

  def resync_google?
    @user == target_user || @user.admin?
  end

  def new_gdrive_workout?
    @business.view_as_employee?(@user)
  end

  # subscriber

  # employee

  # A user won't be able to be edited later on by a business, but for now they can
  # A business will be able to edit only the subscription, or a business profile

  def new_modal?
    @business.view_as_employee?(@user)
  end

  def edit_modal?
    @business.view_as_employee?(@user)
  end

  def edit?
    if @business
      @business.view_as_employee?(@user) || @user == target_user
    else
      @user == target_user || @user.admin
    end
  end

  def settings?
    if @business
      @business.view_as_employee?(@user) || @user == target_user
    else
      @user == target_user || @user.admin
    end
  end

  def approve?
    @business.view_as_employee?(@user)
  end

  def index?
    @business.view_as_employee?(@user)
  end

  # profiles are currently disabled / not implemented

  def show?
    false
  end

  def update?
    if @business
      @business.view_as_employee?(@user)
    else
      @user == target_user || @user.admin
    end
  end


  # admin

  def admin_index?
    @user.admin?
  end

  def admin_create?
    @business.view_as_employee?(@user)
  end
end
