class BusinessPolicy < ApplicationPolicy
  attr_reader :user, :business

  def initialize(user, business)
    @user = user
    @business = business
  end

  # subscriber

  def show?
    @business.view_as_employee?(@user) || @business.subscribed_user?(@user.id)
  end

  # employee

  def get_month?
    @business.view_as_employee?(@user) || @business.subscribed_user?(@user.id)
  end

  def calendar?
    @business.view_as_employee?(@user) || @business.subscribed_user?(@user.id)
  end

  def dashboard?
    @business.view_as_employee?(@user)
  end

  def edit?
    (@business.owner == @user) || @user.admin?
  end

  def update?
    (@business.owner == @user) || @user.admin?
  end

  def destroy?
    (@business.owner == @user) || @user.admin?
  end

  # admin
  def new?
    @user
  end

  def index?
    @user.admin?
  end

  def create?
    @user
  end
end
