//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require bootstrap
//= require moment
//= require bootstrap-datetimepicker
//= require bootstrap-toggle
//= require select2
//= require turbolinks
//= require_tree .

$( document ).on('turbolinks:load', function() {
	$(".invoice_trigger").click(function(event) {
    if (event.target.type !== 'checkbox') {
      $(':checkbox', this).trigger('click');
    }
  });
})
