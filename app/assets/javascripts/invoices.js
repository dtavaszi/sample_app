//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require bootstrap
//= require moment
//= require bootstrap-datetimepicker
//= require bootstrap-toggle
//= require select2
//= require turbolinks
//= require_tree .

$( document ).on('turbolinks:load', function() {
	$('#selectAllAvailable').click(function() {
   if (this.checked) {
       $('input[id^="user_workout_ids"]').each(function() {
           this.checked = true;                        
       });
   } else {
      $('input[id^="user_workout_ids"]').each(function() {
           this.checked = false;                        
       });
   } 
	});
	
	$('#selectAllFinalized').click(function() {
   if (this.checked) {
       $('input[id^="user_workout_ids"]').each(function() {
           this.checked = true;                        
       });
   } else {
      $('input[id^="user_workout_ids"]').each(function() {
           this.checked = false;                        
       });
   } 
	});	

	$('#selectAllItemsFinalized').click(function() {
   if (this.checked) {
       $('input[id^="user_workout_ids"]').each(function() {
           this.checked = true;                        
       });
   } else {
      $('input[id^="user_workout_ids"]').each(function() {
           this.checked = false;                        
       });
   } 
	});
})