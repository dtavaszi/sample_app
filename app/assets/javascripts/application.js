//= require jquery
//= require jquery-ui
//= require jquery.turbolinks
//= require jquery_ujs
//= require bootstrap
//= require moment
//= require bootstrap-datetimepicker
//= require bootstrap-toggle
//= require select2
//= require turbolinks
//= require_tree .
//= require jsapi
//= require chartkick

$(document).on('page:fetch', function() {
  $(".loading-indicator").show();
});
$(document).on('page:change', function() {
  $(".loading-indicator").hide();
});

$( document ).on('turbolinks:load', function() {
		$('.edit_event').click(function(){
			var event_modal = document.getElementById("session_modal")
			event_modal.style.display = "block";
		});

		$('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover({container: 'body'});

		$('.userselect_clients').select2({
			placeholder: "Select clients..",
			closeOnSelect: false,
			allowClear: true
		});

		$('.userselect_friends').select2({
			placeholder: "Select friends..",
			closeOnSelect: false,
			allowClear: true
		});

		$('.eventselect_eventtype').select2({
			placeholder: "Select type..",
      minimumResultsForSearch: Infinity
		});
})
