json.array!(@reminders) do |reminder|
  json.extract! reminder, :id, :user_id, :date, :workout_session_id, :memo
  json.url reminder_url(reminder, format: :json)
end
