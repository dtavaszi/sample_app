json.array!(@suggestions) do |suggestion|
  json.extract! suggestion, :id, :user_id, :business_id, :category, :location, :body
  json.url suggestion_url(suggestion, format: :json)
end
