json.array!(@payments) do |payment|
  json.extract! payment, :id, :payment_type, :amount, :user_id, :invoice_id
  json.url payment_url(payment, format: :json)
end
