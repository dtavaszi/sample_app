json.array!(@subscriptions) do |subscription|
  json.extract! subscription, :id, :user_id_id, :business_id, :type
  json.url subscription_url(subscription, format: :json)
end
