module WorkoutSessionsHelper
	def nl2br(s)
	  s.gsub(/\n/, '<br>')
	end

	def render_view_for(event, event_new, date, user)
		if event.business.view_as_employee?(user)
			case event.eventtype
			when WorkoutSession.workout
				puts "workout admin"
				return render 'session_workout_admin', ws: event, ws_new: event_new
			when WorkoutSession.event
				puts "event admin"
				return render 'session_event', ws: event
			when WorkoutSession.groupworkout
				puts "instance group admin"
				return render 'session_groupworkout_admin', ws: event
			when -WorkoutSession.groupworkout
				puts "repeating group admin"
				return render 'session_repeating_groupworkout_admin', ws: event
			end
		else
			case event.eventtype
			when WorkoutSession.workout
				puts "workout client"
				return render 'session_workout_client', ws: event, ws_new: event_new
			when WorkoutSession.event
				puts "event client"
				#return render 'session_event', ws: event
			when WorkoutSession.groupworkout
				puts "group client"
				return render 'session_groupworkout_client', ws: event
			when -WorkoutSession.groupworkout
				puts "repeating group client"
				return render 'session_repeating_groupworkout_client', ws: event
			end
		end
	end

	def paid_status(ws)
		if ws.paid?
			return "Paid"
		else
			return "Unpaid"
		end
	end

  def is_groupworkout_and_in_futuredate_or_partakes_in(event, partakes_in_hash, date = nil)
		if event.eventtype == WorkoutSession.groupworkout && (event.date > Time.zone.now || partakes_in_hash.key?(event.id))
			return true
		elsif event.eventtype == -WorkoutSession.groupworkout && (date > Time.zone.now || partakes_in_hash.key?(event.id))
			return true
		end
		return false
  end

	def date_for(w)
		"<span class='event_header_day'>" + w.date.strftime("%A").upcase + "</span> </br> " + w.date.strftime("<span class='event_header_date'> %B %-d, %Y at %-I:%M%p</span>")
	end

  def date_for_old(w)
    w.date.strftime("%A %B %-d, %Y")
	end

	def date_time_for(w)
		start_date = w.date
		start_date.strftime("%B %-d, %Y @ %I:%M%P")
	end

	def buffer_time_for(w)
		w.bfr_from.to_s + ' min <span class="glyphicon glyphicon-transfer" aria-hidden="true"></span> ' + w.bfr_to.to_s + " min"
	end

	def date_until(w)
		if Time.zone.now.to_date == w.date.to_date
			"Today"
		elsif Time.zone.now.to_date < w.date.to_date
			d = (w.date.to_date - Time.zone.today.to_date).to_i
			"Coming up in " + pluralize(d, "day")
		else
			d = (Time.zone.now.to_date - w.date.to_date ).to_i
			pluralize(d, "day") + " ago"
		end
	end

	def assigned_clients(w)
		clients = UserWorkout.where(:workout_session_id => w.id)
		temp = []
		clients.each do |c|
			temp << c.user_id
		end

		temp
	end

	def can_edit(ws)
		if current_user.admin
			true
		elsif [0,4].include?(ws.status) && (ws.date > Time.zone.now + 1.days)
			true
		else
			false
		end
	end

	def edit_status(ws)
		if can_edit(ws)
			"<span class='glyphicon success glyphicon-ok-sign'></span>"
		else
			"<span class='glyphicon noshow glyphicon-exclamation-sign'></span>"
		end
	end
end
