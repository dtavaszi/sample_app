module BusinessesHelper

  def bookable_collection
    bookable_collection = {}
    bookable_collection["Session"] = WorkoutSession.workout if @business.allow_private_session?
    bookable_collection["Group Session"] = WorkoutSession.groupworkout if @business.allow_group_session?
    bookable_collection["Personal Event"] = WorkoutSession.event

    bookable_collection
  end

  def weekly?
    @weekly
  end

  def monthly?
    @monthly
  end

  def user_type?(user)
    if @business.view_as_employee?(user)
      "employee"
    else
      "client"
    end
  end

  def get_events_json
    workout_sessions = WorkoutSession.where(:business => @business,)

    only = [ :id, :date, :duration, :bfr_from, :bfr_to ]

    workout_sessions.to_json(only: only).html_safe
  end

  def render_events_for_employee(event, date, user)
    case event.eventtype
    when WorkoutSession.workout
      return render('workout_admin', workout: event)
    when WorkoutSession.event
      return render('event_admin', event: event)
    when WorkoutSession.groupworkout
      return render('groupworkout_admin', groupworkout: event)
    when -WorkoutSession.groupworkout
      return render('repeating_groupworkout_admin', groupworkout: event, date: date)
    end
  end
  def render_events_for_client(event, date, user)
    case event.eventtype
    when WorkoutSession.workout
      return render('workout_client', workout: event)
    when WorkoutSession.event
      return render('event_client', event: event)
    when WorkoutSession.groupworkout
      return render('groupworkout_client', groupworkout: event)
    when -WorkoutSession.groupworkout
      return render('repeating_groupworkout_client', groupworkout: event, date: date)
    end
  end

  def calendar_weekly_time_block(date = Time.zone.now, &block)
    CalendarWeekly.new(self, date, block, nil, nil).time_block
  end

  def calendar_weekly_events_block(&block)
    CalendarWeekly.new(self, nil, block, nil, nil).flex_containers
  end

  def calendar_weekly_horiz_grid(date, &block)
    CalendarWeekly.new(self, date, block, nil, nil).horizontal_grid
  end

  def calendar_weekly_day(date = Time.zone.now.to_date, for_user, events, &block)
    CalendarWeekly.new(self, date, block, @business.view_as_employee?(for_user), events).day_column(date)
  end

  def calendar(date = Date.today, &block)
    Calendar.new(self, date, block, @business.view_as_employee?(@for_user)).div
  end

  def reminders_for(date = Date.today, for_user, reminders, &block)
    CalendarReminder.new(self, date, reminders, block).day_reminders
  end

  def events_for(date = Date.today, events, &block)
    CalendarEvent.new(self, date, events, block).day_events
  end

  def business_color_css(business)
    business.color.present? ? (business.color) : 'white'
  end

  def overflow
    content_tag :table, class: "overflow" do
        content_tag :tr, class: "overflow" do
          content_tag :td, class: "overflow" do
            yield
        end
      end
    end
  end

  ###
  def end_time_for(w, busy = false)
  duration = w.duration
    if @business.view_as_employee?(current_user)
      return duration.strftime("%l:%M%P").chomp("m") unless w.duration.min == 0
      return duration.strftime("%l%P").chomp("m")
    else
      duration = duration + w.bfr_to.minutes if busy
      return duration.strftime("%l:%M%p") unless duration.min == 0
      return duration.strftime("%l%p")
    end
  end

  def start_time_for(w, busy = false)
    date = w.date
    if @business.view_as_employee?(current_user)
      return date.strftime("%l:%M%P").chomp("m") unless w.date.min == 0
      return date.strftime("%l%P").chomp("m")
    else
      date = date - w.bfr_from.minutes if busy
      return date.strftime("%l:%M%p") unless date.min == 0
      return date.strftime("%l%p")
    end
  end

  def event_time_for(w, busy = false)
    s = start_time_for(w, busy)
    e = end_time_for(w, busy)
    if (w.date + 1.hour == w.duration && @business.view_as_employee?(current_user))
      s
    else
      s + " &ndash; " + e
    end
  end

  def event_time_full_for(w)
    start_time = w.date
    end_time = w.duration
    start_time.strftime("%l:%M%P") + " &ndash; " + end_time.strftime("%l:%M%P")
  end

  def event_time_from_to(w1, w2, busy = false)
      s = start_time_for(w1, true)
      e = end_time_for(w2, true)
      s + " &ndash; " + e
  end

  def end_to_start(w1, w2, busy = false)
    if (w1 && w2)
      if ensure_one_hour_between_events(w1, w2)
        e = end_time_for(w1, true)
        s = start_time_for(w2, true)
        e + " &ndash; " + s
      else
        nil
      end
    elsif w1
      e = end_time_for(w1, true)
      e + " &ndash; " + "Midnight"
    else
      s = start_time_for(w2, true)
      "Morning" + " &ndash; " + s
    end
  end

  def ensure_one_hour_between_events (w1, w2)
    (w2.date - w2.bfr_from.minutes) > (w1.duration + w1.bfr_to.minutes + 1.hour)
  end

  def next_user_sesh uid
    ws = WorkoutSession.joins("JOIN user_workouts ON workout_sessions.id = user_workouts.workout_session_id AND user_workouts.user_id =" + uid.to_s).where("date > ?", Time.zone.now).reorder(:date).first
  end

  def next_event
    WorkoutSession.where("date >= ? AND eventtype = ?", Time.zone.now, 1)
  end

  def today?(datetime)
    datetime.to_date == Time.zone.now.to_date
  end

  def next_month
    schedule_path + '?month=' + (@m + 1).to_s
  end

  def previous_month
    schedule_path + '?month=' + (@m + -1).to_s
  end

  def repeat_groupworkout_status(ws, date, tooltip = false)
    if date > Time.zone.now
      status = "unchecked upcoming"
      text = "Session coming up"
    else
      status = "ok success"
      text = "Session successful"
    end

    if tooltip
      return "<span class='glyphicon glyphicon-#{status}'></span> <small id='small_status'> #{text}</small>"
    else
      return "<span class='glyphicon glyphicon-#{status}'></span>"
    end
  end

  def repeat_on_to_weekdays(repeat_on)
    out = ""

    arr = repeat_on.split("")
    first = true
    arr.each_with_index do |char, index|
      if char == "1"
        (out << ", ") unless first
        (out << Date::ABBR_DAYNAMES[index])
        first = false
      end
    end

    out
  end

  def workout_status(ws, reschedules_list = nil, tooltip = false)
    @rescheduled = nil
    reschedules_list = Reschedule.all unless reschedules_list
    text = nil

    if reschedules_list.where(:link => ws.id).empty?
      status = "unchecked upcoming"
      text = "Session coming up"

      case ws.status
        when WorkoutSession::STATUS["ok"]
          if Time.zone.now > ws.date
            status = "ok success"
            text = "Session successful"
          end
        when WorkoutSession::STATUS["reschedule"]
          @rescheduled = true
          if @workout_sessions_all && !(rs = reschedules_list.where(:workout_session_id => ws.id)).empty?
            if !(w = @workout_sessions_all.where(:id => rs.first.link)).empty?
              rescheduled = w.first.date.strftime("%-d %b, %Y")
              rescheduled_title = "Rescheduled to"
            end
          end
          status = "share-alt rescheduled_from"
          text = "Session rescheduled to #{rescheduled}"
        when WorkoutSession::STATUS["cancel"]
          status = "remove cancelled"
          text = "Session cancelled"
        when WorkoutSession::STATUS["noshow"]
          status = "remove-sign noshow"
          text = "Session was a no show"
        when WorkoutSession::STATUS["draft"]
          status = "flag draft"
          text = "Session pending approval"
      end
    else
      if @workout_sessions_all && !(rs = reschedules_list.where(:link => ws.id)).empty?
        if  !(w = @workout_sessions_all.where(:id => rs.first.workout_session_id)).empty?
          rescheduled = w.first.date.strftime("%-d %b, %Y")
          rescheduled_title = "Rescheduled from"
        end
      end

      status = "unchecked upcoming_re"
      text = "Session coming up"

      case ws.status
        when WorkoutSession::STATUS["ok"]
          if Time.zone.now > ws.date
            status = "exclamation-sign success_re"
            text = "Session successful"
          end
        when WorkoutSession::STATUS["reschedule"]
          status = "share-alt rescheduled_from"
          text = "Session coming up."
          text += "Rescheduled from #{rescheduled}" if rescheduled
        when WorkoutSession::STATUS["cancel"]
          status = "remove cancelled_re"
          text = "Session cancelled"
        when WorkoutSession::STATUS["noshow"]
          status = "remove-sign noshow_re"
          text = "Session was a no show"
        when WorkoutSession::STATUS["draft"]
           status = "flag draft"
          text = "Session pending approval"
       end
    end

    if tooltip
      return "<span class='glyphicon glyphicon-#{status}' data-title='#{rescheduled_title}' data-content='#{rescheduled}' data-html='true' data-toggle='popover' data-trigger='hover' data-placement='top'></span> <small id='small_status'> #{text}</small>"
    else
      return "<span class='glyphicon glyphicon-#{status}' data-title='#{rescheduled_title}' data-content='#{rescheduled}' data-html='true' data-toggle='popover' data-trigger='hover' data-placement='top'></span>"
    end
  end

  def get_buffer(ws)
    (ws.bfr_from.to_s || "0") + "/" + (ws.bfr_to.to_s || "0")
  end

  def reminder_popover(ws)
    val = ""
    ws.reminders.where(:user => current_user).each do |r|
      val += r.memo + "</br>"
    end

    return val
  end

	def current_date(date)
		date.beginning_of_day..date.end_of_day
	end

	def start_and_end_day_wrap (date, events, &block)
		if events.any?
			start_day_wrap(date, events).html_safe + capture(&block) + end_day_wrap(date, events).html_safe
		elsif date > Time.zone.now
			available_all_day_wrap.html_safe
		end
	end

	def start_day_wrap (date, events)
		if (date > Time.zone.now) && (events.first.date > events.first.date.beginning_of_day + 1.hour)
			open_client_wrap end_to_start(nil, events.first)
		else
			""
		end
	end

	def end_day_wrap (date, events)
		if date > Time.zone.now && events.last.date > events.last.date.beginning_of_day
			open_client_wrap end_to_start(events.last, nil)
		else
			""
		end
	end

  def mid_day_wrap (date, events)

  end

	def open_client_wrap(t)
		content_tag :tr do
			content_tag :td, class: "day", colspan: 2 do
				content_tag :div, class: "div_open_client" do
					content_tag :i, class: "alert-success i_open_client_time" do
						t.html_safe
					end
				end
			end
		end
	end

	def available_all_day_wrap
		content_tag :tr do
			content_tag :td, class: "day", colspan: 2 do
				content_tag :div, class: "div_open_client" do
					content_tag :small, class: "i_available_client_time" do
						"(Available)".html_safe
					end
				end
			end
		end
	end
end
