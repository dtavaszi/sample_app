module ApplicationMailerHelper
    def mailer_date_strftime (date)
      date.strftime("%-l:%M%p %B %-d, %Y")
    end
end
