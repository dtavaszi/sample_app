module FriendsHelper

	def get_friends_for user
		user_list = []
		
		fs = Friend.where("user1_id = ? OR user2_id = ?", user.id, user.id)
		
		fs.each do |f|
			if f.user1_id == user.id
				user_list << f.user2_id
			else
				user_list << f.user1_id
			end
		end
		
		User.where(:id => user_list)
	end
	
	def get_friends_for_client user
		user_list = []
		
		fs = Friend.where("user1_id = ? OR user2_id = ? AND status = ?", user.id, user.id, 0)
		
		fs.each do |f|
			if f.user1_id == user.id
				user_list << f.user2_id
			else
				user_list << f.user1_id
			end
		end
		
		User.where(:id => user_list)
		
	end

end
