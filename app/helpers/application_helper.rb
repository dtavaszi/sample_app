module ApplicationHelper
  include ApplicationMailerHelper
  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "F!Trackular"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  def today_or_tomorrow date
    if date.beginning_of_day == Time.zone.now.beginning_of_day
      "Today"
    elsif date == (Time.zone.now + 1.day).beginning_of_day
      "Tomorrow"
    end
  end

	def next_session_alert
		if logged_in? && @business
			if @business.view_as_employee?(current_user)
				ws = WorkoutSession.where("date >= ?", Time.zone.now).reorder(:date).first
					if !ws.nil?
						link_to ws.date.strftime("Your next event is at %l:%M%p on %B %-d, %Y"), business_workout_session_path(ws.business.slug, ws), style: "color: white; text-decoration: none;"
					else
						"You have no upcoming events or sessions."
					end
			else
				ws_next = next_user_sesh current_user.id
				if !ws_next.nil?
            ws = WorkoutSession.find(ws_next.id)
						link_to (ws_next.date).strftime("Your next session is at %l:%M%p on %B %-d, %Y"), business_workout_session_path(ws.business.slug, ws), style: "color: white; text-decoration: none;"
				else
						"You have no upcoming sessions."
				end
			end
		end
	end

  def request_path_includes(allowedPaths)
		path = Rails.application.routes.recognize_path(request.env['PATH_INFO'] )
		allowedPaths.include?(path[:controller] + "/" + path[:action])
	end

  def active_path?(path, path_string)
    path == path_string
  end
end
