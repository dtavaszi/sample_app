module InvoicesHelper
	def show_controls?
		!(@invoice.processed? && @im.locked?)
	end

	def get_invoice_workout_count(invoice)
		invoice.user_workouts.includes(:workout_session).where("workout_sessions.status" => [0,3]).count
	end

  def reset_monthly()
    @monthly_sessions = 0
    @monthly_cost = 0
    @monthly_paid = 0
    @monthly_difference = 0
  end

	def graph_sessions_piechart(date_range, height)
		workouts_all = WorkoutSession.where(:date => date_range)
		successful_workouts = workouts_all.where(:status => 0).count
		rescheduled_workouts = workouts_all.where(:status => 1).count
		cancelled_workouts = workouts_all.where(:status => 2).count
		noshow_workouts = workouts_all.where(:status => 3).count

		output = [["Successful", successful_workouts], ["Rescheduled", rescheduled_workouts],["Cancelled", cancelled_workouts],["No show", noshow_workouts], ]

		pie_chart output, colors: ["#94b8d6", "#facbb8", "#4D4D4D", "#F15854"], legend: false, height: height, donut: true, label: "$", library: {segmentShowStroke: false, backgroundColor: "transparent", plotOptions: { pie: { dataLabels: { enabled: false }, showInLegend: true } }}
	end

	def invoicerevenue_over_time_linechart(date_range, height)
		total = []
		total2 = []
		invoices = @invoices.where(:date => date_range).where.not(:amount => nil).reorder(date: :asc).group_by{ |i| i.date.beginning_of_month}

		invoices.each do |m, i|
			total.append([m.strftime("%B"), (invoices[m].pluck(:amount).sum / 100) + 2000])
			total2.append([m.strftime("%B"), invoices[m].pluck(:amount).sum / 100 + (100 + Random.rand(50))])
		end

		invoices_linechart([{name: "testing", data: total}, {name: "testing2", data: total2}], height)
	end

	def invoices_linechart(data, height)
		line_chart data, stacked: true, label: "$", library: { vAxis: {gridlines: {color: "transparent"}}, backgroundColor: "transparent", curveType: "none"}, legend: "bottom", height: height, colors: ["#337ab7", "#FFA07A"]
	end
end
