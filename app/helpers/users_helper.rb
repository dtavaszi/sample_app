module UsersHelper

  def output_error_messages(err_msgs)
    output = ""
    err_msgs.each do |msg|
      output << msg + "<br>"
    end
    escape_javascript output.html_safe
  end

  # Returns the Gravatar for the given user.
  def gravatar_for(user, options = {})
    size = options[:size] || 80
    cl = options[:class] || "gravatar"

    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    size = options[:size]
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: user.name, :class => cl.html_safe)
  end

	def sesh_attended(user, month_date = nil)
		if month_date
			WorkoutSession.joins("JOIN user_workouts ON workout_sessions.id = user_workouts.workout_session_id AND user_workouts.user_id =" + user.id.to_s).where(:status => 0, :eventtype => 1, :date => month_date.beginning_of_month..month_date.end_of_month).count
		else
			WorkoutSession.joins("JOIN user_workouts ON workout_sessions.id = user_workouts.workout_session_id AND user_workouts.user_id =" + user.id.to_s).where('date < ? AND status = ? AND eventtype = ?', Time.zone.now, 0, 1).count
		end
	end

	def sesh_missed(user, month_date = nil)
		if month_date
			WorkoutSession.joins("JOIN user_workouts ON workout_sessions.id = user_workouts.workout_session_id AND user_workouts.user_id =" + user.id.to_s).where(:status => 3, :eventtype => 1,:date => month_date.beginning_of_month..month_date.end_of_month).count
		else
			WorkoutSession.joins("JOIN user_workouts ON workout_sessions.id = user_workouts.workout_session_id AND user_workouts.user_id =" + user.id.to_s).where('date < ? AND status = ? AND eventtype = ?', Time.zone.now, 3, 1).count
		end
	end

	def sesh_upcoming(user)
		WorkoutSession.joins("JOIN user_workouts ON workout_sessions.id = user_workouts.workout_session_id AND user_workouts.user_id =" + user.id.to_s).where('date > ? AND status = ? AND eventtype = ?', Time.zone.now, 0, 1).count
	end

	def sesh_total(user, month_date = nil)
		if month_date
			WorkoutSession.joins("JOIN user_workouts ON workout_sessions.id = user_workouts.workout_session_id AND user_workouts.user_id =" + user.id.to_s).where(:status => 0, :eventtype => 1,:date => month_date.beginning_of_month..month_date.end_of_month).count
		else
			WorkoutSession.joins("JOIN user_workouts ON workout_sessions.id = user_workouts.workout_session_id AND user_workouts.user_id =" + user.id.to_s).where(:status => 0, :eventtype => 1).count
		end
	end

	def sesh_cost(user, month_date = nil)
		unless month_date
			sesh = UserWorkout.joins("JOIN workout_sessions ON workout_sessions.id = user_workouts.workout_session_id AND user_workouts.user_id = #{user.id} AND workout_sessions.eventtype = 1 AND workout_sessions.status = 0")
		else
			sesh = UserWorkout.joins("JOIN workout_sessions ON workout_sessions.id = user_workouts.workout_session_id AND user_workouts.user_id = #{user.id} AND workout_sessions.eventtype = 1 AND workout_sessions.status = 0").where(:workout_sessions => {:date => month_date.beginning_of_month..month_date.end_of_month})
		end

		total = 0
		sesh.each do |s|
			if (cost = s.cost)
				total += cost
			end
		end

		number_with_precision(total, :precision => 2)
	end

	def sesh_owing(user, month_date = nil)
		if month_date
			sesh = UserWorkout.joins("JOIN workout_sessions ON workout_sessions.id = user_workouts.workout_session_id AND user_workouts.user_id = #{user.id} AND workout_sessions.eventtype = 1 AND workout_sessions.status = 0").where(:workout_sessions => {:date => month_date.beginning_of_month..month_date.end_of_month})
		else
			sesh = UserWorkout.joins("JOIN workout_sessions ON workout_sessions.id = user_workouts.workout_session_id AND user_workouts.user_id = #{user.id} AND workout_sessions.eventtype = 1 AND workout_sessions.status = 0")
		end

		total = 0
		sesh.each do |s|
			if (cost = s.cost) && !s.paid
				total += cost
			end
		end

		number_with_precision(total, :precision => 2)
	end

	def overview_for(user, month_date = nil)
		if month_date
			sesh = UserWorkout.joins("JOIN workout_sessions ON workout_sessions.id = user_workouts.workout_session_id AND user_workouts.user_id = #{user.id} AND workout_sessions.eventtype = 1 AND workout_sessions.status = 0").where(:workout_sessions => {:date => month_date.beginning_of_month..month_date.end_of_month}).includes(:workout_session)
		else
			sesh = UserWorkout.joins("JOIN workout_sessions ON workout_sessions.id = user_workouts.workout_session_id AND user_workouts.user_id = #{user.id} AND workout_sessions.eventtype = 1 AND workout_sessions.status = 0").includes(:workout_session)
		end

		overview = ""
		sesh.each do |s|
			if s.cost
				overview += s.workout_session.date.strftime("%B %d - $") + number_with_precision(s.cost, :precision => 2).to_s + "</br>"
			else
				overview += s.workout_session.date.strftime("%B %d - N/A </br>")
			end
		end

		overview
	end

	def general_settings
    if @business
      if @business.view_as_employee?(current_user)
        return business_settings_path(@business.slug) + "/?tab=general&user=#{@user.id}"
      else
        return business_settings_path(@business.slug) + '/?tab=general&'
      end
    else
      if current_user.admin?
        settings_path + "/?tab=general&user=#{@user.id}"
      else
        settings_path + '/?tab=general'
      end
    end
	end

	def billing_settings
    if @business
      if @business.view_as_employee?(current_user)
        business_settings_path + "/?tab=billing&user=#{@user.id}"
      else
        business_settings_path + '/?tab=billing'
      end
    else
      if current_user.admin?
        settings_path + "/?tab=billing&user=#{@user.id}"
      else
        settings_path + '/?tab=billing'
      end
    end
	end

	def admin_settings
    if current_user.admin?
      if @business
		    business_settings_path(@business.slug) + "/?tab=admin&user=#{@user.id}"
      else
    		settings_path + "/?tab=admin&user=#{@user.id}"
      end
    end
	end

	def user_settings_for(user)
    @user = user
    general_settings
	end

	def user_billing_for(user)
		@user = user
    billing_settings
	end

	def user_admin_for(user)
		@user = user
    admin_settings
	end

	def approved?(user)
		if user.account
				return user.account.approved
		else
			false
		end
	end

end
