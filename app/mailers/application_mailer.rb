class ApplicationMailer < ActionMailer::Base
  require 'mail'
  add_template_helper(ApplicationHelper)
  helper :users

  default from: 'Fitrackular <noreply@fitrackular.com>'
  layout 'mailer'

  def activation_email(user)
    @user = user
    mail(to: user.email, subject: 'Email verification')
  end

  def welcome_email(user)
    @user = user
    mail(to: user.email, subject: 'Welcome to Fitrackular!')
  end

  def booking_notification_email(business, changes)
    @changes = changes
    @business = business
    @user = @business.user
    mail(to: @user.email, subject: 'Booking Notifications')
  end

  def send_workouts_reminder(user, user_workouts)
    @user = user
    @user_workouts = user_workouts
    puts "Sending #{ user.shortname } workouts reminder"
    mail(to: @user.email, subject: 'Reminder: Upcoming sessions')
  end
end
