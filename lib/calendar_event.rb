class CalendarEvent < Struct.new(:view, :date, :events, :callback)
    delegate :content_tag, to: :view

    def day_events
      # Need to implement USER / BUSINESS EVENTS, not APP EVENTS
      events.where(:date => current_date).map do |event|
        view.capture(event, &callback)
      end.join.html_safe
    end

    private
      def current_date() date.beginning_of_day..date.end_of_day end
end
