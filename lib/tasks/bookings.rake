namespace :bookings do
  desc "Sends notification to business owners as to the most recent bookings"
  task send_notifications: :environment do
    puts "Sending recent booking notifications"
    Business.all.each do |business|
      business.send_booking_notifications
    end
  end

  desc "Send reminders for session in the next morning"
  task send_morning_notifications: :environment do
    Business.all.each do |business|
      puts "sending #{ business.id } morning notifications"
      business.send_morning_notifications
    end
  end

  desc "Send reminders for sessions this evening"
  task send_evening_notifications: :environment do
    Business.all.each do |business|
      puts "sending #{ business.id } evening notifications"
      business.send_evening_notifications
    end
  end
end
