class CalendarReminder < Struct.new(:view, :date, :reminders, :callback)
    delegate :content_tag, to: :view

    def day_reminders
      reminders.where(:date => date).map do |reminder|
        content_tag :tr do
          content_tag :td, class: "span-2 alert alert-info", colspan: "2" do
            overflow do
              view.capture(reminder, &callback)
            end
          end
        end
      end.join.html_safe
    end

    private
      def overflow
        content_tag :table, class: "overflow" do
          content_tag :tr, class: "overflow" do
            content_tag :td, class: "overflow" do
              yield
            end
          end
        end
      end

      def current_date()
        date.beginning_of_day..date.end_of_day
      end
end
