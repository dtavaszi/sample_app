class Calendar < Struct.new(:view, :date, :callback, :admin)
    HEADER = %w[Sunday Monday Tuesday Wednesday Thursday Friday Saturday]
    START_DAY = :sunday

    delegate :content_tag, to: :view

    def div
			content_tag :div, class: "div_table" do
				table_header + table_month
			end
    end

		def table_header
			content_tag :table, class: "table_schedule_header" do
				header
			end
		end

    def header
      content_tag :tr do
        HEADER.map { |day| content_tag :th, day[0] }.join.html_safe
      end
    end

    def content
      content_tag :table, class: "calendar_content" do
        yield
      end
    end

		def table_month
			content_tag :table, class: "table_schedule_month" do
				week_rows
			end
		end

    def week_rows
      weeks.map do |week|
        content_tag :tr do
          week.map { |day| day_cell(day) }.join.html_safe
        end
      end.join.html_safe
    end

    def book_session_div(day)
      content_tag :div, class: "book-modal-div addclick", onclick: "change_date('#{ day.strftime('%m/%d/%Y') }')", 'data-toggle' => "modal", 'data-target' => "#session_modal" do
        book_session_span.html_safe + book_session_text.html_safe
      end
    end


    def book_session_span
      content_tag :span, class: "glyphicon glyphicon-plus book-modal-span" do
      end
    end

    def book_session_text
      content_tag :span, class: "book-modal-span-text" do
        "BOOK"
      end
    end

    def day_cell(day)
      content_tag :td, class: day_classes(day) do
        content do
            if admin || day > Time.zone.now.to_date
              book_session_div(day).html_safe + day_header(day) + block(day)
            else
              day_header(day) + block(day)
            end
        end
      end
    end

    def day_header(day)
        content_tag :tr do
          content_tag :th do
            content_tag :div, class: "date-div" do
              day.strftime("%d").upcase
            end
          end
        end
    end

    def block(day)
      view.capture(day, &callback)
    end

    def day_classes(day)
      classes = ['relative']
      classes << "today" if day == Time.zone.now.to_date
      classes << (day.month != date.month ?  "not-month" : "this-month")
      classes.empty? ? nil : classes.join(" ")
    end

    def weeks
      first = date.beginning_of_month.beginning_of_week(START_DAY).to_date
      last = date.end_of_month.end_of_week(START_DAY).to_date
      (first..last).to_a.in_groups_of(7)
    end
end
