class CalendarWeekly < Struct.new(:view, :date, :callback, :admin, :workout_sessions)
    HEADER = %w[Sunday Monday Tuesday Wednesday Thursday Friday Saturday]
    START_DAY = :sunday

    HEIGHT = 50
    GAP = 1

    delegate :content_tag, to: :view

    def time_block
      time = date.beginning_of_day
      out = ""
      klass = "GMT-time"
      while time <= (date.end_of_day + 1.hour) do
        out += time_instance(time, klass)
        time += 1.hour

        klass = ""
      end

      content_tag :div, class: "time-block-flex", id: "tbf" do
        content_tag :div, class: "tbf-inner", id: "tbfi" do
          out.html_safe
        end
      end
    end

    def time_instance(time, klass)
      content_tag :div, class: "weekly-time", style: "height: #{ HEIGHT }px;" do
        content_tag :span, class: "weekly-time-span #{ klass } unselectable" do
          if klass != ""
            "GMT" + Time.zone.formatted_offset[0..2]
          else
            time.strftime("%l%P")
          end
        end
      end
    end

    def flex_containers
      content_tag :div, class: "weekly-outer" do
        content_tag :div, class: "flex-container weekly-inner" do
          view.capture(&callback)
        end
      end
    end

    def horizontal_grid
      grids = ""

      (0..25).each do |hour|
        klass = ""
        klass = "now" if (date.beginning_of_day == Time.zone.now.beginning_of_day) && (hour == Time.zone.now.hour)

        grids += hgrid(klass)
      end

      content_tag :div, "aria-hidden" => true, style: "height: 0px;" do
        grids.html_safe
      end
    end

    def hgrid(klass)
      content_tag :div, class:"horiz-grid #{ klass }", style: "height: #{ HEIGHT }px;" do
      end
    end

    def day_column(curr_date)
      content_tag :div, id: "wday#{ curr_date.wday }", class: "day-block" do
      end
    end

    def event_block(ws, zindex, top, left, width, height)
      height = HEIGHT unless height > 0
      out = ws.date.strftime("%l")
      out += ws.date.strftime(":%M") if ws.date.min != 0
      out += ws.duration.strftime(" - %l")
      out += ws.duration.strftime(":%M") if ws.duration.min != 0
      out += ws.duration.strftime("%P")

      if ws.user_workouts.any?
        out += "</br>"

        ws.user_workouts.each do |uw|
          out += uw.user.shortname
          out += "," unless uw = ws.user_workouts.last
        end
      end

      content_tag :div, class: "day-event", style: "z-index: #{ zindex }; top: #{ top }px; left: #{ left }%; width: #{ width }%; height: #{ height }px;" do
        out.html_safe
      end
    end

    def event_bfr(ws, zindex = 2, top, left, width, height)
      if height > 0
        content_tag :div, class: "day-bfr", style: "z-index: #{ zindex }; top: #{ top }px; left: #{ left }%; width: #{ width }%; height: #{ height }px;" do
        end
      else
        ""
      end
    end

    def event_height(length)
      return 0 if length == 0
      (HEIGHT * ((length / 3600.0 - 0.01) * 4).ceil / 4) - GAP
    end

    def get_hour_height(hour, minutes)
      hour.to_f + ((minutes / 60.0) * 4).ceil / 4.0
    end
end
