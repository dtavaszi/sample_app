class ApplicationMailerPreview < ActionMailer::Preview

  def welcome_email
    ApplicationMailer.welcome_email(User.first)
  end

  def activation_email
    usr = User.first
    usr.activation_token = User.digest(User.new_token)
    ApplicationMailer.activation_email(usr)
  end

  def booking_notification_email
    changes = ChangeHistory.all
    business = Business.first
    ApplicationMailer.booking_notification_email(business, changes)
  end

  def send_workouts_reminder
    ApplicationMailer.send_workouts_reminder(User.first, User.first.user_workouts)
  end

end
