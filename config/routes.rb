Rails.application.routes.draw do
  default_url_options :host => "www.fitrackular.com"
  root 'businesses#welcome'

  get '/account_activations/send_activation', to: 'account_activations#send_activation', as: :account_activations_send_activation
  get '/account_activations/blacklist', to: 'account_activations#blacklist', as: :account_activations_blacklist
  get '/account_activations/:id/', to: 'account_activations#activate', :constraints => { :id => /[^\/]+/ }, as: :account_activations_activate

  resources :subscriptions
  patch 'subscribe' => 'subscriptions#subscribe', via: [:patch, :put], as: :subscriptions_subscribe
  patch 'approve' => 'subscriptions#approve', via: [:patch, :put], as: :subscriptions_approve
  patch 'unsubscribe' => 'subscriptions#unsubscribe', via: [:patch, :put], as: :subscriptions_unsubscribe
  patch 'deactivate' => 'subscriptions#deactivate', via: [:patch, :put], as: :subscriptions_deactivate

  get 'auth/failure', to: redirect('/') #Redirect to _RETRY_
  get    '/signup',  to: 'users#new'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'

  get '/embedded_login', to: 'sessions#new_embedded'
  get '/embedded_signup', to: 'users#new_embedded'

  delete '/logout',  to: 'sessions#destroy'

	resources :users, except: [:index]

  get    '/settings',to: 'users#settings'
  get    '/users',to: 'users#admin_index'

  patch    '/users/:id/resync_google' => 'users#resync_google', via: [:patch, :put], as: :users_resync_google
	patch    '/users/:id/approve' => 'users#approve', via: [:patch, :put], as: :users_approve

  resources :friends, 						only: [:create, :destroy]
  patch    '/friends/:id/approve' => 'friends#approve', via: [:patch, :put], as: :friends_approve

  get 'auth/:provider/callback', to: 'sessions#google_authorize'

  resources :reminders
  get ':id/reminders/:id/add', to: 'reminders#add', as: :reminders_add
  get 'reminders/:id/view', to: 'reminders#view', as: :reminders_view

  resources :invoice_items
  resources :user_rates

  patch    ':id/invoices/:id/set_processed' => 'invoices#set_processed', via: [:patch, :put], as: :invoices_set_processed
  patch    ':id/invoices/:id/set_unprocessed' => 'invoices#set_unprocessed', via: [:patch, :put], as: :invoices_set_unprocessed
  patch    ':id/invoices/:id/settle_month' => 'invoices#settle_month', via: [:patch, :put], as: :invoices_settle_month
  patch ':id/invoices/:id/reconcile' => 'invoices#reconcile', via: [:patch, :put], as: :invoices_reconcile

  get ':id/home', to: 'businesses#home', as: :business_home
  get ':id/view_available_groupsessions_embedded', to: 'businesses#view_available_groupsessions_embedded', as: :view_available_groupsessions_embedded

  get ':id/dashboard', to: 'businesses#dashboard', as: :business_dashboard
  get ':id/calendar(/:mode/:date)' => 'businesses#calendar', as: :businesses_calendar
  get ':id/get_month(/:date/:token)' => 'businesses#get_month', as: :businesses_get_month

  resources :suggestions
  resources :invoice_months

  resources :businesses, path: '/' do
    get 'edit/?tab=sessions', to: 'businesses#edit', as: :edit_sessions
    patch 'change_histories/mark_all_read', to: 'change_histories#mark_all_read', as: :mark_all_read

    resources :invoice_items,    only: [:create, :index, :new]
    resources :items
    resources :constants
    resources :taxes
    resources :memberships
    resources :reminders,    only: [:create, :index, :new]
    resources :rates, 							only: [:create]
    resources :workout_sessions
    resources :user_rates,          only: [:create, :index]
    resources :payments
  	resources :invoices

    patch    ':id/invoices/:id/pay_with_credit' => 'invoices#pay_with_credit', via: [:patch, :put], as: :invoices_pay_with_credit
    patch ':date/invoice_months/flip' => 'invoice_months#flip', via: [:patch, :put], as: :invoice_months_flip
    patch ':date/invoice_months/recalculate' => 'invoice_months#recalculate', via: [:patch, :put], as: :invoice_months_recalculate

    patch    '/users/:id/new_gdrive_workout' => 'users#new_gdrive_workout', via: [:patch, :put], as: :users_new_gdrive_workout
    get '/users/new', to: 'users#new_modal', as: :users_new_modal
    post '/users/', to: 'users#admin_create'
  	resources :users

    get '/users/:id/edit_modal', to: 'users#edit_modal', as: :users_edit_modal


    get '/login',   to: 'sessions#new'
    post '/login',   to: 'sessions#create'

    get '/embedded_login', to: 'sessions#new_embedded'
    get '/embedded_signup', to: 'users#new_embedded'
    post '/embedded_signup', to: 'users#create'
    get '/settings', to: 'users#settings'

    get '/sharks', to: 'businesses#sharks', as: :sharks #testing temporary

    get ':id/workout_sessions/add', to: 'workout_sessions#add', as: :workout_sessions_add
    get '/workout_sessions/:id/book_session(/:date)', to: 'workout_sessions#book_groupsession', as: :book_groupsession
    get '/workout_sessions/:id/cancel_session', to: 'workout_sessions#cancel_groupsession', as: :cancel_groupsession


    resources :user_workouts do
      collection do
        put :setoptions
      end
    end

    resources :user_workouts,				only: [:update, :edit]

    get '/workout_sessions/:id/view', to: 'workout_sessions#view', as: :workout_sessions_view
    patch    '/workout_sessions/:id/success' => 'workout_sessions#success', via: [:patch, :put], as: :workout_sessions_success
    patch    '/workout_sessions/:id/approve' => 'workout_sessions#approve', via: [:patch, :put], as: :workout_sessions_approve
    patch    '/workout_sessions/:id/approveallthismonth' => 'workout_sessions#approveallthismonth', via: [:patch, :put], as: :workout_sessions_approveallthismonth
    patch    '/workout_sessions/:id/approveallfollowing' => 'workout_sessions#approveallfollowing', via: [:patch, :put], as: :workout_sessions_approveallfollowing
    patch    '/workout_sessions/:id/approveupto' => 'workout_sessions#approveupto', via: [:patch, :put], as: :workout_sessions_approveupto
    patch    '/workout_sessions/:id/reschedule' => 'workout_sessions#reschedule', via: [:patch, :put], as: :workout_sessions_reschedule
    patch    '/workout_sessions/:id/cancel' => 'workout_sessions#cancel', via: [:patch, :put], as: :workout_sessions_cancel
    patch    '/workout_sessions/:id/noshow' => 'workout_sessions#noshow', via: [:patch, :put], as: :workout_sessions_noshow
    patch    '/workout_sessions/:id/remove/:date' => 'workout_sessions#remove', via: [:patch, :put], as: :workout_sessions_remove
    patch    '/workout_sessions/:id/removefollowing(/:date)' => 'workout_sessions#removefollowing', via: [:patch, :put], as: :workout_sessions_removefollowing
    patch    '/workout_sessions/:id/removeall' => 'workout_sessions#removeall', via: [:patch, :put], as: :workout_sessions_removeall
    patch    '/workout_sessions/:id/unlink' => 'workout_sessions#unlink', via: [:patch, :put], as: :workout_sessions_unlink
    patch    '/workout_sessions/:id/sync_google_event' => 'workout_sessions#sync_google_event', via: [:patch, :put], as: :workout_sessions_sync_google_event
  end
end
