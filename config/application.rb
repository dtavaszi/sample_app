require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)


module SampleApp
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.action_mailer.preview_path = "#{Rails.root}/lib/mailer_previews"

		config.autoload_paths += %W(#{config.root}/lib)
    config.assets.precompile += %w(*.png *.jpg *.jpeg *.gif)
         config.assets.precompile += %w( *.js )
         config.assets.debug = false #was true at first
         config.serve_static_assets = true
		config.eager_load_paths += %W(#{config.root}/lib)
		config.time_zone = 'Pacific Time (US & Canada)'

    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins 'http://jumpingfitnessvictoria.herokuapp.com'
        resource '*', :headers => :any, :methods => [:get, :post, :options], :credentials => true
      end
    end
  end
end
